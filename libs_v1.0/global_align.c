/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      
 *      {gpt,lrommel}@ic.unicamp.br 
 *
 *************************************/


#include "global_align.h" 

alignment* global_align(char* s, char* t, int** M, int g) {

  // Indices for rows and columns, and the matrix:
  int n = strlen(s);
  int m = strlen(t);
  int i=0, j=0;

  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: a \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 
  int** A = im_malloc(n+1,m+1);
  if (!A) 
    return 0;

  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: b {%s} {%s}\n", s, t);
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 

  /* sequences how index to alphabet */
  int *si = seq2index(s);
  int *ti = seq2index(t);


  // Fills the matrix: (global)
  A[0][0] = 0;
  for (i=1; i<=n; i++)
    A[i][0] = i*g;     /* gap */
  for (j=1; j<=m; j++)
    A[0][j] = j*g;     /* gap */

  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: c \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 


  int dia, up, left;
  for (i=1; i<n+1; i++) {
    for (j=1; j<m+1; j++) {
      dia = A[i-1][j-1] + M[(int)si[i-1]][(int)ti[j-1]];
      up = A[i-1][j] + g;
      left = A[i][j-1] + g;
      A[i][j] = (up > dia ? (up > left ? up : left) : (dia > left ? dia : left));
    }
  }

  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: d \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 

  // Searches for maximum score:
  int imax = n;
  int jmax = m;
 

 // Gets best alignment:
  int l = n>m ? 2*n+2 : 2*m+2;
  char* as = malloc(l*sizeof(char));  // A buffer for the aligned portion of s.
  char* at = malloc(l*sizeof(char));  // idem, t.
  int al;

  if (!as || !at) {
    im_free(A,n+1);
    return 0;
  }


  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: e \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 

  alignment* align = malloc(sizeof(alignment));
  if (!align) {
    im_free(A,n+1);
    free(as);
    free(at);
    return 0;
  }

  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: f \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 

  align->s_gaps = 0;
  align->t_gaps = 0;

  as[l-1] = 0;
  at[l-1] = 0;

  i = imax;
  j = jmax;
  al = l-2;


  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf("'global_align' CHECKPOINT: g \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 
 
  while (!(i==0&&j==0)){
    if (i>0 && j>0){
    if (A[i][j] == A[i-1][j-1] + M[(int)si[i-1]][(int)ti[j-1]]) { // dia
      as[al] = s[--i];
      at[al--] = t[--j];    
      continue;
    }

    }

    if (j>0) {
    if (A[i][j] == A[i][j-1] + g) {  // left
      as[al] = '-';
      at[al--] = t[--j];
      align->s_gaps++;    
      continue;
    }

    }


    if (i>0){
     { // top: A[i][j] == A[i-1][j] + g
      as[al] = s[--i];
      at[al--] = '-';
      align->t_gaps++;
      continue;
    }
    }
   
  }

  #if DEBUG==1000  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf(" h \n");
    printf("--------------------- \n");
    fflush(stdout);
  #endif
 
 

  /* original sequences */
  align->s = malloc((1+strlen(s))*sizeof(char));
  strcpy(align->s, s );

  align->t = malloc((1+strlen(t))*sizeof(char));
  strcpy(align->t, t );


  /* save alignament */

  align->score = A[imax][jmax];
  align->length = l-al-1;

  al++;
  align->s_line = malloc(align->length*sizeof(char));
  memcpy(align->s_line,as+al,align->length*sizeof(char));

  align->t_line = malloc(align->length*sizeof(char));
  memcpy(align->t_line,at+al,align->length*sizeof(char));

  align->length--;
  align->s_start = i;
  align->t_start = j;

  im_free(A,n+1);
  free(as);
  free(at);

  free(si);
  free(ti);

  return align;

}
 

