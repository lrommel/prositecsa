/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef DFAMIN_H
#define DFAMIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#include "nodelist_state.h"
#include "dfa.h"

/*************************************/
typedef struct _nodediff{
  int  i;
  int  j;                       
  struct _nodediff *next;     
} nodediff;
/*************************************/

/*************************************/
nodediff ***matlist_nodediff_malloc(int rows, int cols);
void        matlist_nodediff_free(nodediff ***d, int rows, int cols);
void        matlist_nodediff_init(nodediff ***d, int rows, int cols);

void          list_nodediff_free(nodediff *L);
nodediff *add_list_nodediff(nodediff *L, int i, int j );
void    print_list_nodediff(nodediff *L);

dfa *dfa2dfamin( dfa *D );

nodediff *mark_recursion(int **M, nodediff ***matlist, nodediff *L);
void cumulative_state(dfa *D, int tp, int tq);
void copy_state(dfa *D, int p, int q);
void reduction_d(dfa *D, int n);

void print_tablediff(int **M, int numst);
/*************************************/

#endif
