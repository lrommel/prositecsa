/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "enfa.h"

/*************************************/
enfa *enfa_init(int numst,int start, int end, int *C){
   enfa *N;
   int i;

   /* allocate enfa */
   N = (enfa *)malloc(sizeof(enfa)); 

   if (!N) {
	perror("enfa malloc");
	exit(errno);
   }

   /* allocate table of transitions */
   N->d = st_malloc(numst,ns+1);

   if (!N->d) {
	perror("allocate table of transitions ");
	exit(errno);
   }

   /* init table of transitions with NULL*/
   st_init( N->d , numst , ns+1 );

   /* allocate table of cycle flag */
   N->C =  (int *) malloc( numst * sizeof(int) );
   if (!(N->C)) {
	perror("allocate table of cycle flag");
	exit(errno);
   }

   if(C == NULL){
      /* init table of cycle flag*/
      memset(N->C, 0, (numst)*sizeof(int));
   }
   else{
     for(i=0;i<numst;i++){
         N->C[i]=C[i];  
     }
   }
 
   /* begin data enfa */
   N->start=start;
   N->end=end;
   N->numst=numst;
 
   return N;
}

/*************************************/
enfa *letter2enfa(char c){ 
   enfa *N;
   int j;

   N = enfa_init(2,0,1,NULL);

   /* transitions */
   for(j=0;j<ns;j++){
       if(SIGMA[j]==c){
             N->d[0][j]=add_nodelist_state(N->d[0][j],1);
       }
   } 
   return N;
}

/*************************************/
enfa *point2enfa(char c){ 
   enfa *N;
   int j;

   N = enfa_init(2,0,1,NULL);

   /* transitions */
   for(j=0;j<ns;j++){
       N->d[0][j]=add_nodelist_state(N->d[0][j],1);
   } 
   return N;
}

/*************************************/
void  enfa_free(enfa *N){
   st_free(N->d, N->numst, ns+1);
   free(N->C);
   free(N);
}

/*************************************/
void closure_E(enfa *N, int state, int *T){

   nodelist_state *aux;

   T[state]=1; 
   aux = N->d[state][ns];   
   while(aux!=NULL){   
       if (T[aux->state]==0){          /* if not in T    */ 
          T[aux->state]=1;             /* add state in T */
          closure_E(N,aux->state,T);   /* Recursion      */    
       }
       aux=aux->next;
   }
}

/*************************************/
void closure_alpha(enfa *N, int *T0, int idxletter, int *T){

   nodelist_state *aux;
   int i;
   
   for(i=0;i<N->numst;i++) if(T0[i]) closure_E(N , i , T0);  /* closure_E init*/

   for(i=0;i<N->numst;i++){ 
      if(T0[i]==1){    
          aux = N->d[i][idxletter]; 
          while(aux!=NULL){       
             if (T[aux->state]==0){   /* if not in T    */      
                 T[aux->state]=1;     /* add state in T */
                 closure_E(N, aux->state, T);  /* E moves*/     
             }
             aux=aux->next;
         }
      }
   } 
}

/*************************************/
int get_numtrans_enfa(enfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;
   int numtrans=0;  

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<=ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               numtrans++;
               aux=aux->next;
         }
     } 
   } 

   return numtrans;
}

/*************************************/
int get_numtransE_enfa(enfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;
   int numtrans=0;  

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=ns;j<=ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               numtrans++;
               aux=aux->next;
         }
     } 
   } 

   return numtrans;
}

/*************************************/
int cycle_start(enfa *N){
   return N->C[N->start];
}

/*************************************/
int cycle_end(enfa *N){
   return N->C[N->end];
}


/*************************************/
void print_enfa_details(enfa *N){

   printf("ENFA: \n{\n"); 
   printf("states: %4i\n",N->numst);
   printf("transitions: %4i\n",get_numtrans_enfa(N));
   printf("E-transitions: %4i\n}\n",get_numtransE_enfa(N));
 
   // printf("\n");

}
 

/*************************************/
void print_enfa(enfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;  

   printf("\t\t\tstart : %4i\n",N->start);
   printf("\t\t\tend   : %4i\n",N->end);
   printf("\t\t\tnumst : %4i\n",N->numst);

   printf("\t\t\t");
   for(i=0;i<numst;i++) 
       printf("%d:%d ",i,N->C[i]);
   printf("\n");

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<=ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               if(j==ns)  printf("\t\t\tq%i -> q%i [label=\"EPS\"]\n",i,aux->state);
               else       printf("\t\t\tq%i -> q%i [label=\"%c\"]\n",i,aux->state,SIGMA[j]);
               aux=aux->next;
         }
     } 
   } 

   printf("\n\n");

}

/*************************************/
void print_enfa_dot(enfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;  

    printf("digraph finite_state_machine {\n");
    printf("   rankdir=LR;\n");
    printf("   edge [fontname=arial,fontsize=11]\n");
    printf("   node [fontname=arial,fontsize=11,shape=doublecircle];q%i;\n",N->end);
    printf("   node [shape=circle,size=10]\n");
    printf("   start [shape=point]\n");
    printf("   start -> q%i\n",N->start);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<=ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               if(j==ns)  printf("   q%i -> q%i [label=\"EPS\"]\n",i,aux->state);
               else       printf("   q%i -> q%i [label=\"%c\"]\n",i,aux->state,SIGMA[j]);
               aux=aux->next;
         }
     } 
   } 
   printf("}\n");
}

/*************************************/
void fprint_enfa_dot(char *basename, char *suffix, enfa *N){

    FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
    char cmd[500];

    nodelist_state *aux;
    int i,j;
    int numst=N->numst;  

    fprintf(of,"digraph finite_state_machine {\n");
    fprintf(of,"   rankdir=LR;\n");
    fprintf(of,"   edge [fontname=arial,fontsize=11]\n");
    fprintf(of,"   node [fontname=arial,fontsize=11,shape=doublecircle];q%i;\n",N->end);
    fprintf(of,"   node [shape=circle,size=10]\n");
    fprintf(of,"   start [shape=point]\n");
    fprintf(of,"   start -> q%i\n",N->start);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<=ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               if(j==ns)  fprintf(of,"   q%i -> q%i [label=\"EPS\"]\n",i,aux->state);
               else       fprintf(of,"   q%i -> q%i [label=\"%c\"]\n",i,aux->state,SIGMA[j]);
               aux=aux->next;
         }
     } 
   } 
   fprintf(of,"}\n");

   fclose(of);

   /* dot command */
   sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
   /* puts(cmd);   */
   /* system(cmd); */

}

/*************************************/

void fprint_enfa_1arc_dot(char *basename, char *suffix, enfa *N){

    FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
    char cmd[5000];

    nodelist_state *aux;
    int i,j,k;
    int numst=N->numst; 

    int *v,flag,w;
    char label[60000],cad[10000]; 

    v = (int *) malloc((ns+1)*sizeof(int));
    memset(v, 0, (ns+1)*sizeof(int));

    fprintf(of,"digraph finite_state_machine {\n");
    fprintf(of,"   rankdir=LR;\n");
    fprintf(of,"   edge [fontname=arial,fontsize=11]\n");
    fprintf(of,"   node [fontname=arial,fontsize=11,shape=doublecircle];q%i;\n",N->end);
    fprintf(of,"   node [shape=circle,size=10]\n");
    fprintf(of,"   start [shape=point]\n");
    fprintf(of,"   start -> q%i\n",N->start);

    /* print transitions */
    for(i=0;i<numst;i++){
      for(k=0;k<numst;k++){
         memset(v, 0, (ns+1)*sizeof(int));
         flag=0;
         /* states i for k */ 
         for(j=0;j<=ns;j++){
               aux=N->d[i][j];
               while(aux!=NULL){
                    if(k==aux->state){
                        v[j]=1;
                        flag++;
                        break;
                    } 
                    aux=aux->next;
               }
         }

         if(flag){
         if(flag==ns+1){
           fprintf(of,"   q%i -> q%i [label=\"all,EPS\"]\n",i,k);
         }
         else if( flag==ns && v[ns]==0 ){
           fprintf(of,"   q%i -> q%i [label=\"all\"]\n",i,k);
         }
         else{
           /* v: i to k */
           w=1;
           strcpy(label,"");
           for(j=0;j<ns;j++){
              if(v[j]){
                 if(w){
                         sprintf(cad,"%c",SIGMA[j]);
                         w=0;
                 }
                 else    sprintf(cad,",%c",SIGMA[j]);

                 strcat(label,cad);
              }
           }

           j=ns;
           if(v[j]){
                 if(w){
                         sprintf(cad,"EPS");
                         w=0;
                 }
                 else    sprintf(cad,",EPS");

                 strcat(label,cad);
           }

           fprintf(of,"   q%i -> q%i [label=\"%s\"]\n",i,k,label);
         }
         } /* if(flag)*/
      } 
    }  
    fprintf(of,"}\n");

    fclose(of);

    /* dot command */
    sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
    /* puts(cmd);   */
    /* system(cmd); */

}

/*************************************/

