/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "enfa2nfa.h"

/*************************************/
nfa *enfa2nfa( enfa *E ){
   nfa *N;
   int *T0,*T;
   int i,j,k;
   int numst = E->numst;

   /* begin nfa */
   N = (nfa *) malloc(sizeof(nfa));
   if (!N) {
	perror("enfa2nfa nfa malloc");
	exit(errno);
   }
   N->end   = NULL;
   N->start = E->start;

   N->numst = numst;  /* number of states in nfa */  


   /* allocate table of transitions */
   N->d = st_malloc(numst, ns);
   if (!N->d) {
	perror("enfa2nfa table of transitions malloc");
	exit(errno);
   }
 
   /* init table of transitions with NULL*/
   st_init( N->d , numst, ns );



   T0 = (int *) malloc(numst*sizeof(int));
   if (!T0) {
	perror("T0 transitions malloc");
	exit(errno);
   }

   T  = (int *) malloc(numst*sizeof(int));
   if (!T) {
	 perror("T transitions malloc");
	 exit(errno);
   }




   /* transitions */
   for(i=0;i<numst;i++){
      memset(T0, 0, numst*sizeof(int) );
      T0[i]=1;

      //closure_E(E , i , T0);      /* closure E */
 
      for(j=0;j<ns;j++){
         memset(T , 0, numst*sizeof(int) );

         /* closure alpha */
         closure_alpha(E, T0, j, T);
 
         for(k=0;k<numst;k++){ 
             if(T[k]==1){
                 N->d[i][j]=add_nodelist_state( N->d[i][j], k );
             }
         }

      }/*end for*/
   }/*end for*/  

 

   /* end states */
   N->end=add_nodelist_state( N->end, E->end );

   /*  end state: condition empty string */
   memset(T0, 0, numst*sizeof(int) );
   closure_E(E , E->start , T0);
   if(T0[E->end]==1){
       N->end=add_nodelist_state( N->end, E->start );
   }


   free(T);
   free(T0);

  return N;
}

/*************************************/

