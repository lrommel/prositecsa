/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "utils.h"

/*************************************/
int **im_malloc(int X,int Y){
  int **matrix;
  int i;
  matrix = (int**) malloc(X*sizeof(int*));
  if (!matrix) {
	perror("malloc matrix in im_malloc");
	exit(errno);
  }

  for (i=0; i<X; i++) {
      matrix[i] = (int*) malloc(Y*sizeof(int));
      if (!matrix[i]) {
	perror("malloc matrix[i] in im_malloc");
	exit(errno);
      }
  }
  return matrix;
}

/*************************************/
void im_free(int **A, int n){
  int i;
  for (i=0; i<n; i++){
     free(A[i]);
  }
  free(A);
}

/*************************************/
void im_print(int **A,int i_1, int i_2, int j_1, int j_2){
  int i,j;

  printf("    ");
  for (j=j_1; j<=j_2; j++)  printf("%4d", j); 
  printf("\n");

  for (i=i_1; i<=i_2; i++){
     printf("%4d", i); 
     for (j=j_1; j<=j_2; j++){
        printf("%4d", A[i][j]);
     }
     printf("\n");
  }
}

/*************************************/
void im_init(int **d, int rows, int cols, int value){
  int i,j;
  for(i=0;i<rows;i++){
     for(j=0;j<cols;j++){
         d[i][j]=value;
     } 
  }
} 


/*************************************/
void print_vector(int *V, int n){
   int i;
   for(i=0;i<n;i++){
      printf("%3d",V[i]);
   }
   printf("\n");
}

/*************************************/
char *load_word(char *filename){
  FILE *ifp;
  char *WORD;
  int  nword;
  char c;
  int  length=8; /* initial length to RE*/
  int  inc=8;  /* increment to realloc length to RE */
  int  flag;

  /* file input */
  ifp = fopen(filename, "r");

  if (ifp == NULL) {
      fprintf(stderr, "ERROR: can't open input file %s!\n",filename);
      exit(1);
  }

  /* allocate */
  WORD = (char *) malloc(length);   
  if (!WORD) {
	perror("load_1word: malloc WORD init");
	exit(errno);
  }

  nword=0;
  WORD[nword]='\0';
  while((flag=fscanf(ifp,"%c",&c))==1&&(c==' '||c=='\n'));
  if(flag==1){
      WORD[nword]=c;
      WORD[++nword]='\0';
      while(fscanf(ifp,"%c",&c)==1&&c!=' '&&c!='\n'){

          /* read alphabet */
          WORD[nword]=c;
          WORD[++nword]='\0';

          if(nword>length-2) { 
              length+=inc;  
              WORD = realloc(WORD,length);
                  if (!WORD) {
                     perror("load_1word: re-alloc WORD");
                      exit(errno);
                  }
                  /*printf("\nREALLOC:%d:",length);print_1word(WORD); */
          }
      }
  }
  /*print_1word(WORD);*/
 
  /* close file */
  fclose(ifp);

  return WORD;

}
/*************************************/
void print_word(char *WORD){
  printf("WORD:%d:%s::\n",(int)strlen(WORD),WORD);
}
/*************************************/
void extract_basename(char *basename, char *filename){
   char *pch;

   strcpy(basename,filename);
   pch=basename+strlen(basename)-1;
   while(pch>=basename && *pch!='.') pch--;
   *pch='\0';

  if(strlen(basename)==0) strcpy(basename,filename);

}


/*************************************/
FILE *fopen_suffix(char *basename, char *suffix, char *extension , char *opentype){
   FILE *p;
   char filename[500];

   strcpy(filename,basename);
   strcat(filename,suffix);
   strcat(filename,extension);

   p = fopen(filename, opentype);

   if (p == NULL) {
      fprintf(stderr, "ERROR: can't open output file %s!\n",filename);
      exit(1);
   }

   return p;
}

/*************************************/

int any( int *T, int n){
  int i;
  for (i=0;i<n;i++){
     if (T[i]!=0) return 1;
  }
  return 0;
}

/*************************************/
int set_equals( int *T0, int *T1, int n){
  int i;
  for (i=0;i<n;i++){
     if (T0[i]!=T1[i]) return 0;
  }
  return 1;
}

/*************************************/
char *strcpy2(char *des, char *ori){
  char *tmp;
  int  n = strlen(ori);
  
  free(des);
  
  tmp = (char *) malloc( (n+1)*sizeof(char) );
  if (!tmp) {
	perror("malloc tmp in strcpy2");
	exit(errno);
  }

  strcpy(tmp,ori);

  //free(des);

  return tmp;
}

/*************************************/
char *strcat2(char *des, char *cad){
  char *tmp;
  int  n1 = strlen(des);
  int  n2 = strlen(cad);

  tmp  = (char *) malloc( (n1+n2+1)*sizeof(char) );
  if (!tmp) {
	perror("malloc tmp in strcat2");
	exit(errno);
  }
 
  strcpy(tmp, des);
  strcat(tmp, cad);
 
  free(des);
 
  return tmp;
}

/*************************************/



/*************************************/
/*************************************/
/*************************************/
/*************************************/
/*************************************/
/*************************************/



/*************************************/
int char2index( char c ){
    int i;
    for (i=0;i<ns;i++){
        if(c==SIGMA[i]) return i;
    }

    printf("ERROR char2index to {%d} :  no character in sequence\n",c);
    exit(0);
    return -1;
}

/*************************************/
int *seq2index( char *seq){
     int i,n = strlen(seq);
     int *si;
 
     si = (int *) malloc(n*sizeof(int));

     for (i=0;i<n;i++){
         si[i] = char2index(seq[i]);
     }
    
     return si;
}
/*************************************/


/*************************************/
void print_seq_index(int *s, int n){
    int i;
    printf("sequence index:\t[");
    for (i=0;i<n;i++){
        printf("%c",SIGMA[s[i]]);
    }
    printf("]\n");
}

void out(char *m){
    printf("+-------------------- \n");
    printf("| CHECKPOINT: %s\n", m);
    printf("+-------------------- \n");
    fflush(stdout);
}

/*************************************/
/*************************************/
/*************************************/
/*************************************/
/*************************************/
/*************************************/



