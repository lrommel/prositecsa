/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef  TICTOC_H
#define  TICTOC_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <unistd.h>
#include <sys/time.h>

#define __NUMMARKS__  8

/*************************************/
struct timeval __ts__[__NUMMARKS__];
struct timeval __tf__[__NUMMARKS__];
/*************************************/

/*************************************/
float time_diff(struct timeval ts, struct timeval tf);
void  tic();
float toc();
void  tic_id(int id);
float toc_id(int id, char *s);
 
/*************************************/

#endif
