/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "nfa.h"


/*************************************/
void  nfa_free(nfa *N){
   st_free(N->d, N->numst, ns);  /* Transitions */
   nodelist_state_free(N->end);  /* final states list */
   free(N);
}
/*************************************/


/*************************************/
int nfa_isFinal( nfa *N, int state ){
    return in_nodelist_state( N->end, state);
}


/*************************************/
void  nfa_addLoop(nfa *N){
   int i,j;
   nodelist_state  *aux;

   /* begin state */
   i = N->start;
   for(j=0;j<ns;j++){
         N->d[i][j] =  add_nodelist_state( N->d[i][j] , i );
   } 
 
   /* final state */
   aux = N->end;
   while(aux!=NULL){ 
        i = aux->state; 

        for(j=0;j<ns;j++){
            N->d[i][j] =  add_nodelist_state( N->d[i][j] , i );
        } 
  
        aux=aux->next;
   }
}
 

/*************************************/
void  nfa_actives_free(nodelist_state  **X, char *s){
   int n = strlen(s);
   int i;
   
   if (X != NULL){
	   for(i=0;i<=n;i++){
	      nodelist_state_free( X[i] );
	   }
   }

   free(X);
}


/*************************************/
nodelist_state  **nfa_actives(nfa *N, char *s){
   nodelist_state  **X, *aux, *aux_old, *tmp;
   int n = strlen(s);
   int i,j, existe;
  
   /* allocate */
   X =  ( nodelist_state  ** ) malloc( (n+1) * sizeof( nodelist_state * ) );
   if (!X) {
	perror("nfa_actives malloc");
	exit(errno);
   }
 
   /* init */
   for(i=0;i<=n;i++){
          X[i]=NULL;
   }
 
   /* sequence  how index to alphabet */
   int *si; 

   /* sequence  */
   si = seq2index(s);
 
   /* begin: q_0 is the one active state */  
   X[0] = add_nodelist_state(X[0], N->start );
 
   /* fill  for each symbol: s[i] */
   for(i=1;i<=n;i++){
       j=si[i-1];
       aux_old = X[i-1];
       while(aux_old!=NULL){
             aux = N->d[ aux_old->state ][j];
             while(aux!=NULL){ 
                 X[i] = add_nodelist_state(X[i], aux->state );  
                 aux=aux->next;
             }
            aux_old = aux_old->next;
       }
 
   }
 
   #if DEBUG==1  
       /* test */
       printf("\n Active list before the reverse process: sequence={%s}", s);
       print_nfa_actives_seq( X,  n+1 , s );
   #endif
 
   /* for each state in X[n], verify if is final state */
   tmp = NULL;
   aux = X[n];
   while(aux!=NULL){ 
         if (  !nfa_isFinal( N, aux->state )  ){
              tmp = add_nodelist_state(tmp, aux->state ); 
         }
         aux=aux->next;
   }
   /* delete */
   aux = tmp;
   while(aux!=NULL){ 
         X[n] = del_nodelist_state( X[n], aux->state );
         aux=aux->next;
   }
   nodelist_state_free(tmp);
   
 

   /* reverse process */
   for( i=n-1; i>=0; i-- ){
       tmp = NULL;
       j=si[i];
       aux_old = X[i];
       while(aux_old!=NULL){
             aux = N->d[ aux_old->state ][j];
             existe = 0;
             while(aux!=NULL){ /*  exists? aux->state in X[i+1] */
                 if( in_nodelist_state( X[i+1], aux->state) ) {
                     existe = 1;
                     break;
                 }
                 aux=aux->next;
             }
             if (existe){
                 tmp = add_nodelist_state(tmp, aux_old->state );   
             }
            aux_old = aux_old->next;
       }
       /* delete */
       nodelist_state_free(X[i]);
       X[i] = tmp;
   }
 
   #if DEBUG==1  
       /* test */
       printf("\n Active list after the reverse process: sequence={%s}", s);
       print_nfa_actives_seq( X,  n+1, s );
   #endif

   free(si);

   return X;
}

/*************************************/
void print_nfa_actives_seq( nodelist_state  **X, int n, char *s ){
   int i;
   printf("\n");
   for(i=0;i<n;i++){
       if (i>0) printf("%3d : %c :",i, s[i-1] );
       else     printf("%3d : %c :",i,   ' ' );
       print_nodelist(X[i]);   
   }
}

/*************************************/
void print_nfa_actives( nodelist_state  **X, int n ){
   int i;
   printf("\n");
   for(i=0;i<n;i++){
       printf("%3d : ",i);
       print_nodelist(X[i]);   
   }
}



/*************************************/
int get_numtrans_nfa(nfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;
   int numtrans=0;  

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               numtrans++;
               aux=aux->next;
         }
     } 
   } 

   return numtrans;
}


/*************************************/
void print_nfa_details(nfa *N){

   printf("NFA: \n{\n"); 
   printf("states: %4i\n",N->numst);
   printf("transitions: %4i\n",get_numtrans_nfa(N));
   printf("E-transitions: %4i\n}\n",0);
 
}
 

/*************************************/
void print_nfa(nfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;  

   printf("\n\t\t\tstart : %4i\n",N->start);

    aux=N->end;
    while(aux!=NULL){ 
      printf("\t\t\t  end : %4i\n",aux->state); 
      aux=aux->next;
    }

   printf("\t\t\tnumst : %4i\n\n",N->numst);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               printf("\t\t\tq%i -> q%i [label=%c]\n",i,aux->state,SIGMA[j]);
               aux=aux->next;
         }
     } 
   } 

   printf("\n\n");

}

/*************************************/
void print_nfa_dot(nfa *N){
   nodelist_state *aux;
   int i,j;
   int numst=N->numst;  

    printf("digraph finite_state_machine {\n");
    printf("   rankdir=LR;\n");
    printf("   edge [fontname=arial,fontsize=11]\n");

    aux=N->end;
    while(aux!=NULL){ 
      printf("   node [fontname=arial,fontsize=14,shape=doublecircle];q%i;\n",aux->state);   
      aux=aux->next;
    }

    printf("   node [shape=circle,size=10]\n");
    printf("   start [shape=point]\n");
    printf("   start -> q%i\n",N->start);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               printf("   q%i -> q%i [label=%c]\n",i,aux->state,SIGMA[j]);
               aux=aux->next;
         }
     } 
   } 
   printf("}\n");
}

/*************************************/
void fprint_nfa_dot(char *basename, char *suffix,  nfa *N){

    FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
    char cmd[500];

    nodelist_state *aux;
    int i,j;
    int numst=N->numst;  

    fprintf(of,"digraph finite_state_machine {\n");
    fprintf(of,"   rankdir=LR;\n");
    fprintf(of,"   edge [fontname=arial,fontsize=11]\n");

    aux=N->end;
    while(aux!=NULL){ 
      fprintf(of,"   node [fontname=arial,fontsize=14,shape=doublecircle];q%i;\n",aux->state);   
      aux=aux->next;
    }
 
    fprintf(of,"   node [shape=circle,size=10]\n");
    fprintf(of,"   start [shape=point]\n");
    fprintf(of,"   start -> q%i\n",N->start);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){ 
               fprintf(of,"   q%i -> q%i [label=%c]\n",i,aux->state,SIGMA[j]);
               aux=aux->next;
         }
     } 
   } 
   fprintf(of,"}\n");

   fclose(of);

   /* dot command */
   sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
   /* puts(cmd);   */
   /* system(cmd); */

}

/*************************************/

void fprint_nfa_1arc_dot(char *basename, char *suffix,  nfa *N){
    FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
    char cmd[5000];

    nodelist_state *aux;
    int i,j,k;
    int numst=N->numst; 

    int *v,flag,w;
    char label[60000],cad[10000]; 

    v = (int *) malloc((ns+1)*sizeof(int));
    if (!v) {
	perror("malloc in fprint_nfa_1arc_dot function");
	exit(errno);
    }
    memset(v, 0, (ns+1)*sizeof(int));

    fprintf(of,"digraph finite_state_machine {\n");
    fprintf(of,"   rankdir=LR;\n");
    fprintf(of,"   edge [fontname=arial,fontsize=11]\n");

    aux=N->end;
    while(aux!=NULL){ 
      fprintf(of,"   node [fontname=arial,fontsize=14,shape=doublecircle];q%i;\n",aux->state);   
      aux=aux->next;
    }

    fprintf(of,"   node [shape=circle,size=10]\n");
    fprintf(of,"   start [shape=point]\n");
    fprintf(of,"   start -> q%i\n",N->start);

    /* print transitions */
    for(i=0;i<numst;i++){
      for(k=0;k<numst;k++){
         memset(v, 0, (ns+1)*sizeof(int));
         flag=0;
         /* states i for k */ 
         for(j=0;j<ns;j++){
               aux=N->d[i][j];
               while(aux!=NULL){
                    if(k==aux->state){
                        v[j]=1;
                        flag++;
                        break;
                    } 
                    aux=aux->next;
               }
         }

         if(flag){
         if( flag==ns ){
           fprintf(of,"   q%i -> q%i [label=\"all\"]\n",i,k);
         }
         else{
           /* v: i to k */
           w=1;
           strcpy(label,"");
           for(j=0;j<ns;j++){
              if(v[j]){
                 if(w){
                         sprintf(cad,"%c",SIGMA[j]);
                         w=0;
                 }
                 else    sprintf(cad,",%c",SIGMA[j]);

                 strcat(label,cad);
              }
           }

           fprintf(of,"   q%i -> q%i [label=\"%s\"]\n",i,k,label);
         }
         } /* if(flag)*/
      } 
    }  
    fprintf(of,"}\n");

    fclose(of);

    /* dot command */
    sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
    /* puts(cmd);   */
    /* system(cmd); */

}

/*************************************/

