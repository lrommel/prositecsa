/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef RE2ENFA_H
#define RE2ENFA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

#include "enfa.h"
#include "re.h"
#include "utils.h"

/*************************************/
typedef struct _nodelist_enfa{
  char symbol; /*  Operator "()*|" ou Enfa "E" */ 
  enfa *p;     /*  ENFA    */

  struct _nodelist_enfa *prev;
  struct _nodelist_enfa *next;

} nodelist_enfa;
/*************************************/

/*************************************/
nodelist_enfa *add_nodelist_enfa(nodelist_enfa *L, char c );

enfa* re2enfa_thompson(char* R, int CASE);
nodelist_enfa *reduction_list_enfa(nodelist_enfa *L, int CASE);

enfa *enfa_opsum_E( enfa *N1);
enfa *enfa_opsum_melhor( enfa *N1);

enfa *enfa_opE_E( enfa *N1);
enfa *enfa_opE_melhor( enfa *N1);

enfa *enfa_kleen_E( enfa *N1);
enfa *enfa_kleen_melhor(enfa *N1);
enfa *enfa_kleen_melhor_caso_i  (enfa *N1);
enfa *enfa_kleen_melhor_caso_ii (enfa *N1);
enfa *enfa_kleen_melhor_caso_iii(enfa *N1);
enfa *enfa_kleen_melhor_caso_iv (enfa *N1);

void  enfa_change_state( enfa *N, int p, int q);

enfa *enfa_alternation_E( enfa *N1, enfa *N2 );
enfa *enfa_alternation_melhor( enfa *N1, enfa *N2 );

enfa *enfa_catenation_Efree (enfa *N1, enfa *N2 );
enfa *enfa_catenation_E     (enfa *N1, enfa *N2 );
enfa *enfa_catenation_melhor(enfa *N1, enfa *N2 );

enfa *enfa_union( enfa *N1, enfa *N2, int state_add );

enfa *enfa_E_correctness( enfa *N1);

void print_list_enfa(nodelist_enfa *L);
/*************************************/

#endif
