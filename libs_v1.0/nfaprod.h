/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef NFAPROD_H
#define NFAPROD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "enfa.h"
#include "utils.h"

#include "nodelist_state.h"
#include "dfa.h"
#include "nfa.h"

/*************************************/
typedef struct _nfaprod{
  int  start;
  struct _nodelist_state  *end;  /*  end states     */
  int  numst;
  int  dfa_numst;
  int  **states;
  nodelist_state  ***d;          /*  Transitions    */
  int *alphaall;
} nfaprod;
/*************************************/

/*************************************/

void  nfaprod_free(nfaprod *P);



int isFinal( nfaprod *P, int state );

void nfaprod_init(nfaprod *P, int numst, int start, nodelist_state *aux);

nfaprod *dfa2prod(dfa *D);
nfaprod *nfa2prod(nfa *N);

int isFinal( nfaprod *P, int st );


int get_numtrans_nfaprod(nfaprod *P);

void print_nfaprod_details(nfaprod *P);
void print_nfaprod_dot(nfaprod *P);
void fprint_nfaprod_dot(char *basename, char *suffix, nfaprod *P);

int spaceletter(int *v);
int letterspace(int *v);
int letterall(int *v, int j);
int allletter(int *v, int j);
void fprint_nfaprod_1arc_dot(char *basename, char *suffix, nfaprod *P);
/*************************************/

#endif
