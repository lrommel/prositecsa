/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef RECSA_H
#define RECSA_H
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "utils.h"

#include "re2enfa.h"
#include "enfa2nfa.h"
//#include "enfa2dfa.h"
//#include "dfamin.h"
#include "nfaprod.h"

#include "alignment.h"
#include "global_align.h"


/*************************************/
typedef struct _dp_state{
     int id;
     int score;
     int s1;
     int t1;
     int s2;
     int t2;      
     struct _dp_state  *next;        
} dp_state;
/*************************************/

/*************************************/
typedef struct _dp_states_container{
    dp_state    *state;              
} dp_states_container;
/*************************************/

/*************************************/

dp_states_container **dp_matrix_allocate(int X,int Y);

void dp_matrix_free(dp_states_container **A, int X, int Y );

void  dp_matrix_vectors_allocate( dp_states_container **A, int X, int Y, int numst );
void  dp_matrix_vectors_free( dp_states_container **A, int X, int Y  );

void  dp_cell_matrix_init( dp_states_container *cell, int id);
void  dp_matrix_1vector_init( dp_states_container **A, int i, int j, int numst);
void  dp_matrix_1vector_init_list( dp_states_container **A, int i, int j, int P_numst,  nodelist_state  **active_s, nodelist_state  **active_t, int x1, int x2, int N_numst);
void  dp_matrix_vectors_init( dp_states_container **A, int X, int Y, int numst);


int symbolcost_prod(int a1, int a2, int** M, int g);
int max_state_end(nfaprod *P, dp_state  *v, int n);

void arslan_transision_savePosition (nfaprod *P, dp_state  *ori,  dp_state  *des, int a1, int a2, int** M, int g, int x1, int x2, int x3, int x4,  nodelist_state  **active_s, nodelist_state  **active_t , int numst,  int *lista_actives , char type);

void arslan_transision(nfaprod *P, dp_state  *ori,  dp_state  *des, int a1, int a2, int** M, int g);
int arslan_previus_transision(nfaprod *P, dp_state  *ori, int value, int q, int a1, int a2, int **M, int g);


dp_states_container **recsa_arslan( char* s, char* t,  nfaprod *P, int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst );
void  recsa_arslan_dp(int* si, int* ti,   int n, int m,   nfaprod *P,  dp_states_container **A,  int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst );

dp_states_container **recsa_arslan_1line( char* s, char* t,  nfaprod *P, int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst );
void  recsa_arslan_dp_1line(int* si, int* ti,   int n, int m,   nfaprod *P,  dp_states_container **A,  int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst );

alignment* recsa_arslan_recovery(char* s, char* t, nfaprod *P,   dp_states_container **A,  int** M, int g );
alignment* recsa_arslan_recovery_by_positions(char* s, char* t, nfaprod *P,   dp_states_container **A, int imax, int jmax,  int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst );


/*************************************/

#endif
