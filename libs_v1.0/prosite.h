/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef PROSITE_H
#define PROSITE_H
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

/*************************************/
 
/*************************************/

/*************************************/
char *pros2R(char *Pros);
char *pros2R_simple(char *Pros);

char *load_prosite(char *filename);
int prosite_success(char *PROSITE);
void print_prosite(char *PROSITE);

/*************************************/

#endif
