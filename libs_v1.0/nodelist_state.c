/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "nodelist_state.h"


/*************************************/
void print_nodelist( nodelist_state *L ){
  printf("NodeList={ ");
  while(L!=NULL){  
          printf("%2d, ", L->state);
          L=L->next;
  }
  printf("}\n");
}


/*************************************/
nodelist_state *del_nodelist_state(nodelist_state *L, int state ){
  nodelist_state  *aux1, *aux2;
 
  if ( L == NULL ) 
     return NULL;
 
  aux1 = L;
  if ( aux1->state == state ){
     L = aux1->next;
     free(aux1);
     return L;
  }

  aux1 = L;
  aux2 = L->next;  
  while(aux2 !=NULL && aux2->state!=state){  /* while new state */
          aux1=aux1->next;
          aux2=aux2->next;
  }
  if(aux2->state==state){  /* is new state */
          aux1->next = aux2->next; 
          free(aux2);
  }
  
  return L;
 
}


/*************************************/
nodelist_state *add_nodelist_state(nodelist_state *L, int state ){
  nodelist_state  *node,*aux;

  /* allocate */
  node =  malloc(sizeof(nodelist_state));
  if (!node) {
	perror("nodelist_state malloc");
	exit(errno);
  }

  /* begin node  */
  node->next=NULL;
  node->state=state;

  /* add node to list  */
  if (L==NULL){
     return node;
  }
  else{
     aux=L;
     while(aux->next!=NULL && aux->state!=state){  /* while new state */
          aux=aux->next;
     }
     if(aux->state!=state){  /* is new state */
          aux->next=node;
     }
     else{
         free(node);
     }
     return L;
  }
}


/*************************************/
void nodelist_state_free(nodelist_state *L){
   nodelist_state *aux;
     
   aux=L;
   while(aux!=NULL){   
       aux=aux->next;
       /* free header list */
       free(L);
       L=aux; 
   }
}


/*************************************/
int in_nodelist_state(nodelist_state *aux, int state){
   int result = 0;

   while(aux!=NULL){   
       if ( aux->state == state ){
               result = 1;
               break;
       } 
       aux=aux->next;
   }

   return result;
}

/*************************************/
nodelist_state ***st_malloc(int X, int Y){
  nodelist_state ***delta;
  int i;

  delta = (nodelist_state***) malloc(X*sizeof(nodelist_state**));
  if (!delta) {
	perror("delta st_malloc");
	exit(errno);
  }

  for (i=0; i<X; i++) {
      delta[i] = (nodelist_state**) malloc(Y*sizeof(nodelist_state*));
  
	if (!delta[i]) {
	char aux[30];
        sprintf(aux,"delta[%d] st_malloc",i);
	perror(aux);
	exit(errno);
	}
}
  return delta;
}

/*************************************/
void st_free(nodelist_state ***delta, int rows, int cols){
  int i,j;
  if (delta!=NULL)
  for (i=0; i<rows; i++) {
     for(j=0;j<cols;j++){
         nodelist_state_free(delta[i][j]);
     } 
     free(delta[i]);
  }
  free(delta);
}

/*************************************/
void st_init(nodelist_state ***d, int rows, int cols){
  int i,j;
  for(i=0;i<rows;i++){
     for(j=0;j<cols;j++){
         d[i][j]=NULL;
     } 
  }
}

/*************************************/


void test_nodelist(){

   nodelist_state *L = NULL;

   printf("\n");
 
   L = add_nodelist_state(L, 1 );  print_nodelist(L);


   L = add_nodelist_state(L, 2 );  print_nodelist(L);   
   L = add_nodelist_state(L, 3 );  print_nodelist(L);
   L = add_nodelist_state(L, 4 );  print_nodelist(L);   
   L = add_nodelist_state(L, 5 );  print_nodelist(L);
   L = add_nodelist_state(L, 5 );  print_nodelist(L);   
   L = add_nodelist_state(L, 6 );  print_nodelist(L);
   L = add_nodelist_state(L, 7 );  print_nodelist(L);   
  

   L = del_nodelist_state(L, 4 );  print_nodelist(L);   

   L = add_nodelist_state(L, 0 );  print_nodelist(L);  
   L = add_nodelist_state(L, 9 );  print_nodelist(L);  

   L = del_nodelist_state(L, 9 );  print_nodelist(L);   

   L = del_nodelist_state(L, 1 );  print_nodelist(L);   
   L = del_nodelist_state(L, 2 );  print_nodelist(L);   
   L = del_nodelist_state(L, 3 );  print_nodelist(L);   

}





