/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      
 *      {gpt,lrommel}@ic.unicamp.br 
 *
 *************************************/

#ifndef GLOBAL_ALIGN_H
#define GLOBAL_ALIGN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "utils.h"
 

#include "alignment.h"


/*************************************/

alignment* global_align(char* s, char* t, int** M, int g) ;

/*************************************/

#endif


