/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "re.h"

/*************************************/
char *load_re(char *filename){
  char *RE;
  
  RE = load_word(filename);
 
  if (!re_success(RE)) {
      fprintf(stderr, "ERROR: RE error!!\n");
      exit(1);
  }

  return RE;
}
/*************************************/
int re_success(char *RE){
   /* valida regular expression */
   int i = 0;
   int flagLE,fp;
   char c;

   fp=0;
   flagLE=1;
   c=RE[i];
   while(c!='\0'){
        if (c=='.'||isLetter(c)||isOperator(c)||isParentheses(c)){
              
              if(isOperator(c)&&flagLE){
                 printf("ERROR: Bad format, operator '%c' without 'Expression' \n",c);
                 exit(0); 

              }
              
              if(c=='(')  fp++;  
              if(c==')')  fp--;  
              if(fp<0){
                 printf("ERROR: Regular Expression bad format, parentheses not balanced -\n");
                 exit(0);
              }

              if(c=='(')  flagLE=1;
              else        flagLE=0;

              i++;
              c=RE[i];
        }
        else {
           printf("ERROR: Regular Expression bad format: symbol %c not in alphabet\n",c);
           exit(0);
        }

   }
   if(fp>0){
        printf("ERROR: Regular Expression bad format, parentheses not balanced +\n");
        exit(0);
   }

   return 1;
}

/*************************************/
void print_re(char *RE){
  printf("RE: %d \n{\n%s\n}\n",(int) strlen(RE), RE );
}
/*************************************/

int isLetter(char c){ 
     int i;

     for(i=0;i<ns;i++){
         if(c==SIGMA[i]) return 1;
     }
     return 0;
}

/*************************************/
int isOperator(char c){
          if(c=='*') return 1;
    else  if(c=='|') return 1;
    else  if(c=='?') return 1;
    else  if(c=='+') return 1;
    else             return 0;
}
/*************************************/
int isParentheses(char c){
          if(c=='(') return 1;
    else  if(c==')') return 1;
    else             return 0;
}

/*************************************/
void reduction_alphabet(char *RE){
   /* tight alphabet SIGMA to RE */
   char *flag;
   int  i,j,n = strlen(RE);
   int  ns_new=0;
   char *SIGMA_new;

   /* allocate flag */
   flag = (char *) malloc(256);
   memset(flag,0,256);

   i=0;
   while(i<n){
     if(isLetter(RE[i]) && flag[(int)RE[i]]==0 ){
          flag[(int)RE[i]]=1;
          ns_new++;
     }
     i++;
   }

   /* allocate */
   SIGMA_new = (char *) malloc(ns_new+1);

   /* SIGMA_new */
   i=0;j=0; 
   while(i<ns){
     if(flag[(int)SIGMA[i]]==1 ){
          SIGMA_new[j]=SIGMA[i];
          j++;
     }
     i++;
   }

   SIGMA_new[ns_new]='\0';

   free(SIGMA);
   SIGMA = SIGMA_new;
   ns = ns_new;

  free(flag);
   
}
/*************************************/

