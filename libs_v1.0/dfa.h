/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef DFA_H
#define DFA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#include "nodelist_state.h"

/*************************************/
 typedef struct _dfa{
  int  start;
  int  numst;
  int  **d;                 /*  Transitions                */
  nodelist_state  *end;     /*  end states   "enfa.c"      */
} dfa;
/*************************************/

/*************************************/
int  get_numtrans_dfa(dfa *D);
 
void print_dfa_details(dfa *D);
void print_dfa_dot(dfa *D);
void fprint_dfa_dot(char *basename, char *suffix, dfa *D);
void fprint_dfa_1arc_dot(char *basename, char *suffix, dfa *D);
/*************************************/

#endif
