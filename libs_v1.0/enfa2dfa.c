/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "enfa2dfa.h"

/*************************************/
void init_nodelist_dfa_state(nodelist_dfa_state *node, int numst, int ns , int *T, int *D){

      int i;
 
      if (T==NULL){
            node->T = (int *) malloc(numst*sizeof(int));
            for(i=0;i<numst;i++) node->T[i]=0;
      }
      else node->T=T;
      
      if (D==NULL){
           node->D = (int *) malloc(ns*sizeof(int)); 
           for(i=0;i<ns;i++) node->D[i]=-1;
      }
      else node->D=D;

      node->id=0;
      node->next=NULL;

}

/*************************************/
void nodelist_dfa_free(nodelist_dfa_state *L){
   nodelist_dfa_state *aux;
     
   aux=L;
   while(aux!=NULL){   
       aux=aux->next;

       /* free header list */
       free(L->T);
       free(L->D);
       free(L);
        
       L=aux; 
   }
}


/*************************************/

int addfind_id(nodelist_dfa_state *L, int *T, int numst){
   nodelist_dfa_state  *aux, *prev, *node_new;

   prev=NULL;
   aux=L; 
   while (aux!=NULL){
      if (set_equals(T,aux->T, numst)){
             free(T);   /* new set free */
             return aux->id;
      }
      prev=aux;
      aux=aux->next;
   }

  /*  add new state */
  if(aux==NULL){
         node_new = (nodelist_dfa_state *) malloc( sizeof (nodelist_dfa_state) );
         init_nodelist_dfa_state( node_new , numst , ns , T , NULL);
       
         node_new->id=prev->id+1;   /* id new node    */ 
         prev->next=node_new;       /* conected       */
   }  

   return node_new->id;
}

/*************************************/
int nodelist_dfa_numst(nodelist_dfa_state  *L){
   nodelist_dfa_state  *aux=L;
   int numst=0;             
   while (aux!=NULL){
      numst++;
      aux=aux->next;
   }
   return numst;
}

/*************************************/
dfa *enfa2dfa( enfa *N ){
   dfa *D;
   nodelist_dfa_state  *L = NULL, *aux;
   int i,j;
   int numst = N->numst; 
   int id, *T;

   /* Create begin state */
   L = (nodelist_dfa_state *) malloc( sizeof (nodelist_dfa_state) );
   init_nodelist_dfa_state(L ,  numst ,  ns , NULL , NULL);

   closure_E(N , N->start , L->T);

   /* constructor of DFA states in nodelist_dfa*/
   aux=L;
   while (aux!=NULL){
      for(i=0;i<ns;i++){
         /* state for closure alpha*/
         T = (int *) malloc(numst*sizeof(int));
         for(j=0;j<numst;j++) T[j]=0;

         #if DEBUG==1
         printf("E: "); 
         print_vector( aux->T , numst);
         #endif

         /* closure alpha */
         closure_alpha(N, aux->T, i, T);

         #if DEBUG==1
         printf("\tA, %c: ",SIGMA[i]); 
         print_vector(T , numst);
         #endif

         if ( any(T, numst) ){
             /* find id DFA state */
             id = addfind_id(L, T, numst);    /* free T */

             /* insert transition to DFA */
             aux->D[i]=id;        
         }
      } 
      aux=aux->next;
   }

   /* begin dfa */
   D = (dfa *) malloc(sizeof(dfa));
   D->end=NULL;
   D->start=0;
   D->numst=nodelist_dfa_numst(L);  /* number of states in dfa */  
   D->d=im_malloc(D->numst,ns);
   im_init( D->d, D->numst, ns, -1);        

   /* copy list to dfa */
   aux=L;
   while (aux!=NULL){
      for(i=0;i<ns;i++){
         D->d[aux->id][i] = aux->D[i];        
      }
      
      /* if end state */
      if(aux->T[N->end]==1) {
           D->end=add_nodelist_state( D->end, aux->id );
      }

      aux=aux->next;
   }
   
   /* free  nodelist_dfa */
   nodelist_dfa_free(L);

   return D;
}

/*************************************/

