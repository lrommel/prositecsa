/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "alphabet.h"

 
/*************************************/
char letter_inSigma( int a ){

   char c = '-';
   if (a < ns) c = SIGMA[a];

   return c;
}


/*************************************/
int alphabet_success(){
  /*SIGMA, ns*/
  char v[256];
  int i;

  /* length alphabet */
  if (ns != strlen(SIGMA) ) {
      fprintf(stderr, "ERROR: length alphabet\n");
      return 0;
  }

  /* duplicated symbols */
  memset(v, 0, 256);
  for(i=0;i<ns;i++){
      if(v[(int)SIGMA[i]]>0){
         fprintf(stderr, "ERROR: symbol '%c' duplicated, offset %d\n",SIGMA[i],i+1);
         return 0;
      }
      else v[(int)SIGMA[i]]++;
  }

  return 1;

}
/*************************************/
void load_alphabet(char *filename){
 
  SIGMA = load_word(filename);
  ns    = strlen(SIGMA);
 
  if (!alphabet_success()) {
      fprintf(stderr, "ERROR: alphabet error!!\n");
      exit(1);
  }

}
/*************************************/
void print_alphabet(){
  printf("Alphabet: %d \n{\n%s\n}\n",ns,SIGMA);
}
/*************************************/
void load_alphabet_old(char *filename){
  FILE *ifp;

  /* file input */
  ifp = fopen(filename, "r");

  if (ifp == NULL) {
      fprintf(stderr, "ERROR: can't open input file %s!\n",filename);
      exit(1);
  }

  /* read length alphabet */
  fscanf(ifp,"%d",&ns);

   /* allocate */
   SIGMA = (char *) malloc(ns+1);

  /* read alphabet */
  fscanf(ifp,"%s",SIGMA);
  
  if (!alphabet_success()) {
      fprintf(stderr, "ERROR: alphabet error!!\n");
      exit(1);
  }
 
  /* close file */
  fclose(ifp);

}

/*************************************/

