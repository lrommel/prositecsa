/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef  NFA_H
#define  NFA_H
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

#include "nodelist_state.h"

/*************************************/
typedef struct _nfa{
  int  start;
  nodelist_state  *end;     /*  end states  */
  int  numst;

  /* Transitions */
  nodelist_state  ***d;      
        
} nfa;
/*************************************/

void  nfa_free(nfa *N);


void  nfa_actives_free(nodelist_state  **X, char *s);


int nfa_isFinal( nfa *N, int state );

void  nfa_addLoop(nfa *N);
nodelist_state  **nfa_actives(nfa *N, char *s);

void print_nfa_actives_seq( nodelist_state  **X, int n, char *s );
void print_nfa_actives( nodelist_state  **X, int n );

/*************************************/
int  get_numtrans_nfa(nfa *N);

void  print_nfa_details(nfa *N);
void  print_nfa(nfa *N);
void  print_nfa_dot(nfa *N);
void fprint_nfa_dot(char *basename, char *suffix, nfa *N);
void fprint_nfa_1arc_dot(char *basename, char *suffix, nfa *N);
/*************************************/

#endif
