/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "prosite.h"


/*************************************/
char *load_prosite(char *filename){
  char *PROSITE;
  
  PROSITE = load_word(filename);
 
  if (!prosite_success(PROSITE)) {
      fprintf(stderr, "ERROR: PROSITE error!!\n");
      exit(1);
  }

  return PROSITE;
}


/*************************************/
int prosite_success(char *PROSITE){
   
   return 1;
}


/*************************************/
void print_prosite(char *PROSITE){
  printf("PROSITE: %d \n{\n%s\n}\n",(int)strlen(PROSITE), PROSITE );
}

  
/*************************************/
char *pros2R(char *Pros){
   const char s[2] = "-";
   char *token;
   int npros = strlen(Pros);
   char *partetemp = (char *)malloc(1), 
        *parte     = (char *)malloc(1), 
        *partenum  = (char *)malloc(1);

   int  i;
   char flag[256];
   char ccc[3];
   char *Rtemp  = (char *)malloc(100); /* strcat2 and strcpy2 inc size */
   char *Rpart1 = (char *)malloc(100);
   char *Rpart2 = (char *)malloc(100);
   char *tmp    = (char *)malloc(100);
   int ctl,n1,n2;

   char *_Pros;
   _Pros        = (char *)malloc( strlen(Pros)+1 );
   strcpy(_Pros,Pros);


   /* delete end point*/
   if(_Pros[npros-1]=='.'){
      npros--;
      _Pros[npros]='\0';
   }
 
   Rtemp[0]='\0'; 

   /* get the first token */
   token = strtok(_Pros, s);
   
   /* walk through other tokens */
   while( token != NULL ){

      #if DEBUG==1
      printf("\ntoken: %s\n", token );fflush(stdout);
      #endif
 
      parte = strcpy2(parte,"");

      if(token[0]=='{'){
          memset(flag,1,256);

          i=1;
          while(token[i]!='}'){
             flag[(int)token[i]]=0;
             i++;
          }

          /* ctl for parenthesis  expand */
          i++;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);
  

          partetemp = strcpy2(partetemp,"(");
          for(i=0;i<ns;i++){
             if( flag[(int)SIGMA[i]]==1 /*&& SIGMA[i]!='<' && SIGMA[i]!='>'*/ ){
                 ccc[0]=SIGMA[i];
                 ccc[1]='|';
                 ccc[2]='\0';
                 partetemp = strcat2(partetemp,ccc);
             }
          }
          partetemp[strlen(partetemp)-1]=')';
 

      }
      else if(token[0]=='['){

          partetemp = strcpy2(partetemp,"(");
          i=1;
          while(token[i]!=']'){
             ccc[0]=token[i];
             ccc[1]='|';
             ccc[2]='\0';
             partetemp = strcat2(partetemp,ccc);
             i++;
          }
          partetemp[strlen(partetemp)-1]=')';

          /* ctl for parenthesis  expand */
          i++;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);


 
      } 
      else if(token[0]=='x'){

          partetemp = strcpy2(partetemp,"(");
          for(i=0;i<ns;i++){
             if( 1 /* && SIGMA[i]!='<' && SIGMA[i]!='>' */){
                 ccc[0]=SIGMA[i];
                 ccc[1]='|';
                 ccc[2]='\0';
                 partetemp = strcat2(partetemp,ccc);
             }
          }
          partetemp[strlen(partetemp)-1]=')';

          /* ctl for parenthesis  expand */
          i=1;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);

      }
      else{
 
          partetemp = strcpy2(partetemp,"");
 
          ccc[0]=token[0];
          ccc[1]='\0';
          ccc[2]='\0';
          partetemp = strcat2(partetemp,ccc);
 

          /* ctl for parenthesis  expand */
          i=1;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);

      }


      /* parenthesis  expand */     /* partetemp is the simple element */
      parte = strcat2(parte,"");
      if(ctl==-1){
              parte = strcat2(parte,"");
      }     
      else if(ctl==1){
              for(i=0;i<n1-1;i++) parte = strcat2(parte, partetemp);
      
      }
      else if(ctl==2){
              for(i=0;i<n1-1;i++) parte = strcat2(parte, partetemp);

              if(n1>0){
                  /* Re-writing parts, able to build with the predecessor */
                  Rpart1 = strcat2( Rpart1, Rpart2);
                  Rpart1 = strcat2( Rpart1, parte);
                  Rpart2 = strcpy2( Rpart2, partetemp);
                  Rtemp  = strcpy2( Rtemp , Rpart1);
                  Rtemp  = strcat2( Rtemp , Rpart2);  
              }

              parte   = strcpy2(parte, partetemp);
              for(i=0;i<n2-n1-1;i++)    {
                     tmp   = strcpy2(tmp,"("); 
                     //tmp   = strcat2(tmp,parte); /**/
                     tmp   = strcat2(tmp, partetemp);
                     tmp   = strcat2(tmp,"|");
                     tmp   = strcat2(tmp, partetemp);
                     tmp   = strcat2(tmp,parte); /**/
                     tmp   = strcat2(tmp,")");

                     parte = strcpy2(parte, tmp);
              }      
              
      }

      #if DEBUG==1
      printf("parte: %s\n",parte);fflush(stdout);
      printf("Rtemp: %s\n",Rtemp );fflush(stdout);
      #endif

      if(ctl==2){
            //composition of Rtemp
            tmp    = strcpy2(tmp,"(");
            tmp    = strcat2(tmp,Rpart2);
            tmp    = strcat2(tmp,parte);  
 
            tmp    = strcat2(tmp,"|");
            tmp    = strcat2(tmp,Rpart2);
            tmp    = strcat2(tmp,")");

            Rpart2 = strcpy2( Rpart2,  tmp);  
      }
      else  {           

           Rpart1 = strcpy2( Rpart1, Rtemp);
           Rpart1 = strcat2( Rpart1, parte);
           Rpart2 = strcpy2( Rpart2, partetemp);    
      }

      Rtemp  = strcpy2( Rtemp , Rpart1);
      Rtemp  = strcat2( Rtemp , Rpart2);  

      #if DEBUG==1
      printf("Rtemp: %s\n",Rtemp );fflush(stdout);
      #endif
    
      token = strtok(NULL, s);
   }

  free(_Pros);
 
  free(partetemp); 
  free(parte);
  free(partenum);
 
  free(Rpart1);
  free(Rpart2);
  free(tmp);
 
  return Rtemp;
}



/*************************************/
char *pros2R_simple(char *Pros){
   const char s[2] = "-";
   char *token;
   int npros = strlen(Pros);
   char *partetemp = (char *)malloc(1), *parte = (char *)malloc(1), *partenum = (char *)malloc(1);

   int  i,j;
   char flag[256];
   char ccc[3];
   char *Rtemp  = (char *)malloc(100); /* strcat2 and strcpy2 inc size */
   char *Rpart1 = (char *)malloc(100);
   char *Rpart2 = (char *)malloc(100);
   char *tmp    = (char *)malloc(100);
   int ctl,n1,n2;

   char *_Pros;
   _Pros = (char *)malloc( strlen(Pros)+1 );
   strcpy(_Pros,Pros);


   /* delete end point*/
   if(_Pros[npros-1]=='.'){
      npros--;
      _Pros[npros]='\0';
   }
 
   Rtemp[0]='\0'; 

   /* get the first token */
   token = strtok(_Pros, s);
   
   /* walk through other tokens */
   while( token != NULL ){

      #if DEBUG==1
      printf("\ntoken: %s\n", token );fflush(stdout);
      #endif
 
      parte = strcpy2(parte,"");

      if(token[0]=='{'){
          memset(flag,1,256);

          i=1;
          while(token[i]!='}'){
             flag[(int)token[i]]=0;
             i++;
          }

          /* ctl for parenthesis  expand */
          i++;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);
  

          partetemp = strcpy2(partetemp,"(");
          for(i=0;i<ns;i++){
             if( flag[(int)SIGMA[i]]==1 /*&& SIGMA[i]!='<' && SIGMA[i]!='>'*/ ){
                 ccc[0]=SIGMA[i];
                 ccc[1]='|';
                 ccc[2]='\0';
                 partetemp = strcat2(partetemp,ccc);
             }
          }
          partetemp[strlen(partetemp)-1]=')';
 

      }
      else if(token[0]=='['){

          partetemp = strcpy2(partetemp,"(");
          i=1;
          while(token[i]!=']'){
             ccc[0]=token[i];
             ccc[1]='|';
             ccc[2]='\0';
             partetemp = strcat2(partetemp,ccc);
             i++;
          }
          partetemp[strlen(partetemp)-1]=')';

          /* ctl for parenthesis  expand */
          i++;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);


 
      } 
      else if(token[0]=='x'){

          partetemp = strcpy2(partetemp,"(");
          for(i=0;i<ns;i++){
             if( 1 /* && SIGMA[i]!='<' && SIGMA[i]!='>' */){
                 ccc[0]=SIGMA[i];
                 ccc[1]='|';
                 ccc[2]='\0';
                 partetemp = strcat2(partetemp,ccc);
             }
          }
          partetemp[strlen(partetemp)-1]=')';

          /* ctl for parenthesis  expand */
          i=1;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);

      }
      else{
 
          partetemp = strcpy2(partetemp,"");
 
          ccc[0]=token[0];
          ccc[1]='\0';
          ccc[2]='\0';
          partetemp = strcat2(partetemp,ccc);
 

          /* ctl for parenthesis  expand */
          i=1;
          partenum = strcpy2(partenum, token+i);
          ctl = sscanf(partenum,"(%d,%d)", &n1,&n2);

      }


      /* parenthesis  expand */     /* partetemp is the simple element */
      parte = strcat2(parte,"");
      if(ctl==-1){
              parte = strcat2(parte,"");
      }     
      else if(ctl==1){
              for(i=0;i<n1-1;i++) parte = strcat2(parte, partetemp);
      
      }
      else if(ctl==2){
              for(i=0;i<n1-1;i++) parte = strcat2(parte, partetemp);

              if(n1>0){
                  /* Re-writing parts, able to build with the predecessor */
                  Rpart1 = strcat2( Rpart1, Rpart2);
                  Rpart1 = strcat2( Rpart1, parte);
                  Rpart2 = strcpy2( Rpart2, partetemp);
                  Rtemp  = strcpy2( Rtemp , Rpart1);
                  Rtemp  = strcat2( Rtemp , Rpart2);  
              }

              parte   = strcpy2(parte, "(");
              for(i=n2-n1; i>1; i--)    {

                  tmp = strcpy2(tmp, "");
                  for(j=1;j<=i; j++)    {
                        tmp   = strcat2(tmp, partetemp);
                  }

                  tmp   = strcat2(tmp, "|");
                  parte = strcat2(parte, tmp);
  
/*                   tmp   = strcpy2(tmp,"("); 
                     tmp   = strcat2(tmp, partetemp);
                     tmp   = strcat2(tmp,"|");
                     tmp   = strcat2(tmp, partetemp);
                     tmp   = strcat2(tmp,parte); 
                     tmp   = strcat2(tmp,")");

                     parte = strcpy2(parte, tmp);
*/
              }      
              parte = strcat2(parte, partetemp);
              parte = strcat2(parte, ")");
              
      }

      #if DEBUG==1
      printf("parte: %s\n",parte);fflush(stdout);
      printf("Rtemp: %s\n",Rtemp );fflush(stdout);
      #endif

      if(ctl==2){
            //composition of Rtemp
            tmp    = strcpy2(tmp,"(");
            tmp    = strcat2(tmp,Rpart2);
            tmp    = strcat2(tmp,parte);  
 
            tmp    = strcat2(tmp,"|");
            tmp    = strcat2(tmp,Rpart2);
            tmp    = strcat2(tmp,")");

            Rpart2 = strcpy2( Rpart2,  tmp);  
      }
      else  {           

           Rpart1 = strcpy2( Rpart1, Rtemp);
           Rpart1 = strcat2( Rpart1, parte);
           Rpart2 = strcpy2( Rpart2, partetemp);    
      }

      Rtemp  = strcpy2( Rtemp , Rpart1);
      Rtemp  = strcat2( Rtemp , Rpart2);  

      #if DEBUG==1
      printf("Rtemp: %s\n",Rtemp );fflush(stdout);
      #endif
    
      token = strtok(NULL, s);
   }

  free(_Pros);

  free(partetemp);
  free(parte);
  free(partenum);

  free(Rpart1);
  free(Rpart2);
  free(tmp);
 

  return Rtemp;
}
