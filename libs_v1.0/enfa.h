/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef ENFA_H
#define ENFA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

#include "nodelist_state.h"

/*************************************/
typedef struct _enfa{
  int  start;
  int  end;
  int  numst;
  int  *C;  /* cycle flag of states */

  /* Transitions */
  nodelist_state  ***d;
        
} enfa;
/*************************************/

/*************************************/
enfa *enfa_init(int numst,int start, int end, int *C);

enfa *letter2enfa(char c);
enfa *point2enfa(char c);

void  enfa_free(enfa *N);
 
void closure_E(enfa *N, int state, int *T);
void closure_alpha(enfa *N, int *T0, int idxletter, int *T);

int  get_numtrans_enfa(enfa *N);
int  get_numtransE_enfa(enfa *N);

int  cycle_start(enfa *N);
int  cycle_end(enfa *N);

void  print_enfa_details(enfa *N);
void  print_enfa(enfa *N);
void  print_enfa_dot(enfa *N);
void fprint_enfa_dot     (char *basename, char *suffix, enfa *N);
void fprint_enfa_1arc_dot(char *basename, char *suffix, enfa *N);
/*************************************/

#endif
