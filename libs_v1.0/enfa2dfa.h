/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef ENFA2DFA_H
#define ENFA2DFA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#include "enfa.h"
#include "dfa.h"

/*************************************/
typedef struct _nodelist_dfa_state{  /* for dfa contruction         */
  int id;
  int *T;                            /* table of ENFA subset states */
  int *D;                            /* DFA Transitions             */
  struct _nodelist_dfa_state *next;
} nodelist_dfa_state;
/*************************************/

/*************************************/
void init_nodelist_dfa_state(nodelist_dfa_state *node, int numst, int ns , int *T, int *D);
void nodelist_dfa_free(nodelist_dfa_state *L);
 
int addfind_id(nodelist_dfa_state *L, int *T, int numst);
int nodelist_dfa_numst(nodelist_dfa_state  *L);

dfa *enfa2dfa( enfa *N );

/*************************************/

#endif
