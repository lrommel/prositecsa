/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "dfa.h"

/*************************************/
int  get_numtrans_dfa(dfa *D){
   int i,j;
   int numst=D->numst;  
   int numtrans=0;

   /* transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
         if(D->d[i][j]>=0) numtrans++;
     } 
   } 
   return numtrans;
}

/*************************************/
void print_dfa_details(dfa *D){
 
   printf("DFA: \n{\n"); 
   printf("states: %4i\n",D->numst);
   printf("transitions: %4i\n",get_numtrans_dfa(D) );
   printf("}\n"); 
   // printf("\n");

}

/*************************************/
void print_dfa_dot(dfa *D){
    nodelist_state *aux;
    int i,j;
    int numst=D->numst;  

    printf("digraph finite_state_machine {\n");
    printf("   rankdir=LR;\n");
    printf("   edge [fontname=arial,fontsize=12]\n");

    aux=D->end;
    while(aux!=NULL){ 
      printf("   node [fontname=arial,fontsize=14,shape=doublecircle];q%i;\n",aux->state);   
      aux=aux->next;
    }

    printf("   node [shape=circle,size=10]\n");
    printf("   start [shape=point]\n");
    printf("   start -> q%i\n",D->start);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
         if(D->d[i][j]>=0) printf("   q%i -> q%i [label=%c]\n",i,D->d[i][j],SIGMA[j]);
     } 
   } 
   printf("}\n");
}

/*************************************/
void fprint_dfa_dot(char *basename, char *suffix, dfa *D){

   FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
   char cmd[500];

   nodelist_state *aux;
   int i,j;
   int numst=D->numst;  

   fprintf(of,"digraph finite_state_machine {\n");
   fprintf(of,"   rankdir=LR;\n");
   fprintf(of,"   edge [fontname=arial,fontsize=12]\n");

   aux=D->end;
   while(aux!=NULL){ 
      fprintf(of,"   node [fontname=arial,fontsize=14,shape=doublecircle];q%i;\n",aux->state);   
      aux=aux->next;
   }

   fprintf(of,"   node [shape=circle,size=10]\n");
   fprintf(of,"   start [shape=point]\n");
   fprintf(of,"   start -> q%i\n",D->start);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){
          if(D->d[i][j]>=0) fprintf(of,"   q%i -> q%i [label=%c]\n",i,D->d[i][j],SIGMA[j]);
     } 
   } 
   fprintf(of,"}\n");
   fclose(of);

   /* dot command */
   sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
   puts(cmd);
   /* system(cmd); */
}

/*************************************/
void fprint_dfa_1arc_dot(char *basename, char *suffix, dfa *D){

   FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
   char cmd[500];

   nodelist_state *aux;
   int i,j,k;
   int numst=D->numst;  

   int *v,flag,w;
   char label[6000],cad[100];
   
   v = (int *) malloc(ns*sizeof(int));
   memset(v, 0, ns*sizeof(int));

   fprintf(of,"digraph finite_state_machine {\n");
   fprintf(of,"   rankdir=LR;\n");
   fprintf(of,"   edge [fontname=arial,fontsize=12]\n");

   aux=D->end;
   while(aux!=NULL){ 
      fprintf(of,"   node [fontname=arial,fontsize=14,shape=doublecircle];q%i;\n",aux->state);   
      aux=aux->next;
   }

   fprintf(of,"   node [shape=circle,size=10]\n");
   fprintf(of,"   start [shape=point]\n");
   fprintf(of,"   start -> q%i\n",D->start);

   /* print transitions */
   for(i=0;i<numst;i++){
      for(k=0;k<numst;k++){
         memset(v, 0, ns*sizeof(int));
         flag=0;
         /* states i for k */ 
         for(j=0;j<ns;j++){
              if(k==D->d[i][j]){
                  v[j]=1;
                  flag++;
              }
         }

         if(flag){
         if(flag==ns){
           fprintf(of,"   q%i -> q%i [label=\"all\"]\n",i,k);
         }
         else{
           /* v: i to k */
           w=1;
           strcpy(label,"");
           for(j=0;j<ns;j++){
              if(v[j]){
                 if(w){
                         sprintf(cad,"%c",SIGMA[j]);
                         w=0;
                 }
                 else    sprintf(cad,",%c",SIGMA[j]);

                 strcat(label,cad);
              }
           }
           fprintf(of,"   q%i -> q%i [label=\"%s\"]\n",i,k,label);
         }
         } /* if(flag)*/
      } 
   } 
   fprintf(of,"}\n");
   fclose(of);

   /* dot command */
   sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
   /* puts(cmd);   */
   /* system(cmd); */
}


