/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "nfaprod.h"

#if DEBUG==1
  #include "malloc_count.h"
#endif




/*************************************/


/*************************************/
void  nfaprod_free(nfaprod *P){

   st_free(P->d, P->numst, (ns+1)*(ns+1)-1 );  /* Transitions */

   nodelist_state_free(P->end);                /* final states list */

   im_free(P->states, P->numst);

   free(P->alphaall);

   free(P);
}
/*************************************/


/*************************************/
int isFinal( nfaprod *P, int state ){
    return in_nodelist_state( P->end, state);
}


/*************************************/
void nfaprod_init(nfaprod *P, int numst, int start, nodelist_state *aux){
   int i,j,k;
   nodelist_state *auxi, *auxj;
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;
 
   /* begin state */
   //P->start = start;
   P->start   = numst * start  +  start;

   /* num states */
   P->numst = numst*numst;
   P->dfa_numst = numst;

   /* transitions */
   P->d = st_malloc( P->numst, nsprod );
   if (!P->d) {
	perror("nfaprod_init P->d malloc");
	exit(errno);
   }

   st_init( P->d , P->numst, nsprod ); 

   /* states prod */
   P->states=im_malloc(P->numst, 2);
   if (!P->states) {
	perror("nfaprod_init P->states malloc");
	exit(errno);
   }

   k=0;
   for(i=0;i<numst;i++){
      for(j=0;j<numst;j++){
          P->states[k][0]=i;
          P->states[k][1]=j; 
          k++;
      }
   }

   /* end states  F x F */    
   P->end=NULL;
   auxi=aux;
   while(auxi!=NULL){ 
        auxj=aux;
        while(auxj!=NULL){  
            P->end=add_nodelist_state( P->end, (auxi->state)*numst + (auxj->state) );
            auxj=auxj->next;
        }
        auxi=auxi->next;
   }

   /*alphaall*/
   P->alphaall = (int *)malloc(P->numst*sizeof(int));
   if (!P->alphaall) {
	perror("nfaprod_init P->alphaall malloc");
	exit(errno);
   }
   memset(P->alphaall, 0, P->numst*sizeof(int));

   /* end transitions */

   aux = P->end;   
   while(aux!=NULL){ 
      for( i=0 ; i < nsprod ; i++ ){
          P->d[aux->state][i] = add_nodelist_state( P->d[aux->state][i], aux->state );        
      }
      P->alphaall[aux->state]=1;  
      aux=aux->next;
   }

  /* start transitions */
  for( i=0 ; i < nsprod ; i++ ){
     P->d[P->start][i] = add_nodelist_state( P->d[P->start][i], P->start );        
  } 
  P->alphaall[P->start]=1;  /*mark begin state to alphaall*/
}

/*************************************/
nfaprod *nfa2prod(nfa *N){
   nfaprod *P;
   int numst = N->numst;  
   int i,j,k,l;
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;
   int a1,a2;
   nodelist_state *auxi, *auxj;

   P = (nfaprod *)malloc(sizeof(nfaprod));
   if (!P) {
	perror("nfaprod P malloc");
	exit(errno);
   }

   nfaprod_init(P, N->numst, N->start, N->end);

   /* transitions */
   k=0;
   for(i=0;i<numst;i++){

      #if DEBUG==1
      malloc_count_print_status(); fflush(stderr);
      #endif

      for(j=0;j<numst;j++){
          for(l=0;l<nsprod;l++){
                a1=l/ns1;
                a2=l%ns1;
                if( a1<ns && a2<ns ){
                   if((N->d[i][a1]!=NULL)&&(N->d[j][a2]!=NULL)){

                        auxi=N->d[i][a1];
                        while(auxi!=NULL){
                            auxj=N->d[j][a2];
                            while(auxj!=NULL){

                                P->d[k][l]=add_nodelist_state( P->d[k][l], numst*(auxi->state)+(auxj->state));

                                #if DEBUG==1
                                printf("\tA:\t(q%dq%d,%d,(%c,%c))-> q%dq%d\n",i,j,k,SIGMA[a1],SIGMA[a2], auxi->state,auxj->state);
                                fflush(stdout);
                                #endif
  
                                auxj=auxj->next;
                            }
                            auxi=auxi->next;
                        }
                   }
                }              
                else if(a1<ns){ /*a2==ns*/

                   if(N->d[i][a1]!=NULL){

                        auxi=N->d[i][a1];
                        while(auxi!=NULL){
                            P->d[k][l]=add_nodelist_state( P->d[k][l], numst*( auxi->state ) + j );
         
                            #if DEBUG==1
                            printf("\tB:\t(q%dq%d,%d,(%c,%c))-> q%dq%d\n",i,j,k,SIGMA[a1],'-',auxi->state , j );
                            fflush(stdout);
                            #endif
                            
                            auxi=auxi->next;
                        }
                   }
                }
                else if(a2<ns){ /*a1==ns*/

                   if(N->d[j][a2]!=NULL){

                        auxj=N->d[j][a2];
                        while(auxj!=NULL){

                             P->d[k][l]=add_nodelist_state(P->d[k][l],numst*( i )+(auxj->state));

                             #if DEBUG==1
                             printf("\tC:\t(q%dq%d,%d,(%c,%c))-> q%dq%d\n",i,j,k,'-',SIGMA[a2], i ,auxj->state );
                             fflush(stdout);
                             #endif
  
                             auxj=auxj->next;
                        }
                   }    
                }
          }
          k++;
      }
   }
   return P;
}



/*************************************/
nfaprod *dfa2prod(dfa *D){
   nfaprod *P;
   int numst = D->numst;  
   int i,j,k,l;
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;
   int a1,a2;

   P = (nfaprod *)malloc(sizeof(nfaprod));
   nfaprod_init(P, D->numst, D->start, D->end);

   /* transitions */
   k=0;
   for(i=0;i<numst;i++){
      for(j=0;j<numst;j++){
          for(l=0;l<nsprod;l++){
                a1=l/ns1;
                a2=l%ns1;
                if( a1<ns && a2<ns ){
                   if((D->d[i][a1]>=0)&&(D->d[j][a2]>=0)){
                        P->d[k][l]=add_nodelist_state(P->d[k][l],numst*(D->d[i][a1])+(D->d[j][a2]));

                        #if DEBUG==1
                        printf("\tA:\t(q%dq%d,%d,(%c,%c))-> q%dq%d\n",i,j,k,SIGMA[a1],SIGMA[a2],D->d[i][a1],D->d[j][a2]);
                        fflush(stdout);
                        #endif
                   }
                }              
                else if(a1<ns){ /*a2==ns*/

                   if(D->d[i][a1]>=0){
                        P->d[k][l]=add_nodelist_state( P->d[k][l], numst*(D->d[i][a1]) + j );

                        #if DEBUG==1
                        printf("\tB:\t(q%dq%d,%d,(%c,%c))-> q%dq%d\n",i,j,k,SIGMA[a1],'-',D->d[i][a1], j );
                        fflush(stdout);
                        #endif
                   }
                }
                else if(a2<ns){ /*a1==ns*/

                   if(D->d[j][l%ns1]>=0){
                        P->d[k][l]=add_nodelist_state(P->d[k][l],numst*( i )+(D->d[j][a2]));

                        #if DEBUG==1
                        printf("\tC:\t(q%dq%d,%d,(%c,%c))-> q%dq%d\n",i,j,k,'-',SIGMA[a2], i ,D->d[j][a2]);
                        fflush(stdout);
                        #endif
                   }    
                }
          }
          k++;
      }
   }

   return P;
}

/*************************************/
int get_numtrans_nfaprod(nfaprod *P){
   nodelist_state *aux;
   int i,j;
   int numst = P->numst; 
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;
   int numtrans = 0;

   /* transitions */
   for(i=0;i<numst;i++){
      for(j=0;j<nsprod;j++){
         aux=P->d[i][j];
         while(aux!=NULL){     
              numtrans++;
              aux=aux->next;
         }/*end while*/
      }/*end for*/
   }/*end for*/   

   return numtrans; 
}


/*************************************/
void print_nfaprod_details(nfaprod *P){
 
   printf("NFAPROD: \n{\n"); 
   printf("states: %4i\n",P->numst);
   printf("transitions: %4i\n",get_numtrans_nfaprod(P) );
   printf("}\n");
 
   // printf("\n");

}

/*************************************/
void print_nfaprod_dot(nfaprod *P){
   nodelist_state *aux;
   int i,j;
   int numst = P->numst; 
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;

    printf("digraph finite_state_machine {\n");
    printf("   rankdir=LR;\n");
    printf("   edge [fontname=arial,fontsize=11]\n");

   aux=P->end;
   while(aux!=NULL){ 
      printf("   node [fontname=arial,fontsize=11,shape=doublecircle];q%iq%i;\n",
             P->states[aux->state][0], P->states[aux->state][1] );
      aux=aux->next;
   }

    printf("   node [shape=circle,size=10]\n");
    printf("   start [shape=point]\n");
    printf("   start -> q%iq%i\n",P->states[P->start][0], P->states[P->start][1]);

   /* print transitions */
   for(i=0;i<numst;i++){

      for(j=0;j<nsprod;j++){
         aux=P->d[i][j];
         while(aux!=NULL){     
            if( P->alphaall[i] && aux->state == i ){ /*if alphaall and auto transition*/
               if(j==0){  /*print all only one*/
                   printf("   q%iq%i -> q%iq%i [label=\"ALL\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[i][0], P->states[i][1]);
               }
            }
            else{
               if( (j/ns1) == ns ){
                   printf("   q%iq%i -> q%iq%i [label=\"(-,%c)\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[ aux->state][0], P->states[aux->state][1], SIGMA[j%ns1]);
               }
               else if( (j%ns1) == ns ){
                   printf("   q%iq%i -> q%iq%i [label=\"(%c,-)\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[aux->state][0], P->states[aux->state][1], SIGMA[j/ns1] );
               }
               else{
                   printf("   q%iq%i -> q%iq%i [label=\"(%c,%c)\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[aux->state][0], P->states[aux->state][1], SIGMA[j/ns1], SIGMA[j%ns1]);
               }
           }
           aux=aux->next;
         }/*end while*/
      }/*end for*/
   }/*end for*/ 
   printf("}\n");
}

/*************************************/
void fprint_nfaprod_dot(char *basename, char *suffix, nfaprod *P){

   FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
   char cmd[500000];

   nodelist_state *aux;
   int i,j;
   int numst = P->numst; 
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;

    fprintf(of,"digraph finite_state_machine {\n");
    fprintf(of,"   rankdir=LR;\n");
    fprintf(of,"   edge [fontname=arial,fontsize=11]\n");

   aux=P->end;
   while(aux!=NULL){ 
      fprintf(of,"   node [fontname=arial,fontsize=11,shape=doublecircle];q%iq%i;\n",
             P->states[aux->state][0], P->states[aux->state][1] );
      aux=aux->next;
   }

    fprintf(of,"   node [shape=circle,size=10]\n");
    fprintf(of,"   start [shape=point]\n");
    fprintf(of,"   start -> q%iq%i\n",P->states[P->start][0], P->states[P->start][1]);

   /* print transitions */
   for(i=0;i<numst;i++){
     for(j=0;j<nsprod;j++){
         aux=P->d[i][j];
         while(aux!=NULL){ 
            if( P->alphaall[i] && aux->state == i ){ /*if alphaall and auto transition*/
               if(j==0){  /*print all only one*/
                   fprintf(of,"   q%iq%i -> q%iq%i [label=\"ALL\"]\n",
                                 P->states[i][0], P->states[i][1],
                                   P->states[i][0], P->states[i][1]);
               }
            }
            else{

               if( (j/ns1) == ns ){
                   fprintf(of,"   q%iq%i -> q%iq%i [label=\"(-,%c)\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[ aux->state][0], P->states[aux->state][1], SIGMA[j%ns1]);
               }
               else if( (j%ns1) == ns ){
                   fprintf(of,"   q%iq%i -> q%iq%i [label=\"(%c,-)\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[aux->state][0], P->states[aux->state][1], SIGMA[j/ns1] );
               }
               else{
                   fprintf(of,"   q%iq%i -> q%iq%i [label=\"(%c,%c)\"]\n",
                                 P->states[i][0], P->states[i][1],
                                 P->states[aux->state][0], P->states[aux->state][1], SIGMA[j/ns1], SIGMA[j%ns1]);
               }
           }
           aux=aux->next;
         }
     } 
   } 
   fprintf(of,"}\n");

   fclose(of);

   /* dot command */
   sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
   puts(cmd);
   /* system(cmd); */
}

/*************************************/
int spaceall(int *v){
  int i,flag=1;

  for(i=0;i<ns;i++){
      if(v[ns*(ns+1)+i]==0) flag=0;
  }

  if(flag){
     for(i=0;i<ns;i++){
         v[ns*(ns+1)+i]=1;
     }
  }

  return flag;
}

/*************************************/
int allspace(int *v){
  int i,flag=1;

  for(i=0;i<ns;i++){
      if(v[i*(ns+1)+ns]==0) flag=0;
  }

  if(flag){
     for(i=0;i<ns;i++){
         v[i*(ns+1)+ns]=1;
     }
  }

  return flag;
}

/*************************************/
int letterall(int *v, int j){
  int i,flag=1;

  for(i=0;i<ns;i++){
      if(v[j*(ns+1)+i]==0) flag=0;
  }

  if(flag){
     for(i=0;i<ns;i++){
         v[j*(ns+1)+i]=1;
     }
  }

  return flag;
}

/*************************************/
int allletter(int *v, int j){
  int i,flag=1;

  for(i=0;i<ns;i++){
      if(v[i*(ns+1)+j]==0) flag=0;
  }

  if(flag){
     for(i=0;i<ns;i++){
         v[i*(ns+1)+j]=1;
     }
  }

  return flag;
}

/*************************************/
void fprint_nfaprod_1arc_dot(char *basename, char *suffix, nfaprod *P){

   FILE *of = fopen_suffix(basename, suffix, ".dot", "w");
   char cmd[500000];

   nodelist_state *aux;
   int i,j,k;
   int numst = P->numst; 
   int ns1 = ns+1;
   int nsprod = ns1*ns1 - 1;
   int *v,flag,w,sq,vz;
   char label[500000],cad[100000];
   
   v = (int *) malloc(nsprod*sizeof(int));
   memset(v, 0, nsprod*sizeof(int));


    fprintf(of,"digraph finite_state_machine {\n");
    fprintf(of,"   rankdir=LR;\n");
    fprintf(of,"   edge [fontname=arial,fontsize=12]\n");

   aux=P->end;
   while(aux!=NULL){ 
      fprintf(of,"   node [fontname=arial,fontsize=14,shape=doublecircle];q%iq%i;\n",
             P->states[aux->state][0], P->states[aux->state][1] );
      aux=aux->next;
   }

    fprintf(of,"   node [shape=circle,size=10]\n");
    fprintf(of,"   start [shape=point]\n");
    fprintf(of,"   start -> q%iq%i\n",P->states[P->start][0], P->states[P->start][1]);

   /* print transitions */
   for(i=0;i<numst;i++){
      for(k=0;k<numst;k++){
         memset(v, 0, nsprod*sizeof(int));

         flag=0;vz=0;

         /* states i for k */ 
         for(j=0;j<nsprod;j++){
             aux=P->d[i][j];
             while(aux!=NULL){ 
                if(k==aux->state){
                  if(v[j]==0){ 
                    v[j]=1;
                    flag++;
                    if(((j/ns1) == ns)||((j%ns1) == ns)) vz++;
                  }
                  break; 
                }
                aux=aux->next;
             } 
         }

         if( i==k && P->alphaall[i] ){
            fprintf(of,"   q%iq%i -> q%iq%i [label=\"ALL\"]\n",
                      P->states[i][0], P->states[i][1],
                      P->states[i][0], P->states[i][1]);
         }
         else 
 
         if(flag){

         if(flag==nsprod){
            fprintf(of,"   q%iq%i -> q%iq%i [label=\"ALL\"]\n",
                      P->states[i][0], P->states[i][1],
                      P->states[k][0], P->states[k][1]);
         }
         else if(vz==0&&flag==ns*ns){
            fprintf(of,"   q%iq%i -> q%iq%i [label=\"(all,all)\"]\n",
                      P->states[i][0], P->states[i][1],
                      P->states[k][0], P->states[k][1]);

         } 
         //else if(flag>100){
         //   fprintf(of,"   q%iq%i -> q%iq%i [label=\"%d CASES of %d\"]\n",
         //             P->states[i][0], P->states[i][1],
         //             P->states[k][0], P->states[k][1],flag,nsprod);
         //} 
         else{
           /* v: i to k */

           strcpy(label,"");
 
           if(spaceall(v)){
               strcat(label,"(-,all)"); 
               flag-=ns;
           }
           if(allspace(v)){
               strcat(label,"(all,-)"); 
               flag-=ns;
           }

           for(j=0;j<ns;j++){
              if(letterall(v,j)){
                  sprintf(cad,"(%c,all)",SIGMA[j]); 
                  strcat(label,cad);
                  flag-=ns;
              }
              if(allletter(v,j)){
                  sprintf(cad,"(all,%c)",SIGMA[j]); 
                  strcat(label,cad);
                  flag-=ns;
              }
           }

           if(strlen(label)) strcat(label,"\\n");
         
           if(flag){
              sq=0;while(6*sq*sq<flag) sq++;
              w=0;
   
              for(j=0;j<nsprod;j++){
                 if(v[j]){
                    if ( (j/ns1) == ns )       sprintf(cad,"(-,%c)",SIGMA[j%ns1]);
                    else if( (j%ns1) == ns )   sprintf(cad,"(%c,-)",SIGMA[j/ns1]);   
                    else                             sprintf(cad,"(%c,%c)",SIGMA[j/ns1], SIGMA[j%ns1]); 
                   
                    if (w%(sq)==sq-1) strcat(cad,"\\n");
                    w++;

                    strcat(label,cad);
                 }
              }
           }

           fprintf(of,"   q%iq%i -> q%iq%i [label=\"%s\"]\n",
                    P->states[i][0], P->states[i][1],
                    P->states[k][0], P->states[k][1], label);

         }

         }/* if(flag) */
      }
   } 
   fprintf(of,"}\n");

   fclose(of);

   /* dot command */
   sprintf(cmd,"/usr/bin/dot -Tpng -o %s%s.png %s%s.dot",basename,suffix,basename,suffix);
   /* puts(cmd);   */
   /* system(cmd); */

}

/*************************************/
