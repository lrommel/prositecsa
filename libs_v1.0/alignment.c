/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "alignment.h"

 

/*************************************/
void alignment_free(alignment *a){
  free(a->s);
  free(a->t);
  free(a->s_line);
  free(a->t_line);
  free(a);
}


/*************************************/

void scoring_free(int **M){
   im_free(M, ns); 
   free(SIGMA);
}

/*************************************/
int **load_scoring(int *pg, char *filename){
  int i,j;
  FILE *ifp;
  int **matrix;

  /* file input */
  ifp = fopen(filename, "r");

  if (ifp == NULL) {
      fprintf(stderr, "ERROR: can't open input file %s!\n",filename);
      exit(1);
  }

  /* read length alphabet */
  fscanf(ifp,"%d",&ns);

   /* allocate */
   SIGMA = (char *) malloc(ns+1);

  /* read alphabet */
  fscanf(ifp,"%s",SIGMA);
  
  if (!alphabet_success()) {
      fprintf(stderr, "ERROR: alphabet error!!\n");
      exit(1);
  }
 
  /* init score matrix */
  matrix = im_malloc(ns,ns);

  /* read score matrix */
  for(i=0;i<ns;i++){
    for(j=0;j<ns;j++){
         if(fscanf(ifp,"%d",&matrix[i][j])!=1){
            fprintf(stderr, "ERROR: incomplete score matrix!!\n");
            exit(1);
         }
    }
  }

  /* read score gap */
  if(fscanf(ifp,"%d",pg)!=1){
      fprintf(stderr, "ERROR: score gap not found!!\n");
      exit(1);
  }

  /* close file */
  fclose(ifp);

  return matrix;
}

/*************************************/
char *load_sequence(char *filename){
   int i;
   char flag=0;
   char *seq;

   seq = load_word(filename);

   for(i=0;i<strlen(seq);i++){
      if(!isLetter(seq[i])){
             flag=seq[i];
             break;
      }
   }   

   if(flag){
       printf("ERROR: sequence not in alphabet, letter: %c \n",flag);
       exit(0);
   }
   return seq;
}


/*************************************/
void print_alignment_details(alignment *a){
 
   printf("\n");
   printf(" alignment length: %d \n", a->length); 
   printf("                s: %s \n",a->s);
   printf("                t: %s \n",a->t);
   printf("\n");
   printf("s': %s \n",a->s_line);
   printf("t': %s \n",a->t_line);
   printf("\n");
   printf(" alignment length: %7d \n", a->length);
   printf("      total score: %7d \n", a->score);
   printf("\n");
   printf("( %d %d %d %d )\n", a->s1, a->t1, a->s2, a->t2);  fflush(stdout); 
   printf("\n");

   if(a->s1==0) printf(" regular expression start in s in the position %d : no symbol \n", a->s1-1);
   else         printf(" regular expression start in s in the position %d : symbol is s[%d] = %c  \n", a->s1-1, a->s1-1, a->s[a->s1-1]);

   if(a->t1==0) printf(" regular expression start in t in the position %d : no symbol \n", a->t1-1);
   else         printf(" regular expression start in t in the position %d : symbol is t[%d] = %c  \n", a->t1-1, a->t1-1, a->t[a->t1-1]);

   printf(" regular expression   end in s in the position %d : symbol is s[%d] = %c  \n", a->s2-1, a->s2-1, a->s[a->s2-1]);
   printf(" regular expression   end in t in the position %d : symbol is t[%d] = %c  \n", a->t2-1, a->t2-1, a->t[a->t2-1]);
   printf("\n");

   /************************************************/ 

   int min = a->cut1 , max = a->cut2;

   /************************************************/ 
   printf("\n");
   printf("        s': %s \n",a->s_line);
   printf("        t': %s \n",a->t_line);
   printf("\n\n Alignment: 0  --- | %d  ---- %d | ---- %d \n", min, max, a->length-1 ); 
 
}


/*************************************/
void alignment_set_cuts(alignment *a, int** M, int g){
   int i;

   /************************************************/ 

   int min=1000000, max=-1000000, c;

   /************************************************/ 
 
   #if DEBUG==1  
   printf("\n( %d %d %d %d )\n", a->s1, a->t1, a->s2, a->t2);   
   fflush(stdout);
   #endif

   #if DEBUG==1  
   printf("( min: %d max:%d )\n", min, max);  
   #endif 
   for(c=0, i=0; i < a->length; i++){
      if( a -> s_line[i] != '-' )  c++;
      if( c == a -> s1 )  { min = i; break; }

   }
   #if DEBUG==1  
   printf("( min: %d max:%d )\n", min, max);  
   #endif 
   for(c=0, i=0; i < a->length; i++){
      if( a -> t_line[i] != '-' )  c++;
      if( c == a -> t1 )  { if ( min > i ) min = i; break; }

   }
   #if DEBUG==1  
   printf("( min: %d max:%d )\n", min, max);  
   #endif  
   for(c=0, i=0; i < a->length; i++){
      if( a -> s_line[i] != '-' )  c++;
      if( c == a -> s2 )  { max = i; break; }

   }
   #if DEBUG==1  
   printf("( min: %d max:%d )\n", min, max);  
   #endif    
   for(c=0, i=0; i < a->length; i++){
      if( a -> t_line[i] != '-' )  c++;
      if( c == a -> t2)  { if ( max < i ) max = i; break; }

   }
   #if DEBUG==1  
   printf("( min: %d max:%d )\n", min, max);  
   #endif  

   a->cut1 = min;
   a->cut2 = max; 

}

/*************************************/
void print_alignment_horizontal(alignment *a, int** M, int g){
   int i;

   /************************************************/ 

   int min = a->cut1 , max = a->cut2;

   /************************************************/
   /* horizontal view                              */
   /************************************************/

   printf("\n");    

   /************************************************/ 

   printf("            "); 
   if( min == 0 )  printf("+");              
   for(i=0; i < a->length; i++){
      if ( i > 0 && i == min )               printf("+"); 
      if ( i <  min )                        printf(" "); 
      else if ( i >=  min  && i <= max )     printf("-");
      else if ( i >  max )                   printf(" "); 
      if ( i < a->length - 1  &&  i == max ) printf("+");
   }
   if( max == i - 1 ) printf("+");              
   printf("\n"); 

   /************************************************/ 
   
   /* print s */ 
   printf("        s': ");
   if( min == 0 )  printf("|");              
   for(i=0; i<a->length; i++){
      if ( i > 0 && i == min ) printf("|"); 
      printf("%c",a->s_line[i]);   
      if ( i == max )          printf("|");
   }
   if( max == i ) printf("+");              
   printf("\n");    

   /* print t */ 
   printf("        t': ");
   if( min == 0 )  printf("|");              
   for(i=0; i<a->length; i++){
      if ( i > 0 && i == min ) printf("|"); 
      printf("%c",a->t_line[i]);   
      if ( i == max )          printf("|");
   }
   if( max == i ) printf("+");              
   printf("\n");   

   /************************************************/ 

   printf("            "); 
   if( min == 0 )  printf("+");              
   for(i=0; i < a->length; i++){
      if ( i > 0 && i == min )               printf("+"); 
      if ( i <  min )                        printf(" "); 
      else if ( i >=  min  && i <= max )     printf("-");
      else if ( i >  max )                   printf(" "); 
      if ( i < a->length - 1  &&  i == max ) printf("+");
   }
   if( max == i - 1 ) printf("+");              
   printf("\n"); 

   /************************************************/

}  
   
/*************************************/
void print_alignment_vertical(alignment *a, int** M, int g){
   int i,   score, total_score;
  
   /************************************************/ 

   int min = a->cut1 , max = a->cut2;

   /************************************************/ 

   printf("\n");
   printf("cut 1: %d\n", min   ); fflush(stdout); 
   printf("cut 2: %d\n", max   ); fflush(stdout); 
 
   /************************************************/
   /*  vertical view                               */
   /************************************************/

   total_score = 0;

   printf("\n");    
   
   /************************************************/ 
   printf("          t' s'                score\n");   
              

   if( min == 0 ) printf("       +--------+-------------------\n");   
   else           printf("       -----------------------------\n");             
   
   for(i=0; i < a->length; i++){
 
      if ( ( i > 0 && i == min) || ( i < a->length && i == max + 1 ) ) printf("       +--------+\n");

      printf("%4d:", i+1);  

      if ( i >=  min  && i <= max ) printf("  |"); 
      else                          printf("   ");   
 
      printf("%3c",a->t_line[i]);      
      printf("%3c",a->s_line[i]);     

      if ( i >=  min  && i <= max )   printf("  |"); 
      else                            printf("   ");
   
      if ( a->t_line[i] == a->s_line[i] ) { 
        printf("     MATCH: "); 
        score = M [ char2index( a->s_line[i] ) ] [ char2index( a->t_line[i] ) ] ; 
      }
      else if ( a->t_line[i] == '-' || a->s_line[i] == '-' ) { 
        printf("       GAP: "); 
        score = g ; 
      } 
      else { 
        printf("  MISMATCH: "); 
        score = M [ char2index( a->s_line[i] ) ] [ char2index( a->t_line[i] ) ] ; 
      }      
      printf("%5d", score  );
      
      total_score+=score;
 
      printf("\n");          
   }
 
   if( max == i - 1 )  printf("       +--------+-------------------\n");  
   else                printf("       -----------------------------\n");   
 
   printf("                Total score:%6d\n",total_score);   
   printf("\n");    
   /************************************************/  
   
   printf("\n");    
 
}

/*************************************/
void print_sequences(char *s, char *t){
   printf("\n");
   printf("s:\t[%s]\n",s);
   printf("t:\t[%s]\n",t);
}

/*************************************/

