/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "alphabet.h"
#include "utils.h"
#include "re.h"

/*************************************/
typedef struct _alignment{
  int s_gaps;
  int t_gaps;

  int score;
  int length;

  int s1;
  int t1;
  int s2;
  int t2;

  int cut1;  /* cut 1 in s_line and t_line */
  int cut2;  /* cut 2 in s_line and t_line */
  
  char* s_line;
  char* t_line;

  char* s;
  char* t;

  int s_start;
  int t_start;

} alignment;

/*************************************/

/*************************************/

void alignment_free(alignment *a);
 
void scoring_free(int **M);

int **load_scoring(int *pg, char *filename);
char *load_sequence(char *filename);

void alignment_set_cuts(alignment *a, int** M, int g);
void print_alignment_details(alignment *a);
void print_alignment_horizontal(alignment *a, int** M, int g);
void print_alignment_vertical(alignment *a, int** M, int g);
void print_sequences(char *s, char *t);
/*************************************/

#endif
