/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "recsa.h"

#if DEBUG==1
  #include "malloc_count.h"
#endif

/*************************************/
dp_states_container **dp_matrix_allocate(int X,int Y){
  dp_states_container **matrix;
  int i;
  matrix = ( dp_states_container ** ) malloc((X+1)*sizeof( dp_states_container * ));
  if (!matrix) {
	perror("malloc matrix in dp_matrix_allocate");
	exit(errno);
  }

  for (i=0; i<X; i++) {
      matrix[i] = ( dp_states_container * ) malloc((Y+1)*sizeof( dp_states_container ));
      if (!matrix[i]) {
	perror("malloc matrix[i] in dp_matrix_allocate");
	exit(errno);
      }
  }
  return matrix;
}

/*************************************/
void dp_matrix_free(dp_states_container **A, int X, int Y ){
  int i;

  dp_matrix_vectors_free(  A,   X,   Y  );

  for (i=0; i<X; i++){
     free(A[i]);
  }
  free(A);
}


/*************************************/
void  dp_matrix_vectors_allocate( dp_states_container **A, int X, int Y, int numst ){
  int i,j;
  for (i=0; i<X; i++) {
     for (j=0; j<Y; j++) {
           A[i][j].state =   ( dp_state * ) malloc( (numst+1) * sizeof( dp_state ) );
           if (! A[i][j].state ) {
	         perror("malloc A[i][j].state  in dp_matrix_vectors_allocate");
	         exit(errno);
           }
     }
  }

}      

/*************************************/
void  dp_matrix_vectors_free( dp_states_container **A, int X, int Y  ){
  int i,j;

  if (!A) {
	perror("dp_matrix_vectors_free : matrix is null");
	exit(errno);
  }


  for (i=0; i<X; i++) {
     for (j=0; j<Y; j++) {
           free( A[i][j].state );
     }
  }

}


/*************************************/
void  dp_cell_matrix_init( dp_states_container *cell, int id){

      /* init IDs */
      cell -> state[id].id = id;

      /* init score */
      cell -> state[id].score = INT_MIN;

      /* init positions */
      cell -> state[id].s1 = INT_MIN;
      cell -> state[id].s2 = INT_MIN;
      cell -> state[id].t1 = INT_MIN;
      cell -> state[id].t2 = INT_MIN;

}

/*************************************/
void  dp_matrix_1vector_init( dp_states_container **A, int i, int j, int numst){
  int k;

  for (k=0; k < numst; k++) {

      #if DEBUG==5  
        printf("function: dp_matrix_vectors_init : k = %d \n", k);
        fflush(stdout);
      #endif

      /*
      // init IDs 
      A[i][j].state[k].id = k;

      // init score  
      A[i][j].state[k].score = INT_MIN;

      // init positions  
      A[i][j].state[k].s1 = INT_MIN;
      A[i][j].state[k].s2 = INT_MIN;
      A[i][j].state[k].t1 = INT_MIN;
      A[i][j].state[k].t2 = INT_MIN;
      */

      dp_cell_matrix_init( &A[i][j], k);

  }

}   
 
/*************************************/
void  dp_matrix_1vector_init_list( dp_states_container **A, int i, int j, int P_numst,  nodelist_state  **active_s, nodelist_state  **active_t, int x1, int x2, int N_numst ){
  int k;
  nodelist_state *aux1,*aux2; /* states */

 
   /*  list of active states */
   if ( active_s != NULL && active_t != NULL ){
        aux1 = active_s[x1];
        while(aux1!=NULL){
            aux2 = active_t[x2];
            while(aux2!=NULL){
                k = N_numst*(aux1->state) + aux2->state;
                dp_cell_matrix_init( &A[i][j], k);
                aux2=aux2->next;
            } 
            aux1=aux1->next;
        } 
   }
   else{
        for (k=0; k < P_numst; k++) {
            dp_cell_matrix_init( &A[i][j], k);
        }
   }
}
/*************************************/
void  dp_matrix_vectors_init( dp_states_container **A, int X, int Y, int numst){
  int i,j;
  for (i=0; i<X; i++) {
     for (j=0; j<Y; j++) {
         dp_matrix_1vector_init(A, i, j, numst);
     }
  }
}   

/*************************************/
/*************************************/
/*************************************/
/*************************************/
/*************************************/
/*************************************/

 


/*************************************/
int symbolcost_prod(int a1, int a2, int** M, int g){
   if( a1==ns || a2==ns ) return g;
   else                   return M[a1][a2];  /*M[a1][a2]*/
}


/*************************************/
int max_state_end_active_list( nfaprod *P, dp_states_container **A, int i, int j, nodelist_state  **active_s, nodelist_state  **active_t, int numst , int x1, int x2 ){
   int qmax = INT_MIN, tmp;
   dp_state  *v = A[i][j].state;
   nodelist_state *aux,*aux1,*aux2; /* states */
   if ( active_s != NULL && active_t != NULL ){
	   aux1 = active_s[x1];
	   while(aux1!=NULL){
		   aux2 = active_t[x2];
		   while(aux2!=NULL){/* add in list*/
			   tmp = numst*(aux1->state) + aux2->state;
			   /* search in final states */
			   aux = P->end;
			   while(aux!=NULL){ /* aux->state */
				   if( tmp == aux->state ){
					   if( v[tmp].score > INT_MIN ){  /* is active state */
						   if( (qmax==INT_MIN) || ( v[tmp].score > v[qmax].score ) ) qmax = tmp;  
					   }
					}
			       aux=aux->next;
			   } 			   
			   aux2=aux2->next;
		   } 
		   aux1=aux1->next;
	   } 
   }
   else{
		qmax = max_state_end(P, A[i][j].state, P->numst ); 
   }
   return qmax;
}

/*************************************/
int max_state_end(nfaprod *P, dp_state  *v, int n){
   int qmax=INT_MIN;
   nodelist_state *aux; /* states */

   aux = P->end;
   while(aux!=NULL){ 
       /* aux->state */
 
       #if DEBUG==3000
       printf("\tend: q%dq%d %d\n", P->states[aux->state][0], P->states[aux->state][1] , v[aux->state].score);
       #endif

       if( v[aux->state].score > INT_MIN ){  /* is active state */
           if( (qmax==INT_MIN) || ( v[aux->state].score > v[qmax].score ) ) qmax = aux->state;  
       }

       aux=aux->next;
   } 

   return qmax;
}
 
/*************************************/
void arslan_transision_savePosition (nfaprod *P, dp_state  *ori,  dp_state  *des, int a1, int a2, int** M, int g, int x1, int x2, int x3, int x4, nodelist_state  **active_s, nodelist_state  **active_t, int numst ,  int *lista_actives , char type){
   int i,k;
   nodelist_state *aux,*aux1,*aux2; /* states */
   int cost;
   int s1, t1, s2, t2;
   int na;
 
   #if DEBUG==1
   printf("\n  %c ---> (%d,%d)->(%d,%d) numst: %d  >>>>  ",type,  x1, x2, x3, x4, numst);
   if (a1==ns )       printf("\t(-,%c)\n",SIGMA[a2]);
   else if (a2==ns )  printf("\t(%c,-)\n",SIGMA[a1]);
   else               printf("\t(%c,%c)\n",SIGMA[a1],SIGMA[a2]);
   #endif
 
   /* create list of active states: ori */
   na=0;
   if ( active_s != NULL && active_t != NULL ){
           aux1 = active_s[x1];
           while(aux1!=NULL){
               aux2 = active_t[x2];

               while(aux2!=NULL){

                   /* add in list*/
                   lista_actives[na] = numst*(aux1->state) + aux2->state;
                   na++;
                   
                   #if DEBUG==1
                   printf("\t\t---> add: (%d,%d) = %d \n", aux1->state, aux2->state, numst*(aux1->state) + aux2->state);
                   #endif

                   aux2=aux2->next;
               } 

               aux1=aux1->next;
           } 
   }
   else{
      na=P->numst;
      for(i=0;i<P->numst;i++){ 
           lista_actives[i] = i;
      }
   }

   #if DEBUG==1
      printf("\t active list created: Ok >>>  "); 
      /* print actives list*/
      printf("    (%d,%d)->(%d,%d) numst: %d { ", x1, x2, x3, x4, numst);   for(k=0; k<na; k++){  
        printf(" %d, ",lista_actives[k]);
      }
      printf(" }\n");   
   #endif


   /*
   #if DEBUG==10
     //going for each symbol in each state: origin Arslan   
     for(i=0;i<P->numst;i++){
         for(k=0; k<ns*ns-1; k++){ 
             if ( P->d[i][k] != NULL ){
                 //printf("state: %d , letter : ( %c, %c )", letter_inSigma(k/(ns+1)), letter_inSigma(k%(ns+1)) );
                 letter_inSigma(k/(ns+1)); 
                 letter_inSigma(k%(ns+1));
             }
         }
     }
   #endif
   */


   /* transision: building des */
   //for(i=0;i<P->numst;i++){  /* for each state in P */
   for(k=0; k<na; k++){  /* for each state in P */
     i=lista_actives[k];
     
     if(ori[i].score > INT_MIN ){  /* if actives states   */ 
           cost = ori[i].score + symbolcost_prod(a1, a2, M, g);

           #if DEBUG==1
               printf("\n ==============> is active : ");
               printf("( %c, %c ) ==> na= %d, ori[ %d ] = %d, ( %d, %d ) => ( %d, %d )  \n", 
                        letter_inSigma(a1), 
                        letter_inSigma(a2), 
                        na, i, 
                        ori[i].score, 
                        x1, x2, x3, x4 );
              fflush(stdout);
           #endif

           /* positions to be propagated: from origin state*/
           s1 = ori[i].s1;
           t1 = ori[i].t1;
           s2 = ori[i].s2;
           t2 = ori[i].t2;

           #if DEBUG==1
                printf(" propagate ===========================================>  \n"); 
                printf(" propagate ==>          ori[%d].score : %d\n", i, ori[i].score );  
                printf(" propagate ==>          ori[%d].s1    : %d\n", i, ori[i].s1 );  
                printf(" propagate ==>          ori[%d].t1    : %d\n", i, ori[i].t1 );  
                printf(" propagate ==>          ori[%d].s2    : %d\n", i, ori[i].s2 );  
                printf(" propagate ==>          ori[%d].t2    : %d\n", i, ori[i].t2 );  
                printf(" propagate ===========================================>  \n");fflush(stdout);
           #endif
       
           /*
           s1 = t1 = s2 = t2 =  INT_MIN; 
           if ( ori[i].s1 > INT_MIN  ){  // exists s1 and t1 to propagate  
                    s1 = ori[i].s1;
                    t1 = ori[i].t1;
                    if ( ori[i].s2 > INT_MIN  ){  // exists s2 and t2 to propagate 
                         s2 = ori[i].s2;
                         t2 = ori[i].t2;
                    }
           }
           */ 


           aux = P->d[i][a1*(ns+1)+a2];

           #if DEBUG==1
           if (aux==NULL)  printf(" P->d[%d][%d:(%c,%c)] is NULL \n",i, a1*(ns+1)+a2 , 
                                    letter_inSigma(a1), 
                                    letter_inSigma(a2)   );
           #endif

           while(aux!=NULL){

           #if DEBUG==1
           printf("\t\t\t\t  destination_state = %d , cost = %d , des[%d].score = %d  \n",  aux->state, cost, aux->state, des[aux->state].score  );
           #endif
               /* aux->state */
               if(cost > des[aux->state].score) {
                    des[aux->state].score = cost;

                    #if DEBUG==1
                       printf("\t\t\t\t  destination_state updated:  des[%d].score = cost = %d \n", aux->state, des[aux->state].score );
                    #endif

                    /* positions to be propagated: destination state*/
                    if ( s1 == INT_MIN) {
                        if ( des[ aux->state].id != P->start ){
                            if (type =='H'){
                               s1 = x3 + 1;
                               t1 = x4;
                            }
                            else if (type =='D'){
                               s1 = x3;
                               t1 = x4; 
                             } 
                            else if (type =='V'){  /* RRN 08 july 2018:  H -> V*/
                                s1 = x3;
                               t1 = x4 + 1; 
                            }
                        }
                        if ( isFinal(P, des[ aux->state].id)  ){  /* RRN 03 july 2018*/
                            s2 = x3;
                            t2 = x4;
                        } 
                    }
                    else if ( s2 == INT_MIN) {
                         if ( isFinal(P, des[ aux->state].id)  ){  /* RRN 03 july 2018*/
                            s2 = x3;
                            t2 = x4; 
                            /*
                            if (type =='H'){
                               s2 = x3;
                               t2 = x4; 
                            }
                            else if (type =='D'){
                               s2 = x3;
                               t2 = x4; 
                             } 
                            else if (type =='H'){
                               s2 = x3;
                               t2 = x4; 
                            }
                            */                                  
                         }
                    }

                    des[aux->state].s1 = s1;
                    des[aux->state].t1 = t1;
                    des[aux->state].s2 = s2;
                    des[aux->state].t2 = t2;

                    #if DEBUG==1
                        printf(" write propagate ===========================================>  \n"); 
                        printf(" write propagate ==>          des[%d].score : %d\n", i, des[i].score );  
                        printf(" write propagate ==>          des[%d].s1 : %d\n", i, des[i].s1 );  
                        printf(" write propagate ==>          des[%d].t1 : %d\n", i, des[i].t1 );  
                        printf(" write propagate ==>          des[%d].s2 : %d\n", i, des[i].s2 );  
                        printf(" write propagate ==>          des[%d].t2 : %d\n", i, des[i].t2 ); 
                        printf(" write propagate ===========================================>  \n");fflush(stdout);
                    #endif

                    #if DEBUG==1
                    printf("\t\ttransision: q%dq%d -> q%dq%d %d ( %d -> %d , %d )\n", 
                        P->states[i][0], P->states[i][1], 
                        P->states[aux->state][0], P->states[aux->state][1] , 
                        cost, i, aux->state,a1*(ns+1)+a2);
                    fflush(stdout);
                    #endif
                }
                else{
                    #if DEBUG==1
                    printf("\t\t   ignored: q%dq%d -> q%dq%d %d\n", 
                            P->states[i][0], P->states[i][1], 
                            P->states[aux->state][0], P->states[aux->state][1], cost);
                    fflush(stdout);
                    #endif
               }
               aux=aux->next;
           }       
     }
     else{
           #if DEBUG==5
                printf("NOT ACTIVE =====>na= %d, i= %d, ( %d, %d) => ( %d, %d)  \n",na, i, x1, x2, x3, x4 );
           #endif
     }
  }

}

 
/*************************************/
void arslan_transision(nfaprod *P, dp_state  *ori,  dp_state  *des, int a1, int a2, int** M, int g){
   int i;
   nodelist_state *aux; /* states */
   int cost;

   #if DEBUG==1
   if (a1==ns )       printf("\t(-,%c)\n",SIGMA[a2]);
   else if (a2==ns )  printf("\t(%c,-)\n",SIGMA[a1]);
   else               printf("\t(%c,%c)\n",SIGMA[a1],SIGMA[a2]);
   #endif

   /* transision: building des */
   for(i=0;i<P->numst;i++){  /* for each state in P */
     if(ori[i].score > INT_MIN ){  /* if actives states   */ 
           cost = ori[i].score + symbolcost_prod(a1, a2, M, g); 
           aux = P->d[i][a1*(ns+1)+a2];
           while(aux!=NULL){
               /* aux->state */
               if(cost > des[aux->state].score) {
                    des[aux->state].score = cost;

                    #if DEBUG==1
                    printf("\t\ttransision: q%dq%d -> q%dq%d %d (%d -> %d , %d)\n", 
                            P->states[i][0], P->states[i][1], 
                            P->states[aux->state][0], P->states[aux->state][1] , 
                            cost, i, aux->state,a1*(ns+1)+a2);
                    fflush(stdout);
                    #endif
                }
                else{
                    #if DEBUG==1
                    printf("\t\t   ignored: q%dq%d -> q%dq%d %d\n", 
                            P->states[i][0], P->states[i][1], 
                            P->states[aux->state][0], P->states[aux->state][1], cost);
                    fflush(stdout);
                    #endif
               }
               aux=aux->next;
           }       
     }
  }
   
}

/*************************************/
int arslan_previus_transision(nfaprod *P, dp_state  *ori, int value, int q, int a1, int a2, int **M, int g){
   int i;
   nodelist_state *aux; /* states */
   int cost;
   int qprev=-1;

   #if DEBUG==1
   if (a1==ns )       printf("\t(-,%c)\n" ,SIGMA[a2]);
   else if (a2==ns )  printf("\t(%c,-)\n" ,SIGMA[a1]);
   else               printf("\t(%c,%c)\n",SIGMA[a1],SIGMA[a2]);
   #endif

   /* transision */
   for(i=0;i<P->numst;i++){  /* for each state in P */
      if(ori[i].score > INT_MIN ){  /* if actives states   */ 
           cost = ori[i].score + symbolcost_prod(a1, a2, M, g);

           #if DEBUG==1
           printf("\t\tativo: %d=q%dq%d %d (%d) \n", i, 
                  P->states[i][0], P->states[i][1], cost, value );
           fflush(stdout);
           #endif  

           if(cost == value){
               aux = P->d[i][a1*(ns+1)+a2];

               #if DEBUG==1  
               if (aux==NULL) printf("\t\tNULL transision \n");
               #endif

               while(aux!=NULL){ 

                   #if DEBUG==1   
                   printf("\t\t\t\tto q%dq%d (=q%dq%d) (%d -> %d , %d )\n", 
                         P->states[aux->state][0], P->states[aux->state][1],
                         P->states[q][0], P->states[q][1], 
                         i, aux->state, a1*(ns+1)+a2);
                   fflush(stdout);
                   #endif


                   /* aux->state */
                   if( aux->state == q ){

                        #if DEBUG==1  
                        printf("\t\t\t\t FOUNDED \n");
                        fflush(stdout);
                        #endif

                        qprev = i;
                        return qprev;
                   }
                   aux=aux->next;
               }
           } 
           else{
               #if DEBUG==1  
               printf("\t\t\tDIFF \n");
               fflush(stdout);
               #endif
           }      
      }
  }
  return qprev;
}



/*************************************/

dp_states_container **recsa_arslan(char* s, char* t,  nfaprod *P, int** M, int g, 
                                   nodelist_state  **active_s, nodelist_state  **active_t, int numst ) {

  /* matrix for dynamic programming */
  dp_states_container **A;

  /* indices for rows and columns, and the matrix */
  int n = strlen(s);
  int m = strlen(t);

  /* sequences how index to alphabet */
  int *si, *ti; 

  /* sequences */
  si = seq2index(s);
  ti = seq2index(t);

  #if DEBUG==1  
    printf("function: recsa_arslan \n");
    print_seq_index(si,n);
    print_seq_index(ti,m);
    fflush(stdout);
  #endif

  /* allocate matrix for dynamic programming */
  A = dp_matrix_allocate(n+1,m+1); 
  if (!A) return 0;
 
  /* alocate states vectors */
  dp_matrix_vectors_allocate( A, n+1, m+1, P->numst );

  #if DEBUG==1  
    printf("function: recsa_arslan : vectors allocated \n");
    fflush(stdout);
  #endif

  /* fills state vectors in the matrix: IDs, inactive(-infinity), positions(-infinity)   */
  dp_matrix_vectors_init(A, n+1, m+1, P->numst);

  /* init the one active state */
  A[0][0].state[P->start].score = 0;
 
  #if DEBUG==1  
    printf("A[0][0].state[P->start].score = %d\n", A[0][0].state[P->start].score);
    printf("function: recsa_arslan : scores initialized \n");
    fflush(stdout);
  #endif

  /* dynamic programming */
  recsa_arslan_dp( si, ti, n, m, P,  A, M, g, active_s, active_t, numst );

  free(si);
  free(ti);
 
  return A;
  
}

/*************************************/
void  recsa_arslan_dp(int* si, int* ti,   int n, int m,   nfaprod *P,  dp_states_container **A,  int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst  ) {
 
  int i=0, j=0 ;
  int *lista_actives = malloc ( P->numst * sizeof ( int )  );

  #if DEBUG==1  
    printf("(*,-)\n");fflush(stdout);
  #endif

  for (i=1; i<=n; i++){  

    #if DEBUG==1  
      printf("  %d\n",i);fflush(stdout);
    #endif

    //arslan_transision(P, A[i-1][0].state, A[i][0].state, si[i-1] , ns , M, g); /* gap */
    arslan_transision_savePosition (P, A[i-1][0].state, A[i][0].state, si[i-1] , ns , M, g,
                                    i-1, 0, i, 0, active_s, active_t, numst, lista_actives, 'V'  ); /* gap vertical */
  }

  #if DEBUG==1  
    printf("(-,*)\n");
    fflush(stdout);
  #endif

  for (j=1; j<=m; j++){

    #if DEBUG==1  
    printf("  %d\n",j);fflush(stdout);
    #endif

    //arslan_transision(P, A[0][j-1].state, A[0][j].state, ns , ti[j-1] , M, g); /* gap */
    arslan_transision_savePosition(P, A[0][j-1].state, A[0][j].state, ns , ti[j-1] , M, g, 
                                   0, j-1, 0, j, active_s, active_t, numst, lista_actives, 'H'  ); /* gap horizontal */
  }


  /* dynamic programming */
  for (i=1; i<n+1; i++) {
    for (j=1; j<m+1; j++) {

 
        #if DEBUG==1  
          printf("--------------------\n");
          printf("( %d, %d )\n", i,j);
          printf("--------------------\n");
          fflush(stdout);
        #endif

        arslan_transision_savePosition(P, A[i-1][j-1].state, A[i][j].state, si[i-1], ti[j-1] , M, g, 
                                       i-1, j-1 , i, j, active_s, active_t , numst , lista_actives , 'D' );  /* dia  */
        arslan_transision_savePosition(P, A[i-1][j  ].state, A[i][j].state, si[i-1], ns      , M, g, 
                                       i-1, j   , i, j, active_s, active_t , numst , lista_actives , 'V'  );  /* up   */
        arslan_transision_savePosition(P, A[i  ][j-1].state, A[i][j].state, ns , ti[j-1]     , M, g, 
                                       i  , j-1 , i, j, active_s, active_t , numst , lista_actives , 'H' );  /* left */
    }
  }
 
  free(lista_actives);

}

/*************************************/

dp_states_container **recsa_arslan_1line(char* s, char* t,  nfaprod *P, int** M, int g, 
                                   nodelist_state  **active_s, nodelist_state  **active_t, int numst ) {

  /* matrix for dynamic programming */
  dp_states_container **A;

  /* indices for rows and columns, and the matrix */
  int n = strlen(s);
  int m = strlen(t);

  /* sequences how index to alphabet */
  int *si, *ti; 

  /* sequences */
  si = seq2index(s);
  ti = seq2index(t);

  #if DEBUG==1  
    printf("function: recsa_arslan \n");
    print_seq_index(si,n);
    print_seq_index(ti,m);
    fflush(stdout);
  #endif

  //int ROWS = n+1, COLS = m+1;   /* complete matrix */
  int ROWS = 1, COLS = m+3;   /* 1 line */

  /* allocate matrix for dynamic programming */
  A = dp_matrix_allocate( ROWS, COLS );    /* 1 line */
  if (!A) return 0;
 

  #if DEBUG==1
    fprintf(stderr,"alocate states vectors, begin : "); malloc_count_print_status(); fflush(stderr);
  #endif
 
  /* alocate states vectors */
  dp_matrix_vectors_allocate( A, ROWS, COLS, P->numst );

  #if DEBUG==1
    fprintf(stderr,"alocate states vectors, end : "); malloc_count_print_status(); fflush(stderr);
  #endif

 
  #if DEBUG==1  
    printf("function: recsa_arslan : vectors allocated \n");
    fflush(stdout);
  #endif
 
  /* dynamic programming */
  recsa_arslan_dp_1line( si, ti, n, m, P,  A, M, g, active_s, active_t, numst );

  free(si);
  free(ti);
 
  return A;
  
}

/*************************************/
void  recsa_arslan_dp_1line(int* si, int* ti,   int n, int m,   nfaprod *P,  dp_states_container **A,  int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int N_numst  ) {
 
  int i=0, j=0;
  int *lista_actives = malloc ( P->numst * sizeof ( int )  );
  int zero, jmax, jcurr, jprev;
  int COLS = m+3;   /* 1 line */

  /* first column, init (0,0) */
  //dp_matrix_1vector_init   (A, 0, 0, P->numst);    /* init only A[0][0], inactive(-infinity), positions(-infinity) */
  dp_matrix_1vector_init_list(A, 0, 0, P->numst, active_s, active_t,  0, 0, N_numst );


  A[0][0].state[P->start].score = 0;            /* init (0,0) the one active state */

   #if DEBUG==1  
      printf("\n--------------------\n");
      printf("A[0][0].state[P->start].score = %d, P->start = %d \n",  A[0][0].state[P->start].score, P->start );
      printf("--------------------\n");
      fflush(stdout);
    #endif

  /* horizontal gaps */
  zero  = 0;  /* init delay in circular list: A[0][0] */
  jcurr = ( zero  + 1 ) % COLS;
  jmax  = ( jcurr + m ) % COLS;   /* jmax defined as first position out range */
  for (j = 1 ; jcurr!=jmax ; jcurr = ( jcurr + 1 ) % COLS, j++ ){

   #if DEBUG==1  
      printf("--------------------\n");
      printf("horizontal gap, row 0 (-,*) : ( jcurr=%d )  jprev_left=%d \n",  jcurr,  ( jcurr - 1 + COLS) % COLS  );
      printf("--------------------\n");
      fflush(stdout);
    #endif

 
    /* horizontal gap, previos cell = jcurr - 1 */
    jprev = ( jcurr - 1 + COLS) % COLS;
    //dp_matrix_1vector_init   (A, 0, jcurr, P->numst);   /* init only A[0][j], inactive(-infinity), positions(-infinity)  */
    dp_matrix_1vector_init_list(A, 0, jcurr, P->numst, active_s, active_t, i, j, N_numst );
    arslan_transision_savePosition(P, A[0][jprev].state, A[0][jcurr].state, ns , ti[j-1] , M, g, 0, j-1, 0, j, active_s, active_t, N_numst, lista_actives, 'H'  );  
 
  }

 
  /* Dynamic programming */
  for (i=1; i<n+1; i++) {

    #if DEBUG==1  
      printf("\n\n--------------------\n");
      printf("ROW i = %d\n",i);
      printf("--------------------\n");fflush(stdout);
    #endif  

    /* first column (j), init with vertical gap */
    jprev = ( jcurr - m - 1 + COLS ) % COLS;           /* vertical gap in first column (j), previos cell = j - m       */
 
    //dp_matrix_1vector_init   (A, 0, jcurr, P->numst);     /* init only A[0][0], inactive(-infinity), positions(-infinity) */
    dp_matrix_1vector_init_list(A, 0, jcurr, P->numst, active_s, active_t, i, 0, N_numst );
    #if DEBUG==1  
      printf("--------------------\n");
      printf("BEGIN vertical gap, col 0 : (*,-) i=%d\n",i);
      printf("--------------------\n");fflush(stdout);
    #endif  
    arslan_transision_savePosition (P, A[0][jprev].state, A[0][jcurr].state, si[i-1] , ns , M, g, i-1, 0, i, 0, active_s, active_t, N_numst, lista_actives, 'V'  );  /* vertical gaps */
    #if DEBUG==1  
      printf("--------------------\n");
      printf("END vertical gap, col 0 : (*,-) i=%d\n",i);
      printf("--------------------\n");fflush(stdout);
    #endif 
 
    /* steps of dynamic programming */
    zero  = jcurr;  /* init delay in circular list: A[0][j] */
    jcurr = ( zero  + 1 ) % COLS;
    jmax  = ( jcurr + m ) % COLS;
    for (j = 1 ; jcurr!=jmax ; jcurr = ( jcurr + 1 ) % COLS, j++ ){

        #if DEBUG==1
          if(i==n){  
            printf("\n +------------------\n");
            printf(" |  ( i=%d, j=%d ) ( jcurr=%d ) jprev_dia=%d  jprev_up=%d  jprev_left=%d \n", i, j,  jcurr, ( jcurr - m - 2 + COLS) % COLS, ( jcurr - m - 1 + COLS) % COLS, ( jcurr - 1 + COLS) % COLS  );
            printf(" +------------------\n");
            fflush(stdout);
          }
        #endif

 
        //dp_matrix_1vector_init   (A, 0, jcurr, P->numst);  /* init only A[0][j], inactive(-infinity), positions(-infinity)  */
        dp_matrix_1vector_init_list(A, 0, jcurr, P->numst, active_s, active_t, i, j, N_numst );

        /* dia  ****************************/
        jprev = ( jcurr - m - 2 + COLS) % COLS;
        arslan_transision_savePosition(P, A[0][jprev].state, A[0][jcurr].state, si[i-1], ti[j-1] , M, g,  i-1, j-1 , i, j, active_s, active_t , N_numst , lista_actives , 'D' ); 
 

        /* up   ****************************/
        jprev = ( jcurr - m - 1 + COLS) % COLS;
        arslan_transision_savePosition(P, A[0][jprev].state, A[0][jcurr].state, si[i-1], ns      , M, g,  i-1, j   , i, j, active_s, active_t , N_numst , lista_actives , 'V'  );  
 
 
        /* left ****************************/
        jprev = ( jcurr - 1 + COLS) % COLS;
        arslan_transision_savePosition(P, A[0][jprev].state, A[0][jcurr].state, ns , ti[j-1]     , M, g,  i  , j-1 , i, j, active_s, active_t , N_numst , lista_actives , 'H' );  
 
    }
  }
 
  free(lista_actives);

}

/*************************************/
alignment* recsa_arslan_recovery(char* s, char* t, nfaprod *P,   dp_states_container **A,  int** M, int g ) {
 
  int i=0, j=0 , qmax, qprev;

  /* indices for rows and columns, and the matrix */
  int n = strlen(s);
  int m = strlen(t);

  /* sequences how index to alphabet */
  int *si = seq2index(s);
  int *ti = seq2index(t);
 

  /* Searches for maximum score: */
  int imax = n;
  int jmax = m;
 

  /* Gets best alignment: */
  int l = n>m ? 2*n+2 : 2*m+2;
  char* as = malloc(l*sizeof(char));  /* A buffer for the aligned portion of s. */
  char* at = malloc(l*sizeof(char));  /* idem, t.                               */
  int al;

  if (!as || !at) {

    #if DEBUG==1  
      printf("\n\n\n\n");
      printf("--------------------- \n");
      printf(" ERROR  if (!as || !at)  \n");
      printf("--------------------- \n");
      fflush(stdout);
    #endif

    return 0;
  }

  alignment* align = malloc(sizeof(alignment));
  if (!align) {

    #if DEBUG==1  
      printf("\n\n\n\n");
      printf("--------------------- \n");
      printf(" ERROR  align = malloc \n");
      printf("--------------------- \n");
      fflush(stdout);
    #endif

   /* print alignament */
    free(as);
    free(at);
    return 0;
  }

  align->s_gaps = 0;
  align->t_gaps = 0;

  as[l-1] = 0;
  at[l-1] = 0;

  i = imax;
  j = jmax;
  al = l-2;

  
  qmax = max_state_end(P, A[i][j].state, P->numst );
  if(qmax==INT_MIN){
     printf("No alignment: %d\n\n", qmax);fflush(stdout);
     return 0;
  }



  align->score = A[imax][jmax].state[qmax].score;
  align->s1 = A[imax][jmax].state[qmax].s1;
  align->t1 = A[imax][jmax].state[qmax].t1;
  align->s2 = A[imax][jmax].state[qmax].s2;
  align->t2 = A[imax][jmax].state[qmax].t2;

  /* correction s2 and t2 */
  if ( align->s2 == INT_MIN ){
    align->s2 = strlen(s)-1;
  }
  if ( align->t2 == INT_MIN ){
    align->t2 = strlen(t)-1;
  }

  #if DEBUG==1  
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf(" RECOVERY \n");
    printf("--------------------- \n");
    print_seq_index(si,n);
    print_seq_index(ti,m);
    fflush(stdout);
  #endif


 qprev = -1;

  while (!(i==0&&j==0)) { 

      #if DEBUG==1
        printf("\n\n----------------------------------\n");
        printf("REC: (%d,%d) (%c,%c) BACK(q%dq%d)\n", i, j, 
               si[i-1]!=ns?SIGMA[si[i-1]]:'?', ti[i-1]!=ns?SIGMA[ti[j-1]]:'?' , 
               P->states[qmax][0], P->states[qmax][1]);
        printf("----------------------------------1111\n");
        fflush(stdout);
      #endif


      if (i>0 && j>0){
            #if DEBUG==1
              printf("\n\n-------------- DIA BEGIN --------------\n");fflush(stdout);
            #endif

            qprev = arslan_previus_transision(P, A[i-1][j-1].state, A[i][j].state[qmax].score , qmax , si[i-1], ti[j-1], M, g); // dia

            #if DEBUG==1
              printf("-------------- DIA FINISH --------------\n");fflush(stdout);
            #endif

            if ( qprev != -1 ) { 

                #if DEBUG==1
                  printf("DIA BACK TRANSISION: q%dq%d\n", P->states[qprev][0], P->states[qprev][1] );fflush(stdout);
                #endif

                as[al] = s[--i];
                at[al--] = t[--j];

                qmax = qprev;
                qprev = -1;
                continue;
            }
      }
      
      if (j>0) {
            #if DEBUG==1
              printf("\n\n-------------- LEFT BEGIN --------------\n");fflush(stdout);
            #endif

            qprev = arslan_previus_transision(P, A[i][j-1].state, A[i][j].state[qmax].score , qmax , ns , ti[j-1], M, g); //left

            #if DEBUG==1
              printf("-------------- LEFT FINISH --------------\n");fflush(stdout);
            #endif

            if( qprev != -1 ) {    
                #if DEBUG==1
                  printf("LEFT BACK TRANSISION: < q%dq%d > \n", P->states[qprev][0], P->states[qprev][1] );fflush(stdout);
                #endif

                as[al] = '-';
                at[al--] = t[--j];
                align->s_gaps++;

                qmax = qprev;
                qprev = -1;
                continue;
            }
     }

     if (i>0){
            #if DEBUG==1
              printf("\n\n-------------- TOP BEGIN -------------- \n");fflush(stdout);
            #endif

            qprev = arslan_previus_transision(P, A[i-1][j].state, A[i][j].state[qmax].score , qmax , si[i-1], ns , M, g);  // top

            #if DEBUG==1
              printf("-------------- TOP FINISH --------------\n");fflush(stdout);
            #endif


            if( qprev != -1 ) {   
                #if DEBUG==1
                  printf("TOP BACK TRANSISION: q%dq%d\n", P->states[qprev][0], P->states[qprev][1] );fflush(stdout);
                #endif

                as[al] = s[--i];
                at[al--] = '-';
                align->t_gaps++;

                qmax = qprev;
                qprev = -1;
                continue;

             }
             else{
                printf("ERRO: qprev=%d, BACK TRANSISION NOT FOUND\n",qprev);fflush(stdout);
                exit(0);
             }

    }

  }


  /* original sequences */
  align->s = malloc((1+strlen(s))*sizeof(char));
  strcpy(align->s, s );

  align->t = malloc((1+strlen(t))*sizeof(char));
  strcpy(align->t, t );


  /* save alignament */

  align->length = l-al-1;
 
  al++;
  align->s_line = malloc((1+align->length)*sizeof(char));
  memcpy(align->s_line,as+al,align->length*sizeof(char));

  align->t_line = malloc((1+align->length)*sizeof(char));
  memcpy(align->t_line,at+al,align->length*sizeof(char));

  align->length--;
  align->s_start = i;
  align->t_start = j;


  free(as);
  free(at);

  free(si);
  free(ti);

  return align;

}


/*************************************/
alignment* recsa_arslan_recovery_by_positions(char* s, char* t, nfaprod *P,   dp_states_container **A, int imax, int jmax,  int** M, int g, nodelist_state  **active_s, nodelist_state  **active_t, int numst ) {
 
  int i=0, j=0 , qmax ;

  /* sequences how index to alphabet */
  int *si = seq2index(s);
  int *ti = seq2index(t);
 

  alignment* align = malloc(sizeof(alignment));
  if (!align) {

    #if DEBUG==1  
      printf("\n\n\n\n");
      printf("--------------------- \n");
      printf(" ERROR  align = malloc \n");
      printf("--------------------- \n");
      fflush(stdout);
    #endif
 
    return 0;
  }

  align->s_gaps = 0;
  align->t_gaps = 0;

 
  i = imax;
  j = jmax;
 

  //qmax = max_state_end(P, A[i][j].state, P->numst );
  qmax = max_state_end_active_list( P, A, i, j, active_s, active_t, numst, strlen(s), strlen(t) );
  if(qmax==INT_MIN){
     printf("No alignment: %d\n\n", qmax);fflush(stdout);
     return 0;
  }



  align->score = A[imax][jmax].state[qmax].score;
  align->s1 = A[imax][jmax].state[qmax].s1;
  align->t1 = A[imax][jmax].state[qmax].t1;
  align->s2 = A[imax][jmax].state[qmax].s2;
  align->t2 = A[imax][jmax].state[qmax].t2;

  printf("--------------------- \n");
  printf("qmax = %d\n", qmax);  fflush(stdout);
  printf("--------------------- \n");

  #if DEBUG==1  
        printf(" ==>    A[imax][jmax].state[qmax].s1 : %d\n", A[imax][jmax].state[qmax].s1);fflush(stdout);
        printf(" ==>    A[imax][jmax].state[qmax].t1 : %d\n", A[imax][jmax].state[qmax].t1);fflush(stdout);
        printf(" ==>    A[imax][jmax].state[qmax].s2 : %d\n", A[imax][jmax].state[qmax].s2);fflush(stdout);
        printf(" ==>    A[imax][jmax].state[qmax].t2 : %d\n", A[imax][jmax].state[qmax].t2);fflush(stdout);
  #endif

  /* correction s2 and t2 */
  if ( align->s2 == INT_MIN ){
    align->s2 = strlen(s)-1;
  }
  if ( align->t2 == INT_MIN ){
    align->t2 = strlen(t)-1;
  }

  #if DEBUG==1  
    int n = strlen(s);
    int m = strlen(t);
    printf("\n\n\n\n");
    printf("--------------------- \n");
    printf(" RECOVERY \n");
    printf("--------------------- \n");
    print_seq_index(si,n);
    print_seq_index(ti,m);
    fflush(stdout);
  #endif

 

  /* original sequences */
  align->s = malloc((1+strlen(s))*sizeof(char));
  strcpy(align->s, s );

  align->t = malloc((1+strlen(t))*sizeof(char));
  strcpy(align->t, t );

  alignment* a1;
  alignment* a2;
  alignment* a3;

  int ls1,ls2,ls3, lt1, lt2, lt3;
  char *ps1, *ps2, *ps3, *pt1, *pt2, *pt3;
 
 

  /* part 1 */
  ls1 = align -> s1 - 1; 
  ps1 = malloc( ( 1 + ls1 ) * sizeof(char) );
  memcpy ( ps1, align->s , ( ls1 ) * sizeof(char) );
  ps1[ls1]='\0';
  lt1 = align -> t1 - 1;
  pt1 = malloc( ( 1 + lt1 ) * sizeof(char) );
  memcpy ( pt1, align->t , ( lt1 ) * sizeof(char) );
  pt1[lt1]='\0';

  #if DEBUG==1  
    printf("\n\n");
    printf("--------------------- \n");
    printf(" ==> s{%s}  t{%s}\n", align -> s, align -> t);
    printf(" ==> PART 1: ps1{%s} pt1{%s}\n", ps1, pt1);
    printf("--------------------- \n");
    printf(" ==>  strlen( s ) : %d\n", (int) strlen( align -> s ) );
    printf(" ==>           s1 : %d\n", align -> s1);
    printf(" ==>          ls1 : %d\n", ls1);
    printf(" ==>  strlen( t ) : %d\n", (int) strlen( align -> t ) );
    printf(" ==>           t1 : %d\n", align -> t1);
    printf(" ==>          lt1 : %d\n", lt1);
    fflush(stdout);
  #endif
 
  a1 = global_align(ps1, pt1,  M,  g) ;



  /* part 2 */
  ls2 = align -> s2 - align -> s1 + 1;
  ps2 = malloc( ( 1 + ls2 ) * sizeof(char) );
  memcpy ( ps2, align->s + align -> s1 - 1 , ( ls2 ) * sizeof(char) );
  ps2[ls2]='\0';
  lt2 = align -> t2 - align -> t1 + 1;
  pt2 = malloc( ( 1 + lt2 ) * sizeof(char) );
  pt2[lt2]='\0';
  memcpy ( pt2, align->t + align -> t1 - 1 , ( lt2 ) * sizeof(char) );

  #if DEBUG==1  
    printf("\n\n");
    printf("--------------------- \n");
    printf(" ==> s{%s}  t{%s}\n", align -> s, align -> t);
    printf(" ==> PART 2: ps2{%s} pt2{%s}\n", ps2, pt2);
    printf("--------------------- \n");
    printf(" ==>  strlen( s ) : %d\n", (int) strlen( align -> s ) );
    printf(" ==>           s1 : %d\n", align -> s1);
    printf(" ==>          ls2 : %d\n", ls2);
    printf(" ==>  strlen( t ) : %d\n", (int) strlen( align -> t ) );
    printf(" ==>           t1 : %d\n", align -> t1);
    printf(" ==>          lt2 : %d\n", lt2);
    fflush(stdout);
  #endif

  a2 = global_align(ps2, pt2,  M,  g) ;
 
 
  /* part 3 */
  ls3 =  strlen( align -> s ) - align -> s2 ;
  ps3 = malloc( ( 1 + ls3 ) * sizeof(char) );
  memcpy ( ps3, align->s  + align -> s2  , ( ls3 + 1 ) * sizeof(char) );  /* + 1  copy '0' */
  ps3[ls3]='\0';
  lt3 =  strlen( align -> t ) - align -> t2;
  pt3 = malloc( ( 1 + lt3 ) * sizeof(char) );
  memcpy ( pt3, align->t  + align -> t2  , ( lt3 + 1 ) * sizeof(char) );  /* + 1  copy '0' */
  pt3[lt3]='\0';

  #if DEBUG==1  
    printf("\n\n");
    printf("--------------------- \n");
    printf(" ==> s{%s}  t{%s}\n", align -> s, align -> t);
    printf(" ==> PART 3: ps3{%s} pt3{%s}\n", ps3, pt3);
    printf("--------------------- \n");
    printf(" ==>  strlen( s ) : %d\n", (int) strlen( align -> s ) );
    printf(" ==>           s2 : %d\n", align -> s2);
    printf(" ==>          ls3 : %d\n", ls3);
    printf(" ==>  strlen( t ) : %d\n", (int) strlen( align -> t ) );
    printf(" ==>           t2 : %d\n", align -> t2);
    printf(" ==>          lt3 : %d\n", lt3);
    fflush(stdout);
  #endif
 
  a3 = global_align(ps3, pt3,  M,  g) ;
 
  printf("\n");
  printf("a1 length: %d \n", a1->length); 
  #if DEBUG==1  
    printf("       s : %s \n", a1->s);
    printf("       t : %s \n", a1->t);
  #endif
  printf("       s': {%s} \n", a1->s_line);
  printf("       t': {%s} \n", a1->t_line);

  printf("\n");
  printf("a2 length: %d \n",  a2->length); 
  #if DEBUG==1  
    printf("       s : %s \n", a2->s);
    printf("       t : %s \n", a2->t);
  #endif
  printf("       s': {%s} \n", a2->s_line);
  printf("       t': {%s} \n", a2->t_line);

  printf("\n"); 
  printf("a3 length: %d \n",  a3->length); 
  #if DEBUG==1  
    printf("       s : %s \n", a3->s);
    printf("       t : %s \n", a3->t);
  #endif
  printf("       s': {%s} \n", a3->s_line);
  printf("       t': {%s} \n", a3->t_line);
 
  /* save alignament */
  align -> length = a1 -> length + a2 -> length + a3 -> length;
  align -> cut1   = a1 -> length;
  align -> cut2   = a1 -> length + a2 -> length - 1;

  printf("\n");
  printf("cut 1: %d\n", align -> cut1   ); fflush(stdout); 
  printf("cut 2: %d\n", align -> cut2   ); fflush(stdout); 
 
  align->s_line = malloc((1+align->length)*sizeof(char));
  strcpy ( align->s_line, a1->s_line );
  strcat ( align->s_line, a2->s_line );
  strcat ( align->s_line, a3->s_line );

  align->t_line = malloc((1+align->length)*sizeof(char));
  strcpy ( align->t_line, a1->t_line );
  strcat ( align->t_line, a2->t_line );
  strcat ( align->t_line, a3->t_line );


  printf("\n");
  printf("( a1 + a2 + a3 ) length: %d \n",  align->length); 
  #if DEBUG==1  
    printf("       s : %s \n", align->s);
    printf("       t : %s \n", align->t);
  #endif
  printf("                     s': {%s} \n", align->s_line);
  printf("                     t': {%s} \n", align->t_line);
  printf("\n");

  /* start in 0,0 */  
  align->s_start = 0;  /* i; */
  align->t_start = 0;  /* j; */
 
  alignment_free(a1);
  alignment_free(a2);
  alignment_free(a3);

  free(si);
  free(ti);

  free(ps1);
  free(pt1);
  free(ps2);
  free(pt2);
  free(ps3);
  free(pt3);


  return align;

}

/*************************************/






