/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "alphabet.h"

/*************************************/
int **im_malloc(int X, int Y);
void  im_free(int **A, int n);
void  im_print(int **A,int i_1, int i_2, int j_1, int j_2);
void  im_init(int **d, int rows, int cols, int value);

void  print_vector(int *V, int n);

char *load_word(char *filename);
void print_word(char *WORD);

void  extract_basename(char *basename, char *filename); /*REVISAR*/
FILE *fopen_suffix(char *basename, char *suffix, char *extension , char *opentype);


int any( int *T, int n);
int set_equals( int *T0, int *T1, int n);

char *strcpy2(char *des, char *ori);
char *strcat2(char *des, char *cad);


int char2index( char c );
int *seq2index( char *seq);
void print_seq_index(int *s, int n);

void out(char *m);

/*************************************/

#endif
