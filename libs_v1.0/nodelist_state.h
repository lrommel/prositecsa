/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef NODELIST_STATE_H
#define NODELIST_STATE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/*************************************/
 typedef struct _nodelist_state{
  int state;

  struct _nodelist_state *next;

} nodelist_state;
/*************************************/

/*************************************/
void print_nodelist( nodelist_state *L );

nodelist_state *del_nodelist_state(nodelist_state *L, int state );

nodelist_state *add_nodelist_state(nodelist_state *L, int state );
void nodelist_state_free(nodelist_state *L);

int in_nodelist_state(nodelist_state *L, int state);

nodelist_state ***st_malloc(int X,int Y);

void st_free(nodelist_state ***delta, int X, int Y);
void st_init(nodelist_state ***d, int rows, int cols);

void test_nodelist();
/*************************************/

#endif
