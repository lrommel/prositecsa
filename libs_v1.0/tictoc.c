/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "tictoc.h" 


/*************************************/

/*************************************/
float time_diff(struct timeval ts, struct timeval tf){
	return (tf.tv_sec - ts.tv_sec) + (float)(tf.tv_usec - ts.tv_usec)/1E6;
}

/*************************************/
void tic(){
   tic_id(0);
}
/*************************************/
float toc(){
   return toc_id(0,"");
}
/*************************************/
void tic_id(int id){
   /* Comeca a medir o tempo */
   gettimeofday(&__ts__[id], NULL);
}
/*************************************/
float toc_id(int id, char *s){
   float d;
   /* Termina a medicao do tempo */
   gettimeofday(&__tf__[id], NULL);
   
   d = time_diff(__ts__[id], __tf__[id]);
   printf("\n%s >> elapsed : \t %.6f segundos\n",s, d);   

   return d;
}


/*************************************/
