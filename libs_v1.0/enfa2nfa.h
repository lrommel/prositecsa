/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef ENFA2NFA_H
#define ENFA2NFA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"

#include "enfa.h"
#include "nfa.h"
 
/*************************************/
nfa *enfa2nfa( enfa *E );

/*************************************/

#endif
