/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "re2enfa.h"

/*************************************/
nodelist_enfa *add_nodelist_enfa(nodelist_enfa *L, char c ){

  nodelist_enfa  *node,*aux;

  /* allocate */
  node =  malloc(sizeof(nodelist_enfa));
  if (!node) {
	perror("add_nodelist_enfa malloc");
	exit(errno);
  }

  /* begin node: list pointers */
  node->prev=NULL;
  node->next=NULL;

  /* begin node: data  */
  if (c=='.'){
     node->symbol='E';
     node->p=point2enfa(c); 
  }
  else if (isLetter(c)){ 
     node->symbol='E';
     node->p=letter2enfa(c); 
  }
  else if (isOperator(c)||isParentheses(c)){
     node->symbol=c;
     node->p=NULL;   /* not ENFA*/
  }
  else {
       printf("ERROR: Regular Expression bad format, symbol [%c] not match \n",c);
       exit(0);
  }

  /* add node to list  */
  if (L==NULL){
     return node;
  }
  else{
     aux=L;
     while(aux->next!=NULL){
          aux=aux->next;
     }
     aux->next=node;
     node->prev=aux;
     return L;
  }
}

/*************************************/
enfa* re2enfa_thompson(char* R, int CASE) {
   /*
   CASE 0, Thompson,   default patterns
   CASE 1, Thompson,  improved patterns
   */

  enfa *N;

  /* lengths */
  int nr = strlen(R);

  nodelist_enfa  *L = NULL, *aux , *aux_a, *aux_b;
 
  int i;

  if(R==NULL) return NULL;

  /* add element to List */
  for(i=0; i<nr; i++){
     L = add_nodelist_enfa(L, R[i] );
  }


  #if DEBUG==1
  printf("RE: %s\n",R);
  print_list_enfa(L);
  #endif


  if(L==NULL) return NULL;  /*  R = '\0' */

  /* iteration : recursion for '()' elimination */
  aux_b=L;
  while(aux_b!=NULL ){

     /* find ')' */
     while(aux_b!=NULL &&  aux_b->symbol!=')'){
          aux_b=aux_b->next;
     }
     if(aux_b!=NULL  && aux_b->symbol==')'){

          /* find '('  */
          aux_a=aux_b;
          while(aux_a!=NULL  && aux_a->symbol!='(' ){
              aux_a=aux_a->prev;
          }
          if(aux_a!=NULL  && aux_a->symbol=='(' ){
              /* success ()  */

             /* cutting list_enfa aux_b */
             aux=aux_b->prev;
             aux_b=aux_b->next; /* warning: NULL*/
             free(aux->next);   /* terminal ')' */
             aux->next=NULL;


             /* cutting list_enfa aux_a */  
             aux=aux_a->next;
             aux_a=aux_a->prev; /* warning: NULL*/
             free(aux->prev);   /* terminal '(' */
             aux->prev=NULL;

             /* reduction list_enfa: aux */
             aux=reduction_list_enfa(aux,CASE);  /* reduction do free's*/
            
            /* connect node "aux":  aux_a - aux - aux_b */ 
            if(aux_a!=NULL) {
                      aux_a->next=aux;   /*  aux_a - aux */
                      aux->prev=aux_a;
            }
            else{
                 L=aux;
                 aux->prev=NULL;
            }
            
            if(aux_b!=NULL) { 
                aux_b->prev=aux;  /*  aux - aux_b */
                aux->next=aux_b; 
            }
            else{
                aux->next=NULL; 
            }

          }
          else{
               printf("ERROR: Regular Expression unbalanced, without '(' \n");
               exit(0);
          }
     }
  }

  #if DEBUG==1
  print_list_enfa(L);
  #endif

  aux=L;
  while(aux->next!=NULL){
    if(aux->symbol=='('){
        printf("ERROR: Regular Expression unbalanced, without ')' \n");
        exit(0);  
    }
    aux=aux->next;
  }


  /* reduction final */
  L=reduction_list_enfa(L, CASE);  /* reduction do free's*/
 
  if (CASE==2)  N = enfa_E_correctness (L->p);
  else          N = L -> p;

  free(L);

  return N;
}

/*************************************/
nodelist_enfa *reduction_list_enfa(nodelist_enfa *L,int CASE){
   /*
   CASE 0, Thompson,   default patterns
   CASE 1, Thompson,  improved patterns
   */

   nodelist_enfa *aux, *tmp, *aux1, *aux2;
   char symbol;

   #if DEBUG==1
   printf("\n-------  REDUCTION  begin ---------------------\n");
   #endif

   if(L==NULL) return NULL;

   #if DEBUG==1
   print_list_enfa(L);
   #endif

   /* Operator '+','*','?' */
   aux=L;
   while(aux!=NULL){

        /* find '+','*','?'  */ 
        if( (aux->symbol=='+' || aux->symbol=='*'|| aux->symbol=='?')){
             if(aux->prev!=NULL && aux->prev->symbol=='E'){
                   symbol = aux->symbol;
                   tmp=aux->prev;        /* expression */
                   
                   tmp->next=aux->next;  /* Expression to next */
                   if(aux->next!=NULL)  aux->next->prev=tmp;  /* next to Expression */

                   free(aux);            /* delete operator '+' */
                   aux=tmp;

                   /* copy enfa */
                   if( symbol=='+'){   
                      if     (CASE==0) aux->p = enfa_opsum_E   ( aux->p );
                      else if(CASE>0) aux->p = enfa_opsum_melhor( aux->p );
                   }   
                   else if(symbol=='*'){   
                      if     (CASE==0) aux->p = enfa_kleen_E   ( aux->p );  /* free enfa */
                      else if(CASE>0) aux->p = enfa_kleen_melhor( aux->p );  /* free enfa */
                   }    
                   else if(symbol=='?'){
                      if     (CASE==0) aux->p = enfa_opE_E   ( aux->p );
                      else if(CASE>0) aux->p = enfa_opE_melhor( aux->p );
                   }   
                   else{
                      printf("ERROR: Algoritm (impossivel: %c)\n",symbol);
                      exit(0);  
                   }                           
            }
             else{
                 printf("ERROR: Bad format, operator '%c' without 'Expression' \n",aux->symbol);
                 exit(0);  
            }

        }
        aux=aux->next;
   }


   #if DEBUG==1
   printf("\n\n -- After to aplicate  Operators: '+','*','?'  \n");
   print_list_enfa(L);
   #endif

   /* Catenation (priority previus alternation) */
   aux1=L;
   while(aux1->next!=NULL ){
      while(aux1->next!=NULL && aux1->next->symbol!='|'){
           /* cat*/
           aux2=aux1->next; 
   
           aux1->next = aux2->next;
           if(aux2->next!=NULL)  aux2->next->prev = aux1;
        
           if     (CASE==0) aux1->p = enfa_catenation_E    ( aux1->p , aux2->p );  /* free's enfa's */
           else if(CASE>0) aux1->p = enfa_catenation_melhor( aux1->p , aux2->p ); /* free's enfa's */
           free(aux2);
      }
      if(aux1->next!=NULL && aux1->next->symbol=='|'){
          aux1=aux1->next;
          if (aux1->next!=NULL && aux1->next->symbol=='E'){
                 aux1=aux1->next;
          }
          else{
              printf("ERROR: Bad format, catenation without 'Expression' \n");
              exit(0);  
          }
      }
   }


   #if DEBUG==1
   printf("\n\n -- After to aplicate  Catenation (priority previus alternation) \n");
   print_list_enfa(L);
   #endif

   /* Alternation  */
   aux=L;
   while(aux!=NULL){

        /* find '|' */
        if(aux->symbol=='|'){
             if(aux->prev!=NULL && aux->next!=NULL && 
                aux->prev->symbol=='E' && aux->next->symbol=='E' ){
                   aux1=aux->prev;  /* expression 1 */
                   aux2=aux->next;  /* expression 2 */
                   
                   aux1->next=aux2->next;  /* Expression to next next */
                   if(aux2->next!=NULL) aux2->next->prev=aux1;  /* next next to Expression */

                   free(aux);             /* delete operator '|' */
                   aux=aux1;

                   if     (CASE==0) aux1->p = enfa_alternation_E    ( aux1->p , aux2->p ); /* free's enfa's */
                   else if(CASE>0) aux1->p = enfa_alternation_melhor( aux1->p , aux2->p ); /* free's enfa's */
                   free(aux2);

                   #if DEBUG==1
                   printf("\n\n -- After to aplicate 1 alternation) \n");
                   print_list_enfa(L);
                   #endif
 
            }
             else{
                 printf("ERROR: Bad format, operator '|' without 'Expression' \n");
                 exit(0);  
            }

        }
        aux=aux->next;
   }

   #if DEBUG==1
   printf("\n\n -- After to aplicate  Alternation \n");
   print_list_enfa(L);
   #endif

  #if DEBUG
  printf("-----  REDUCTION  END -----------------------\n\n");
  #endif


  return L;
}

/*************************************/
enfa *enfa_opsum_E( enfa *N1){
   enfa *N;

   #if DEBUG==1
   printf(" * O P E R A T O R  +  w i t h   E : begin ************ \n");
   print_enfa(N1);
   #endif

   N = enfa_union(N1,NULL,2);  /* copy enfa with 2 news states */

   N->start = N1->numst;
   N->end   = N1->numst+1;
  
   /* N1->end  to  N->end */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N->end );

   /* N->start to N1->start */
   N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns] , N1->start );

   /* N1->end to N1->start */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N1->start );
 
   enfa_free(N1);  

   #if DEBUG==1
   print_enfa(N);
   printf(" * O P E R A T O R  +  w i t h   E : end ************ \n");
   #endif

 
   return N;
}

/*************************************/
enfa *enfa_opsum_melhor( enfa *N1){

   #if DEBUG==1
   printf(" * O P E R A T O R  +    M E L H O R : begin ************ \n");
   print_enfa(N1);
   #endif


   /* N->end  to  N->start */
   N1->d[N1->end][ns] = add_nodelist_state(N1->d[N1->end][ns] , N1->start );
 
   #if DEBUG==1
   print_enfa(N1);
   printf(" * O P E R A T O R  +     M E L H O R : end ************ \n");
   #endif

 
   return N1;
}


/*************************************/
enfa *enfa_opE_E( enfa *N1){
   enfa *N;

   #if DEBUG==1
   printf(" * O P E R A T O R  ?  w i t h   E : begin ************ \n");
   print_enfa(N1);
   #endif

   N = enfa_union(N1,NULL,2);  /* copy enfa with 2 news states */

   N->start = N1->numst;
   N->end   = N1->numst+1;
  
   /* N1->end  to  N->end */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N->end );

   /* N->start to N1->start */
   N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns] , N1->start );

   /* N->start to N->end */
   N->d[N->start ][ns] = add_nodelist_state(N->d[N->start ][ns] , N->end );
 
   enfa_free(N1);  

   #if DEBUG==1
   print_enfa(N);
   printf(" * O P E R A T O R  ?  w i t h   E : end ************ \n");
   #endif

 
   return N;
}

/*************************************/
enfa *enfa_opE_melhor( enfa *N1){
   enfa *N;

   #if DEBUG==1
   printf(" * O P E R A T O R  ?  M E L H O R : begin ************ \n");
   print_enfa(N1);
   #endif


   if (N1->end==N1->start){
         N = N1;
   }
   else if( (!cycle_start(N1))&&(!cycle_end(N1)) ){ 
      N = N1;

      /* N->end  to  N->start */
      N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns] , N->end );
   }
   else if ( ( !cycle_start(N1))&&( cycle_end(N1)) ){
      N = enfa_union(N1,NULL,1);  /* copy enfa with 1 new  state  */
      
      N->numst = N1->numst + 1;
      N->start = N1->start;
      N->end   = N->numst - 1 ;
    
      N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns], N->end );
      N->d[N1->end][ns]  = add_nodelist_state(N->d[N1->end][ns] , N->end );

      enfa_free(N1);
   }
   else  if ( (  cycle_start(N1))&&( !cycle_end(N1)) ){
      N = enfa_union(N1,NULL,1);  /* copy enfa with 1 new  state  */

      N->numst = N1->numst + 1;
      N->start = N->numst  - 1 ;
      N->end   = N1->end;
    
      N->d[N->start][ns]  = add_nodelist_state(N->d[N->start][ns],  N->end );
      N->d[N->start][ns]  = add_nodelist_state(N->d[N->start][ns],  N1->start );
  
      enfa_free(N1);
   }else{
      N = enfa_union(N1,NULL,2);  /* copy enfa with 2 news  states  */
      
      N->numst = N1->numst + 2;
      N->start = N->numst  - 2 ;
      N->end   = N->numst  - 1 ;
    
      N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns], N->end );
      N->d[N->start][ns]  = add_nodelist_state(N->d[N->start][ns],  N1->start );
      N->d[N1->end][ns]  = add_nodelist_state(N->d[N1->end][ns] , N->end );

      enfa_free(N1);
   }                                           

   #if DEBUG==1
   print_enfa(N1);
   printf(" * O P E R A T O R  ?  M E L H O R : end ************ \n");
   #endif

 
   return N;
}

/*************************************/
enfa *enfa_E_correctness( enfa *N1){ /*caso (i), sem ciclos*/

   enfa *N;
   nodelist_state *aux;
   int i,j,k,ii,kk;
   int numst1,numst;
   int **NS;

   #if DEBUG==1
   printf(" * E  C O R R E C T N E S S : begin ************ \n");
   print_enfa(N1);
   #endif

   if(N1==NULL) return NULL;
   numst1=N1->numst;

   /* allocate and init table to EMOVEs */
   NS = im_malloc(numst1,numst1);
   im_init(NS,numst1,numst1,0);
 
   /* copy EMOVEs to NS */ 
   for(i=0;i<numst1;i++){
      aux=N1->d[i][ns];
      while(aux!=NULL){  /* aux->state */
          NS[i][aux->state]=1;
          aux=aux->next;
      }
   }

   /* init numst */
   numst = numst1;

   i=0;
   while(i<numst){ 
      for(j=0;j<i;j++){
          if(NS[i][j]==1){  /* Exist EMOVE i to j */
              if(NS[j][i]==1){ /* Exist EMOVE j to i */
                    /* fusion j with i */ 
                    /* copy transitions of i to j */
                    for(k=0;k<=ns;k++){
                        aux=N1->d[i][k];
                        while(aux!=NULL){  
                              if (aux->state==i) aux->state=j;
                              N1->d[j][k]=add_nodelist_state(N1->d[j][k],aux->state);

                              /*change in NS*/
                              if(k==ns){
                                  NS[j][aux->state]=1;  
                              } 

                              aux=aux->next;
                        }
                    } 

                    /* move transitions with final state i to j */
                    for(ii=0;ii<numst;ii++){
                      for(kk=0;kk<=ns;kk++){
                          aux=N1->d[ii][kk];
                          while(aux!=NULL){  
                              if (aux->state==i) {
                                      aux->state=j;

                                      /*change in NS*/
                                      NS[ii][i]=0;  
                                      NS[ii][j]=1;
                                       
                              }
                              aux=aux->next;
                          }
                      } 
                    }

                    /* move i to numst-1*/
                    enfa_change_state(N1, i, numst-1);  
                    if(N1->end == numst-1)   N1->end   = j;
                    if(N1->start == numst-1) N1->start = j;
                      
                    /* copy EMOVEs of j to NS */ 
                    aux=N1->d[j][ns];
                    while(aux!=NULL){  /* aux->state */
                           NS[j][aux->state]=1;
                           aux=aux->next;
                    }

                    numst--;
                    i=0;
                    break;
              }
          }
      }
      i++;
   }
 
   /* init new enfa: N */
   N = enfa_init(numst,N1->start,N1->end,N1->C);
 
   /* copy transitions the firsts numst states from N1 to N */
   for(i=0;i<numst;i++){
     for(j=0;j<=ns;j++){
         aux=N1->d[i][j];
         while(aux!=NULL){  
               if(j==ns && aux->state == i) { aux=aux->next; continue;}
               N->d[i][j]=add_nodelist_state(N->d[i][j], aux->state );
               aux=aux->next;
         }
     } 
   }

   #if DEBUG==1
   print_enfa(N1);
   printf(" * E  C O R R E C T N E S S : end ************ \n");
   #endif

   enfa_free(N1);

   return N;
}
/*************************************/
enfa *enfa_kleen_melhor_caso_i( enfa *N1){ /*caso (i), sem ciclos*/

   enfa *N;
   nodelist_state *aux;
   int i,j;
   int numst1;

   #if DEBUG==1
   printf(" * K L E E N  M E L H O R   Caso (i) E - free : begin ************ \n");
   print_enfa(N1);
   #endif

   if(N1==NULL) return NULL;
   numst1=N1->numst;
 
   N = enfa_init(numst1-1,N1->start,N1->start,N1->C);
   N->C[N->start]=1; /* cycle to start state */

   /*** re-index state end to (numst-1) in N1 ***/
   enfa_change_state(N1, numst1-1, N1->end);

   /* copy transitions N1 to N, whitout end state*/
   for(i=0;i<numst1-1;i++){
     for(j=0;j<=ns;j++){
         aux=N1->d[i][j];
         while(aux!=NULL){  
               if ( (j==ns)&&(i==N1->start)&&(aux->state==N1->end)  ){
                  aux=aux->next;
                  continue;
               } 
               N->d[i][j]=add_nodelist_state(N->d[i][j],(aux->state==N1->end)?(N->end):(aux->state));
               aux=aux->next;
         }
     } 
   }
   /* transitions end state N1 to N */ 
   for(j=0;j<=ns;j++){
         aux=N1->d[N1->end][j];
         while(aux!=NULL){ 
               if ( (j==ns)&&(aux->state==N1->start)  ){
                  aux=aux->next;
                  continue;
               }
               N->d[N->end][j]=add_nodelist_state(N->d[N->end][j],(aux->state==N1->end)?(N->end):(aux->state));
               aux=aux->next;
         }
   }
 
   #if DEBUG==1
   print_enfa(N1);
   printf(" * K L E E N  M E L H O R   Caso (i) E - free : end ************ \n");
   #endif

   enfa_free(N1);

   return N;
}

/*************************************/
enfa *enfa_kleen_melhor_caso_ii( enfa *N1){

   #if DEBUG==1
   printf(" * K L E E N  M E L H O R   Caso (ii): begin ************ \n");
   print_enfa(N1);
   #endif

   /* N1->end  to  N1->start */
   N1->d[N1->end][ns] = add_nodelist_state(N1->d[N1->end][ns] , N1->start );

   N1->end            = N1->start;
   N1->C[N1->start]   = 1;

   #if DEBUG==1
   print_enfa(N1);
   printf(" * K L E E N  M E L H O R   Caso (ii) : end ************ \n");
   #endif

 
   return N1;
}

/*************************************/
enfa *enfa_kleen_melhor_caso_iii( enfa *N1){

   #if DEBUG==1
   printf(" * K L E E N  M E L H O R   Caso (iii): begin ************ \n");
   print_enfa(N1);
   #endif

   /* N1->end  to  N1->start */
   N1->d[N1->end][ns] = add_nodelist_state(N1->d[N1->end][ns] , N1->start );

   N1->start          = N1->end;
   N1->C[N1->start]   = 1;

   #if DEBUG==1
   print_enfa(N1);
   printf(" * K L E E N  M E L H O R   Caso (iii) : end ************ \n");
   #endif

 
   return N1;
}

/*************************************/
enfa *enfa_kleen_melhor_caso_iv( enfa *N1){
   enfa *N;

   #if DEBUG==1
   printf(" * K L E E N  M E L H O R   Caso (iv): begin ************ \n");
   print_enfa(N1);
   #endif

    if (N1->end==N1->start){
         N = N1;
    }
    else{
        N = enfa_union(N1,NULL,1);  /* copy enfa with 1 news state  */

        N->start           = N1->numst;
        N->end             = N1->numst;
        N->C[N->start]    = 1;
  
        /* N1->end  to  N->end */
        N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N->end );

        /* N->start to N1->start */
        N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns] , N1->start );

        /* N1->end to N1->start */
        //N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N1->start );

        enfa_free(N1);  
   }

   #if DEBUG==1
   print_enfa(N);
   printf(" * K L E E N  M E L H O R   Caso (iv): end ************ \n");
   #endif
 
   return N;
}


/*************************************/
enfa *enfa_kleen_melhor( enfa *N1){
   enfa *N;

   #if DEBUG==1
   printf(" * K L E E N  M E L H O R: begin ************ \n");
   print_enfa(N1);
   #endif

   if      ( !cycle_start(N1) && !cycle_end(N1) ) N = enfa_kleen_melhor_caso_i  (N1);
   else if ( !cycle_start(N1) &&  cycle_end(N1) ) N = enfa_kleen_melhor_caso_ii (N1);
   else if (  cycle_start(N1) && !cycle_end(N1) ) N = enfa_kleen_melhor_caso_iii(N1);
   else if (  cycle_start(N1) &&  cycle_end(N1) ) N = enfa_kleen_melhor_caso_iv (N1);
 
   #if DEBUG==1
   print_enfa(N);
   printf(" * K L E E N  M E L H O R: end ************ \n");
   #endif
 
   return N;
}

/*************************************/
enfa *enfa_kleen_E( enfa *N1){
   enfa *N;

   #if DEBUG==1
   printf(" * K L E E N  w i t h   E: begin ************ \n");
   print_enfa(N1);
   #endif

   N = enfa_union(N1,NULL,2);  /* copy enfa with 2 news states */

   N->start = N1->numst;
   N->end   = N1->numst+1;
  
   /* N1->end  to  N->end */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N->end );

   /* N->start to N1->start */
   N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns] , N1->start );

   /* N1->end to N1->start */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N1->start );

   /* N->start  to  N->end */
   N->d[N->start][ns] = add_nodelist_state(N->d[N->start][ns] , N->end );
 
   enfa_free(N1);  

   #if DEBUG==1
   print_enfa(N);
   printf(" * K L E E N  w i t h   E: end ************ \n");
   #endif
 
   return N;
}

/*************************************/
enfa *enfa_alternation_E( enfa *N1 , enfa *N2 ){
   enfa *N;
   int numst1,numst2;

   if(N1==NULL || N2==NULL) return NULL;

   numst1=N1->numst;
   numst2=N2->numst;

   #if DEBUG==1
   printf(" * A L T E R N A T I O N - E : begin ************ \n");
   print_enfa(N1);
   print_enfa(N2);
   #endif

   N = enfa_union(N1,N2,2);  /* copy enfa with 2 news states */

   N->start = numst1+numst2;
   N->end   = numst1+numst2+1;

   /* N->start to  N1->start */
   N->d[N->start ][ns] = add_nodelist_state(N->d[N->start ][ns] , N1->start );

   /* N->start to  N2->start */
   N->d[N->start ][ns] = add_nodelist_state(N->d[N->start ][ns] , N2->start + numst1);

   /* N1->end  to  N->end */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N->end );

   /* N2->end  to  N->end */
   N->d[N2->end+numst1][ns] = add_nodelist_state(N->d[N2->end+numst1][ns] , N->end );

   enfa_free(N1);
   enfa_free(N2);

   #if DEBUG==1
   print_enfa(N);
   printf(" * A L T E R N A T I O N - E : end ************ \n");
   #endif

   return N;
}

/*************************************/
void enfa_change_state( enfa *N, int p, int q){
   /* change state p to q   and  q to p */
   nodelist_state *aux;
   int i,j,tmp;

   for(i=0;i<N->numst;i++){
     for(j=0;j<=ns;j++){
         aux=N->d[i][j];
         while(aux!=NULL){  
               if (aux->state == p) aux->state = q;
               else if (aux->state == q) aux->state = p;
               aux=aux->next;
         }
     } 
   }

   /* change state from table */
   for(j=0;j<=ns;j++){
         aux=N->d[p][j];
         N->d[p][j]=N->d[q][j];
         N->d[q][j]=aux; 
   } 
  
   if      (N->start == p ) N->start = q;
   else if (N->start == q ) N->start = p;
   if      (N->end == p ) N->end = q;
   else if (N->end == q ) N->end = p;


   /* cycle change */
   tmp     = N->C[p];
   N->C[p] = N->C[q];
   N->C[q] = tmp;
}  

/*************************************/
enfa *enfa_alternation_melhor( enfa *N1 , enfa *N2 ){
   enfa *N;
   nodelist_state *aux;
   int i,j;
   int numst1,numst2, newstate, N2start, N2end, Nstart, Nend, numst;

   #if DEBUG==1
   printf(" * A L T E R N A T I O N - M E L H O R : begin ************ \n");
   print_enfa(N1);
   print_enfa(N2);
   #endif

   if (N1==NULL && N2!=NULL)  return N2;
   if (N1!=NULL && N2==NULL)  return N1;
   if (N1==NULL && N2==NULL ) return NULL;

   numst1=N1->numst;
   numst2=N2->numst;

  /*** re-index states in N2 ***/
  if (numst2>2) {
      if (N2->start==N2->end){ 
          enfa_change_state(N2, numst2-1, N2->end);
      }
      else{
          enfa_change_state(N2, numst2-1, N2->end);
          enfa_change_state(N2, numst2-2, N2->start);
      }
   }

   /*** position in N after copy: complete N1 + N2 without start e end ***/
   numst = numst1 + numst2 - 1 - (N2->start!=N2->end);
   if( (cycle_start(N1)||cycle_start(N2)) )     N2start =  numst++;
   else                                         N2start =  N1->start;
 
   if (N2->start==N2->end)                      N2end   =  N2start; 
   else if(!cycle_end(N1)&&!cycle_end(N2))      N2end   =  N1->end;
   else                                         N2end   =  numst++;
    
   if( cycle_start(N1) && cycle_start(N2) )     Nstart  =  numst++;
   if( cycle_end(N1) && cycle_end(N2) )         Nend    =  numst++;
 
 
   /* init N without: start state, end state, cycle flag */
   N = enfa_init(numst,0,0,NULL);
 
  /*** copy N1(Mb) to N complete  ***********************/
  for(i=0;i<numst1;i++){
     for(j=0;j<=ns;j++){
         aux=N1->d[i][j];
         while(aux!=NULL){  
               N->d[i][j]=add_nodelist_state(N->d[i][j],aux->state);
               aux=aux->next;
         }
     }
 
     /* copy cycle flag N1 */
     N->C[i] = N1->C[i];
  }
  


  /*** copy N2, whitout start,end  ***********************/
  for(i=0;i<numst2-1-(N2->start!=N2->end);i++){
     for(j=0;j<=ns;j++){
         aux=N2->d[i][j];
         while(aux!=NULL){  
               if     (aux->state==N2->start) newstate = N2start;
               else if(aux->state==N2->end)   newstate = N2end;
               else                           newstate = numst1 + aux->state;
               N->d[numst1+i][j]=add_nodelist_state(N->d[numst1+i][j],newstate);
               aux=aux->next;
         }
     } 

     /* copy cycle flag N2 */
     N->C[numst1+i] = N2->C[i];
  }  


  /** copy N2 start to N2start in N **/
  for(j=0;j<=ns;j++){
       aux=N2->d[N2->start][j];
       while(aux!=NULL){  
             if     (aux->state==N2->start) newstate = N2start;
             else if(aux->state==N2->end)   newstate = N2end;
             else                           newstate = numst1 + aux->state;
             N->d[N2start][j]=add_nodelist_state(N->d[N2start][j], newstate);
             aux=aux->next;
       }
  }
  N->C[N2start] = N->C[N2start] || N2->C[N2->start]; /* copy cycle */
  
  /** copy N2 end to N2end in N **/
  if (N2->end!=N2->start)
     for(j=0;j<=ns;j++){
         aux=N2->d[N2->end][j];
         while(aux!=NULL){  
               if     (aux->state==N2->start) newstate = N2start;
               else if(aux->state==N2->end)   newstate = N2end;
               else                           newstate = numst1 + aux->state;
               N->d[N2end][j]=add_nodelist_state(N->d[N2end][j], newstate);
               aux=aux->next;
         }
  }
  N->C[N2end] = N->C[N2end] || N2->C[N2->end]; /* copy cycle */
 
  /** E move: N1 start to  N2 start **/
  if(  (!cycle_start(N1)) && cycle_start(N2) && (N1->start!=N2start)  ) {
        N->d[ N1->start ][ns]=add_nodelist_state(N->d[ N1->start ][ns], N2start);
  }
 
  /** E move: N2 start to  N1 start **/
  if(  cycle_start(N1) && (!cycle_start(N2)) && (N1->start!=N2start) ) {
        N->d[ N2start ][ns]=add_nodelist_state(N->d[ N2start ][ns], N1->start );
  }
  /** E move: N1 end to  N2 end **/
  if(  cycle_end(N1) && !cycle_end(N2) && (N1->end!=N2end) ) {
        N->d[ N1->end ][ns]=add_nodelist_state(N->d[ N1->end ][ns], N2end);
  }
 
  /** E move: N2 end to  N1 end **/
  if(  !cycle_end(N1) && cycle_end(N2) && (N1->end!=N2end) ) {
        N->d[ N2end ][ns]=add_nodelist_state(N->d[ N2end ][ns], N1->end );
  }
 
  /** N start **/
  if(  cycle_start(N1) && cycle_start(N2)  ){
        N->d[Nstart][ns]=add_nodelist_state(N->d[Nstart][ns], N1->start);
        N->d[Nstart][ns]=add_nodelist_state(N->d[Nstart][ns], N2start);
  }
 
  /** N end **/
  if( cycle_end(N1) && cycle_end(N2)  ){
        N->d[N1->end][ns]=add_nodelist_state(N->d[N1->end][ns], Nend);
        N->d[N2end][ns]=add_nodelist_state(N->d[N2end][ns], Nend);
  }
 
  /* start , end  state */
  if( !cycle_start(N1) && !cycle_start(N2) ) N->start = N1->start;
  if( !cycle_end(N1)   && !cycle_end(N2)   ) N->end   = N1->end;

  if( !cycle_start(N1) && cycle_start(N2)  ) N->start = N1->start;
  if( !cycle_end(N1)   && cycle_end(N2)    ) N->end   = N1->end;

  if(  cycle_start(N1) &&!cycle_start(N2)  ) N->start = N2start;
  if(  cycle_end(N1)   &&!cycle_end(N2)    ) N->end   = N2end;

  if(  cycle_start(N1) && cycle_start(N2)  ) N->start = Nstart;
  if(  cycle_end(N1)   && cycle_end(N2)    ) N->end   = Nend;

   
 
  enfa_free(N1);
  enfa_free(N2);

  #if DEBUG==1
  print_enfa(N);
  printf(" * A L T E R N A T I O N - M E L H O R : end ************ \n");
  #endif

  
  return N;
}

/*************************************/
enfa *enfa_catenation_Efree( enfa *N1 , enfa *N2 ){
   enfa *N;
   nodelist_state *aux;
   int i,j;
   int numst1,numst2,newstate;

   #if DEBUG==1
   printf(" * C A T E N A T I O N  Efree: begin ************ \n");
   print_enfa(N1);
   print_enfa(N2);
   #endif

   if(N1==NULL) return NULL;
   numst1=N1->numst;

   if(N2==NULL) numst2=0;  /* case only copy with news states */
   else         numst2=N2->numst;

   N = enfa_init( numst1+numst2-1,N1->start, N2->end + numst1-1, NULL);

   /*** re-index states in N1 ***/
   enfa_change_state(N1, N1->end, numst1-1);

   /*** re-index states in N2 ***/
   enfa_change_state(N2, N2->start, 0);

   /* copy transitions N1 to N*/
   for(i=0;i<numst1;i++){
     for(j=0;j<=ns;j++){
         aux=N1->d[i][j];
         while(aux!=NULL){  
               N->d[i][j]=add_nodelist_state(N->d[i][j],aux->state);
               aux=aux->next;
         }
     } 
     N->C[i] = N1->C[i]; /* cycle */
   } 
 
  /* copy transitions N2 to N*/
  for(i=0;i<numst2;i++){
     for(j=0;j<=ns;j++){
         aux=N2->d[i][j];
         while(aux!=NULL){  
               if     (aux->state==N2->start) newstate = numst1-1;
               else                           newstate = numst1-1 + aux->state;
               N->d[numst1+i-1][j]=add_nodelist_state(N->d[numst1+i-1][j],newstate);
               aux=aux->next;
         }
     } 
     N->C[numst1+i-1] = N2->C[i]||N->C[numst1+i-1]; /* cycle */
  }  

   enfa_free(N1);
   enfa_free(N2);

   #if DEBUG==1
   print_enfa(N);
   printf(" * C A T E N A T I O N   Efree: end ************ \n");
   #endif

  
   return N;
}

/*************************************/
enfa *enfa_catenation_E( enfa *N1 , enfa *N2 ){
   enfa *N;
   int numst1;

   if(N1==NULL || N2==NULL) return NULL;
   numst1=N1->numst;
   
   #if DEBUG==1
   printf(" * C A T E N A T I O N  E: begin ************ \n");
   print_enfa(N1);
   print_enfa(N2);
   #endif

   
   N = enfa_union(N1,N2,0);  /* copy enfa with 2 news states */

   N->start = N1->start;
   N->end   = N2->end + numst1;
  
   /* N1->end to  N2->start */
   N->d[N1->end ][ns] = add_nodelist_state(N->d[N1->end ][ns] , N2->start + numst1 );

   enfa_free(N1);
   enfa_free(N2);

   #if DEBUG==1
   print_enfa(N);
   printf(" * C A T E N A T I O N  E: end ************ \n");
   #endif

   return N;

}

/*************************************/
enfa *enfa_catenation_melhor( enfa *N1 , enfa *N2 ){
   enfa *N;
 
   #if DEBUG==1
   printf(" * C A T E N A T I O N  M E L H O R: begin ************ \n");
   print_enfa(N1);
   print_enfa(N2);
   #endif
   
   if ( cycle_end(N1) && cycle_start(N2) ) N = enfa_catenation_E(N1,N2);
   else                                    N = enfa_catenation_Efree(N1,N2);

   #if DEBUG==1
   print_enfa(N);
   printf(" * C A T E N A T I O N  M E L H O R: end ************ \n");
   #endif

   return N;

}

/*************************************/
enfa *enfa_union( enfa *N1, enfa *N2, int state_add ){
   enfa *N;
   nodelist_state *aux;
   int i,j;
   int numst1,numst2;

   if(N1==NULL) return NULL;
   numst1=N1->numst;

   if(N2==NULL) numst2=0;  /* case only copy with news states */
   else         numst2=N2->numst;

   N = enfa_init(numst1+numst2+state_add , -1, -1, NULL); /* foreing control */

   /* copy transitions N1 to N*/
   for(i=0;i<numst1;i++){
     for(j=0;j<=ns;j++){
         aux=N1->d[i][j];
         while(aux!=NULL){  
               N->d[i][j]=add_nodelist_state(N->d[i][j],aux->state);
               aux=aux->next;
         }
     } 
     N->C[i] = N1->C[i]; /* cycle */
   } 

   /* copy transitions N2 to N   ( when numst2!=0 ) */  
   for(i=0;i<numst2;i++){
     for(j=0;j<=ns;j++){
         aux=N2->d[i][j];
         while(aux!=NULL){  
               N->d[i+numst1][j]=add_nodelist_state( N->d[i+numst1][j], aux->state + numst1 );
               aux=aux->next;
         }
     } 
     N->C[i+numst1] = N2->C[i]; /* cycle */
   } 
  
   return N;
}


/*************************************/
void print_list_enfa(nodelist_enfa *aux){
printf("     __________  BEGIN : print_list_enfa  __________ \n");
printf("    | \n");
  while(aux!=NULL){
        printf("\tSymbol:  %c  \n",aux->symbol);
        if(aux->symbol=='E')   { 
            printf("\t\t\tp: %p\n",aux->p);
            print_enfa(aux->p);
        }
        else printf("\n");
        aux=aux->next;
  }
printf("    |\n");
printf("    |__________  END : print_list_enfa  __________\n\n");
}

/*************************************/
 






