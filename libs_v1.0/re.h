/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef RE_H
#define RE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

/*************************************/
char *load_re(char *filename);
int re_success(char *RE);
void print_re(char *RE);

int isLetter(char c);
int isOperator(char c);
int isParentheses(char c);

void reduction_alphabet(char *cad);
/*************************************/

#endif
