/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#include "dfamin.h"

/*************************************/
nodediff ***matlist_nodediff_malloc(int rows, int cols){
  nodediff ***nd;
  int i;
  nd = (nodediff***) malloc(rows*sizeof(nodediff**));
  for (i=0; i<rows; i++) {
      nd[i] = (nodediff**) malloc(cols*sizeof(nodediff*));
  }
  return nd;
}

/*************************************/
void matlist_nodediff_free(nodediff ***d, int rows, int cols){
  int i,j;
  if (d!=NULL){
    for(i=0;i<rows;i++){
       for(j=0;j<cols;j++){
         list_nodediff_free(d[i][j]); /**/
         d[i][j]=NULL;
       } 
    }
    for (i=0; i<rows; i++) free(d[i]);
    free(d);
  }
}

/*************************************/
void matlist_nodediff_init(nodediff ***d, int rows, int cols){
  int i,j;
  for(i=0;i<rows;i++){
     for(j=0;j<cols;j++){
         d[i][j]=NULL;
     } 
  }
}

/*************************************/

/*************************************/
void list_nodediff_free(nodediff *L){
   nodediff *aux;

   aux=L; 

   while(aux!=NULL){   
       aux=aux->next;

       /* free header list */
       free(L);

       L=aux; 
   }

}

/*************************************/
nodediff *add_list_nodediff(nodediff *L, int i, int j ){

  nodediff *node,*aux;

  /* allocate */
  node =  malloc(sizeof(nodediff));

  /* begin node  */
  node->next=NULL;
  node->i=i;
  node->j=j;

  /* add node to list  */
  if (L==NULL){
     return node;
  }
  else{
     aux=L;
     while( aux->next!=NULL && !((aux->i==i)&&(aux->j==j)) ){  /* while new state */
          aux=aux->next;
     }
     if(!((aux->i==i)&&(aux->j==j))){  /* is new state */
          aux->next=node;
     }
     else{
         free(node);
     }
     return L;
  }
}

/*************************************/
void   print_list_nodediff(nodediff *L){
    nodediff *aux;
    aux=L;
    while( aux!=NULL  ){  /* while new state */
          printf("\t\t\t list %d %d\n",aux->i,aux->j);
          aux=aux->next;
    }
}

/*************************************/

/*************************************/
dfa *dfa2dfamin( dfa *D ){

  nodelist_state *aux;
  nodediff ***matlist;

  int **M;

  int *end,*nullable;

  int numst=D->numst;
  int i,j,k;
  int numnull,r,s,ni,nj;

  /* allocate and init M */
  M = im_malloc(numst,numst);
  im_init(M, numst, numst, 0);

  /* allocate and init end vector */ 
  end = (int *) malloc(numst*sizeof(int));
  memset(end, 0, numst*sizeof(int));

  #if DEBUG==1
  printf("end states:\n");
  print_vector(end, numst);
  #endif 

  /* allocate and init nullable vector */ 
  nullable = (int *) malloc(numst*sizeof(int));
  memset(nullable, 0, numst*sizeof(int));  

  #if DEBUG==1
  printf("nullable states:\n");
  print_vector(nullable, numst);
  #endif 

  /* mark end states */
  aux=D->end;
  while(aux!=NULL){ 
     /* aux->state */
     end[aux->state]=1;  
     aux=aux->next;
  }

  #if DEBUG==1
  printf("end states:\n");
  print_vector(end, numst);
  #endif 

  /* final state vs not final state */ 
  aux=D->end;
  while(aux!=NULL){              /* for each end state */
     for(i=0;i<numst;i++){
         if(end[i]==0){          /* if not end state   */
            M[aux->state][i]=1;  /* different          */
            M[i][aux->state]=1;  /* different symmetry */
         }
     }
     aux=aux->next;
  }

  #if DEBUG==1
  print_tablediff(M, numst);
  #endif  
  
  /* matlist_nodediff */
  matlist = matlist_nodediff_malloc(numst, numst); 
  matlist_nodediff_init(matlist, numst, numst);
  
  for(i=0;i<numst;i++){
     for(j=0;j<i;j++){     /* j < i : triangle low */
          if(M[i][j]==0){  /* not different        */

               #if DEBUG==1
               printf("\n%d %d :\n", i, j );
               #endif     
          
               for(k=0;k<ns;k++){
                    r=D->d[i][k];
                    s=D->d[j][k];
                    if( (r>=0)&&(s>=0)&&(r!=s) ){  /* exist transition   */
                       ni=r>s?r:s;             /* ni > nj : triangle low */
                       nj=r<s?r:s;             /* nj < ni : triangle low */
                       if(M[ni][nj]==1){       /* if differents          */

                          M[i][j]=1;

                          #if DEBUG==1
                          printf("%c: (%d,%d -> %d,%d)\n", SIGMA[k] , i, j, ni, nj);
                          print_tablediff(M, numst);
                          #endif  

                          /* mark recursion and free list */
                          matlist[i][j] = mark_recursion(M,matlist,matlist[i][j]); 

                          k=ns; /* to next */
                          continue;
                       }
                       else{ 

                          #if DEBUG==1
                          printf("\t%c: (%d,%d -- DEP --> %d,%d)\n", SIGMA[k] , i, j, ni, nj);
                          #endif 

                          /* add list dependent */ 
                          matlist[ni][nj] = add_list_nodediff(matlist[ni][nj],i,j);
                       }
                    }
                    else if( (r<0)&&(s>=0) ){
                          M[i][j]=1;

                          #if DEBUG==1
                          printf("%c: (%d,%d -> NULL A)\n", SIGMA[k] , i, j );
                          print_tablediff(M, numst);
                          #endif  

                          /* mark recursion and free list */
                          matlist[i][j] = mark_recursion(M,matlist,matlist[i][j]); 

                          k=ns; /* to next */
                          continue;
                    }
                    else if( (r>=0)&&(s<0) ){

                          M[i][j]=1;

                          #if DEBUG==1
                          printf("%c: (%d,%d -> NULL B)\n", SIGMA[k] , i, j);
                          print_tablediff(M, numst);
                          #endif  

                          /* mark recursion and free list */
                          matlist[i][j] = mark_recursion(M,matlist,matlist[i][j]); 

                          k=ns; /* to next */
                          continue;
                    }
               }   
          }
     }
  }

  #if DEBUG==1
  print_tablediff(M, numst);
  #endif  

  /* cumulative transitions */
  for(i=numst-1;i>=0;i--){
     /* if i state equal for some state */
     for(j=i-1;j>=0;j--){     /* j < i : triangle low */
         if(M[i][j]==0){       /* equals        */
              cumulative_state(D,j,i);  
              nullable[i]=1;   /* i is nullable marked */
              continue;
         } 
     }
  }

  /* numnull */
  numnull=0;
  for(i=0;i<numst;i++){
    if(nullable[i]) numnull++;
  }

  /* free list of end states */
  nodelist_state_free(D->end);
  D->end=NULL;


  /* mark free states from tail */ 
  j=numst-1;
  for(i=0;i < numst-numnull;i++){
    if(nullable[i]) {
       while(nullable[j]) j--;
       copy_state(D,i,j);
       if(end[j]) D->end = add_nodelist_state(D->end,i);
       j--;
    }
    else if(end[i]) D->end = add_nodelist_state(D->end,i);
  }

  /* reduction table of transitions */
  reduction_d(D,numnull);

  /* free's */
  matlist_nodediff_free(matlist, numst, numst);

  im_free(M,numst);

  free(end);
  free(nullable);

  return D;
}

/*************************************/

/*************************************/
nodediff *mark_recursion(int **M, nodediff ***matlist, nodediff *L){
  nodediff *aux;

  aux=L;
  while(aux!=NULL){ 
     if(M[aux->i][aux->j]==0){       /* if differents  */
         M[aux->i][aux->j]=1;

         #if DEBUG==1
         printf("RECURSION: (%d,%d) <-  1\n",  aux->i, aux->j);
         #endif  

         /* mark recursion */
         matlist[aux->i][aux->j] = mark_recursion(M, matlist, matlist[aux->i][aux->j]); 
     }
     aux=aux->next;
  }
  
  list_nodediff_free(L); /* free list */

  return NULL;
}
/*************************************/
void cumulative_state(dfa *D, int p, int q){
   int numst=D->numst;
   int i,j;

   /* pointers for q change for p */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){     
          if(D->d[i][j]==q){  /* state q */
                D->d[i][j]=p;
          }
     }
   }
  
}

/*************************************/
void copy_state(dfa *D, int p, int q){
   int numst=D->numst;
   int i,j;

   /* pointers for q change for p */
   for(i=0;i<numst;i++){
     for(j=0;j<ns;j++){     
          if(D->d[i][j]==q){  /* state q */
                D->d[i][j]=p;
          }
     }
   }

   /* copy transitions: q to p */
   for(j=0;j<ns;j++){  
      D->d[p][j]=D->d[q][j];
   }
   
}

/*************************************/
void reduction_d(dfa *D, int n){
   int **newd;
   int numst=D->numst;
   int i,j;

   /* allocated new table of transitions */
   newd = im_malloc(numst-n,ns);
   im_init(newd, numst-n , ns , -1);


   /* copy transitions */
   for(i=0;i<numst-n;i++){
     for(j=0;j<ns;j++){     
           newd[i][j] = D->d[i][j];
     }
   }

   /* free old d */
   free(D->d);
 
   /* connect newd to dfa */
   D->d = newd;
   
   /* numst */
   D->numst=numst-n;

}

/*************************************/

/*************************************/
void print_tablediff(int **M, int numst){
  int i,j;

  printf("\n");

  for(i=0;i<numst;i++){
     printf("%4d |",i);
     for(j=0;j<i;j++){     /* j < i : triangle low */
          printf("%4d",M[i][j]);
     }
     printf("\n");
  }

  printf("     +");
  for(j=0;j<i;j++){     
      printf("----");
  }
  printf("\n");

  printf("      ");
  for(j=0;j<i;j++){     
      printf("%4d",j);
  }
  printf("\n\n");

}

/*************************************/
