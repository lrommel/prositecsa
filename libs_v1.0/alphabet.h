/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/

#ifndef ALPHABET_H
#define ALPHABET_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

/* globals */
/*************************************/
char *SIGMA;
int  ns;
/*************************************/


/*************************************/

char letter_inSigma( int a );

/*************************************/
int  alphabet_success();
void load_alphabet(char *filename);
void print_alphabet();

/*************************************/

#endif
