
This is an implementation of the algorthm to align two sequences 
restricted by a PROSITE pattern, that was published in <TO APPEAR>.

The programs will compile in a regular Linux environment with git,
gcc and make.

After cloning the repository and compiling, an executable named 
prositecsa will be at the top level directory.  
The repository also includes two example sequences that may be
used to test the installation.

To download, clone this git repository:

> git clone https://lrommel@bitbucket.org/lrommel/prositecsa

To compile, entre the repository directory and invoke make:

> cd prositecsa/
> make

The executable, prositecsa, receives six arguments:

> prositecsa [file-s] [file-t] [file-PROSITE] [file-scoring] [NFA-Construction-type] [state-list]

where
               [file-s] is the pathname of a text file containing
                        the sequence "s"
               [file-t] is the pathname of a text file containing 
                        the sequence "t"
         [file-PROSITE] is the pathname of a text file containing 
                        the PROSITE pattern
         [file-scoring] is the pathname of a text file containing 
                        the alphabet and the scoring function
[NFA-Construction-type] may be 0 for Thompson construction and 1 
                        for the simplified construction
           [state-list] may 0 not to use active-states lists and 1 
                        to use active-states lists.

You will almost always prefer [NFA-Construction-type] = 1 and 
[state-list] = 1.

The alingment is printed to the standard output. 
The memory usage data are printed to the standard error.

The directory "prositecsa/patterns" contains 1318 PROSITE patterns 
that may be used with prositecsa. 
For example, the file "patterns/PS60024.prosite" contains:

C-x(6)-C-x(6)-C-C-x(2)-C-x(2)-C-x-C-x(6)-C-x-C-x(6,9)-C.

Other patterns may be created in a text file with a single line.

The alphabet and scoring function file format is:

  - a line containing the length (n) of the alphabet,
  - a blank line,
  - a line containing n alphabet symbols with no blank spaces,
  - a blank line,
  - an n times n matrix of integer scores for each pair of 
                symbols in the alphabet, indexed in the same 
                order provided in the third row,
  - a blank line,
  - the integer gap score.

A scoring file is provided in the "in" directory.  
In the same directory there are two example sequences, 
that may be aligned with the command, that will write 
two files:

./prositecsa in/seq_1.txt in/seq_2.txt patterns/PS00017.prosite in/scoring.txt 1 1 >out/alignment.txt 2>out/memory_usage.txt

The "out" directory contains the files that are produced 
by the above command.


###########################################
# By:  
#      Lise Rommel Romero Navarrete
#      lrommel@ic.unicamp.br   
#
#      Universidade Estadual de Campinas
#      Instituto de Computação
#
###########################################




