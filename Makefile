#####################################
# By:  
#      Universidade Estadual de Campinas
#      Instituto de Computação
#
#      Lise Rommel Romero Navarrete
#      lrommel@ic.unicamp.br   
#
##################################### 

#GCC = @gcc
GCC = gcc

#CFLAGS  = -g -Wall
CFLAGS  = -O3

LIBS = -ldl

CC = $(GCC) $(CFLAGS) $(LIBS)

ifdef DEBUG
CC = $(GCC) -DDEBUG=$(DEBUG)  $(CFLAGS) $(LIBS)
endif

DIRLIBS = libs_v1.0
DIROBJS = objs



########################################################################
o_UTIL  = $(DIROBJS)/utils.o
o_NODE  = $(DIROBJS)/nodelist_state.o 
o_RE    = $(DIROBJS)/re.o
o_ENFA  = $(DIROBJS)/enfa.o
o_R2E   = $(DIROBJS)/re2enfa.o 
o_ALPH  = $(DIROBJS)/alphabet.o 
o_BASE  = $(o_UTIL) $(o_NODE) $(o_ENFA) $(o_RE) $(o_R2E) $(o_ALPH)
o_DFA   = $(DIROBJS)/dfa.o
o_E2D   = $(DIROBJS)/enfa2dfa.o 
o_DMIN  = $(DIROBJS)/dfamin.o 
o_NPRO  = $(DIROBJS)/nfaprod.o 
o_NFA   = $(DIROBJS)/nfa.o
o_E2N   = $(DIROBJS)/enfa2nfa.o 
o_ALIGN = $(DIROBJS)/alignment.o 
o_RECSA = $(DIROBJS)/recsa.o 
o_GLOBA = $(DIROBJS)/global_align.o 
o_PROS =  $(DIROBJS)/prosite.o 
o_TT   =  $(DIROBJS)/tictoc.o 
o_MC   =  $(DIROBJS)/malloc_count.o 
o_SC   =  $(DIROBJS)/stack_count.o 
o_CNT  =  $(o_MC) $(o_SC)
########################################################################




########################################################################
# make 
all: prositecsa



########################################################################
# make prositecsa
prositecsa: $(o_BASE) $(o_NFA)  $(o_DFA) $(o_E2N) $(o_NPRO) $(o_ALIGN) $(o_RECSA) $(o_TT) $(o_PROS) $(o_CNT) $(o_GLOBA) ./main_prositecsa.c
	$(CC) $^ -o $@ 


########################################################################
# make clean 
clean:
	$(RM) $(DIROBJS)/*.o
	$(RM) prositecsa



########################################################################
# make $(DIROBJS)/%.o
$(DIROBJS)/%.o: $(DIRLIBS)/%.c $(DIRLIBS)/%.h
	$(CC)  -o $@  -c $< 
 

########################################################################
# make  out 
# make  out   ID=PS00017   CONSTRUCTION=1  STATELIST=1

ifndef ID
ID = PS00017
endif

ifndef CONSTRUCTION
CONSTRUCTION=1
endif

ifndef STATELIST
STATELIST=1
endif

SCORE    = in/scoring.txt
SEQ1     = in/seq_1.txt
SEQ2     = in/seq_2.txt
PATTERN  = patterns/$(ID).prosite
FILES    = $(SEQ1) $(SEQ2) $(PATTERN) $(SCORE)

CURR:= $(shell date +%Y%m%d_%Hh%Mm%Ss%N)

FILE_OUT = out/$(CURR)_$(ID)_C$(CONSTRUCTION)_S$(STATELIST).alignment.txt
FILE_MEM = out/$(CURR)_$(ID)_C$(CONSTRUCTION)_S$(STATELIST).memory_usage.txt

out: $(FILE_OUT)

$(FILE_OUT): prositecsa $(FILES)
	./$(^)  $(CONSTRUCTION)  $(STATELIST)  > $(FILE_OUT)  2> $(FILE_MEM)

########################################################################







