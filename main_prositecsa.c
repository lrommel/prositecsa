/*************************************
 * By:  
 *      Universidade Estadual de Campinas
 *      Instituto de Computação
 *
 *      Lise Rommel Romero Navarrete
 *      lrommel@ic.unicamp.br 
 *
 *************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libs_v1.0/prosite.h"
#include "libs_v1.0/re2enfa.h"
#include "libs_v1.0/enfa2nfa.h"
#include "libs_v1.0/nfaprod.h"
#include "libs_v1.0/alignment.h"
#include "libs_v1.0/recsa.h"
#include "libs_v1.0/tictoc.h"
#include "libs_v1.0/malloc_count.h"

int main(int argc, char *argv[] ){
	
   /*******************************************/  	
   char basename[200];
   int construction_type, state_list, linear_dp = 1, ROWS, COLS;
   char *Pros;                  /* PROSITE pattern */ 
   char *R;                     /* Regular expression */
   enfa *E;                     /* ENFA */
   nfa  *N, *Np;                /* NFAs */
   nfaprod *P;                  /* Arslan automaton */
   dp_states_container **A;     /* Dynamic programming matrix */
   alignment* align = NULL;     /* Alignment */
   char *s,*t;                  /* sequences */
   int n, m;                    /* sequences lengths */
   int **M;                     /* matrix score */
   int g;                       /* gap score */       
   /*******************************************/


   /*******************************************/
   /* command line */
   if (argc < 5){
       printf("Usage: %s  [file-s] [file-t] [file-PROSITE] ",argv[0]);
       printf("[file-scoring]  [NFA-Construction-type]  [state-list] \n"); 
       exit(0);
   }
   /*******************************************/
   
   
   /*******************************************/    
   /* print memory status */
   malloc_count_print_status(); fflush(stderr);
   /*******************************************/    


   /*******************************************/    
   /* construction-type: argv[5]*/
   construction_type=1;
   if      ( strcmp(argv[5],"0")==0 ) construction_type=0;
   else if ( strcmp(argv[5],"2")==0 ) construction_type=2;

   /* state_list: argv[6]*/
   state_list=0;
   if      ( strcmp(argv[6],"1")==0 ) state_list=1;
 
   /* extract basename  */
   extract_basename(basename,argv[3]);

   /* read scoring */
   M = load_scoring(&g,argv[4]);  // include SIGMA!

   #if DEBUG==1
       printf("[%d]\n",ns);
       printf("[%s]\n",SIGMA);
       im_print(M,0,ns-1,0,ns-1);
       printf("[%d]\n",g);
   #endif

   /* read sequences */
   s = load_sequence(argv[1]);
   t = load_sequence(argv[2]);
 
   /* print sequences */
   print_sequences(s,t);
 
   /* read PROSITE Pattern */
   Pros = load_prosite(argv[3]);
   print_prosite(Pros);
   /*******************************************/
   

   /*******************************************/
   /* Total time: init  */
   tic_id(1);
   /*******************************************/  
   

   /*******************************************/
   printf("\n\n\n\n");
   printf("--------------------- \n"); 
   printf("PROSITE Pattern to Regular Expression :\n"); 
   fprintf(stderr,"\nRegular Expression construction ::\n");
   malloc_count_print_status(); fflush(stderr);
   printf("--------------------- ");
   tic();
   R = pros2R(Pros); /* Regular Expression: convert Pros to R  */
   toc();
   malloc_count_print_status(); fflush(stderr);
   printf("---------------------\n");
   print_re(R);
   /*******************************************/
 

   /*******************************************/
   printf("\n\n\n\n");
   printf("--------------------- \n"); 
   printf("ENFA Equivalent construction :\n"); 
   fprintf(stderr,"\nENFA Equivalent construction ::\n");
   malloc_count_print_status(); fflush(stderr);
   printf("--------------------- ");
   tic();
   E = re2enfa_thompson(R, construction_type); /* Automaton Equivalent */
   toc();
   malloc_count_print_status(); fflush(stderr);
   printf("---------------------\n");
   /*******************************************/


   /*******************************************/
   printf("\n\n\n\n");
   printf("--------------------- \n"); 
   printf("E-free NFA Equivalent construction :\n"); 
   printf("--------------------- ");
   fprintf(stderr,"\nE-free NFA Equivalent construction ::\n");
   malloc_count_print_status(); fflush(stderr);
   tic(); 
   N = enfa2nfa(E);  /* E-free NFA construction */
   toc();
   malloc_count_print_status(); fflush(stderr);
   printf("---------------------\n");
   /*******************************************/


   /*******************************************/
   /* Output of constraint details: Regular expression and automata */
   printf("---------------------\n");
   printf("Alfabeto :\t %s\n"          ,   SIGMA       );
   printf("size Alf.:\t %d simbolos\n" ,   ns          );  
   printf("exp reg  :\t %s\n"          ,   R           );
   printf("size RE  :\t %d simbolos \n",   (int) strlen(R)   ); 
   printf("enfa     :\t %d \t %d \t %d\n"    ,   E->numst    , get_numtrans_enfa(E) , get_numtransE_enfa(E)  );
   printf("nfa      :\t %d \t %d\n"    ,   N->numst    , get_numtrans_nfa(N)    );
   printf("---------------------\n");
   /*******************************************/


   /*******************************************/
   printf("\n\n\n\n");
   printf("--------------------- \n"); 
   printf("Arslan automaton construction :\n"); 
   printf("--------------------- ");
   fprintf(stderr,"\nNFAPROD:\n"); 
   malloc_count_print_status(); fflush(stderr);
   tic();
   P = nfa2prod(N);  /* Arslan automaton */
   toc();
   malloc_count_print_status(); fflush(stderr);
   printf("---------------------\n");
   /*******************************************/


   /*******************************************/
   /* Output of constraint details: Regular expression and automata */
   printf("---------------------\n");
   printf("Alfabeto :\t %s\n"          ,   SIGMA       );
   printf("size Alf.:\t %d simbolos\n" ,   ns          );  
   printf("exp reg  :\t %s\n"          ,   R           );
   printf("size RE  :\t %d simbolos \n",   (int) strlen(R)   ); 
   printf("enfa     :\t %d \t %d\n"    ,   E->numst    , get_numtrans_enfa(E)   );
   printf("nfa      :\t %d \t %d\n"    ,   N->numst    , get_numtrans_nfa(N)    );
   printf("nfaprod  :\t %d \t %d\n"    ,   P->numst    , get_numtrans_nfaprod(P) );
   printf("---------------------\n");
   /*******************************************/


   /*******************************************/
   nodelist_state  **active_s = NULL, **active_t = NULL;
   Np = enfa2nfa(E);
   nfa_addLoop(Np);

   /* init active state lists */
   if (state_list==1) {
         active_s = nfa_actives( Np, s);
         active_t = nfa_actives( Np, t);
   }
 
   /* print memory status */
   fprintf(stderr,"\nMATRIX:\n");
   malloc_count_print_status(); fflush(stderr);
 
   /* print state list status */
   printf("\n%s state list:  state_list = %d\n"  , (state_list==1)?"with":"without", state_list );

   /* Dynamic programming type */
   linear_dp = 1;
   /*******************************************/
      

   /*******************************************/
   /* length to Dynamic programming Matrix */
   n = strlen(s);
   m = strlen(t);
   if (linear_dp){   /* 1 line: A ( 1 ) x ( m + 3 ) */
      ROWS = 1; 
      COLS = m + 3;
   }
   else{             /* complete matrix: A ( n + 1 ) x ( m + 1 ) */
      ROWS = n + 1;
      COLS = m + 1;
   } 
   /*******************************************/


   /*******************************************/
   printf("\n\n\n\n");
   printf("--------------------- \n"); 
   printf("Dynamic programming:  \n"); 
   printf("--------------------- ");
   tic_id(0);  /* runtime count init */
   if (linear_dp) {
     /* Dynamic programming: 1 line: A ( 1 ) x ( m + 3 ) : final cell in DP is A[0][?] */
     A = recsa_arslan_1line(s, t, P, M, g, active_s, active_t, Np->numst ); 
   }
   else{
     /* Dynamic programming: complete matriz: A ( n + 1 ) x ( m + 1 ) : final cell in DP is A[n][m] */
     A = recsa_arslan(s, t, P, M, g, active_s, active_t, Np->numst ); 
   }
   toc_id(0,"_TIME_DP_");  /* runtime count end */
   malloc_count_print_status(); fflush(stderr); /* memory count */
   printf("---------------------\n");
   /*******************************************/

 
    /*******************************************/
   if (A==0)  {
        printf("\n DP Matrix  is NULL\n"); 
        fflush(stdout); 
        exit(0);
   }
   /*******************************************/
   

   /*
   ////////////////////////////////////////////// 
   printf("\n\n\n\n");
   printf("--------------------- \n"); 
   printf("Recovery by complete matrix\n"); 
   printf("--------------------- \n");
   printf("\n");

   // recovery by complete matrix  
   align = recsa_arslan_recovery( s, t, P, A, M, g );
 
   if (align==NULL) {
        printf("\n Alignment is NULL\n"); 
        fflush(stdout); 
        exit(0);
   }

   // set cut1 and cut2 to s_line and t_line in align  
   alignment_set_cuts(align, M,  g);  
   ////////////////////////////////////////////// 
 
   //////////////////////////////////////////////
   // print details 
   print_alignment_details(align);    
   print_prosite(Pros);
   print_re(R);
   print_alignment_horizontal(align, M,  g);     
   print_alignment_vertical(align, M,  g);    
   ////////////////////////////////////////////// 
   */

 
  /*******************************************/
   printf("\n\n\n\n");
   printf("--------------------- \n");
   printf("Recovery by positions\n"); 
   printf("---------------------\n");
   printf("\n");
   tic();  /* runtime count init */
 
   /* Final position to recovery: A[imax][jmax] */
   int jmax, imax;
   if (linear_dp){ // 1 line: imax = 0,  jmax = ( ( n + 1 ) * ( m + 1 ) - 1 ) % ( m + 3 );
      imax = 0;
      jmax = ( ( n + 1 ) * ( m + 1 ) - 1 ) % ( m + 3 );
   }
   else{ // complete matrix: imax = strlen(s), jmax = strlen(t)
      imax = n;
      jmax = m;
   }

   #if DEBUG==3000  
       /* test */
       printf("\n to Recovery: imax=%d, jmax=%d\n", imax , jmax); 
       fflush(stdout); 
   #endif

   /* recovery  by positions */
   align = recsa_arslan_recovery_by_positions( s, t, P, A, imax , jmax, M, g , active_s, active_t, Np->numst  );   
   if (align==NULL) {
        printf("\n Alignment is NULL: imax=%d, jmax=%d\n", imax , jmax); 
        fflush(stdout); 
        exit(0);
   }
   toc_id(0,"_TIME_RECOVERY_" );  /* runtime count end */
   /*******************************************/
 

   /*******************************************/
   /* Total time  */
    toc_id(1,"_TIME_ALL_");  /* runtime count end */
    /*******************************************/


   /*******************************************/
   /* print details */
   print_alignment_details(align);   
   print_prosite(Pros);
   print_re(R);
   print_alignment_horizontal(align, M,  g);    
   print_alignment_vertical(align, M,  g);    
   /*******************************************/ 


   /*******************************************/
   /* free memory */
   free(Pros);
   free(R);
   enfa_free(E);
   nfa_free(N);
   nfa_free(Np);
   alignment_free(align);
   nfa_actives_free( active_s , s );
   nfa_actives_free( active_t , t );
   free(s);   
   free(t);
   nfaprod_free(P);   /* rev */
   scoring_free(M);
   dp_matrix_free( A, ROWS, COLS );   /* rev */
 
   /* final status memory */
   fprintf(stderr,"\n");
   malloc_count_print_status(); fflush(stderr);
   fprintf(stderr,"\n");
   /*******************************************/

   return 0;

}

/*************************************/

